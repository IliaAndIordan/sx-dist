export const environment = {
  production: true,
  abreviation:'PD',
  services: {
    url: {

      auth: 'https://sx.ws.iordanov.info/',
      company: 'https://sx.ws.iordanov.info/company/',
      pricefile:'https://sx.ws.iordanov.info/pricefile/',
      project:'https://sx.ws.iordanov.info/project/',
      commodity:'https://sx.ws.iordanov.info/mfr/',
      bom: 'https://sx.ws.iordanov.info/bom/',
    },
    domains: {
      sx: 'sx.ws.iordanov.info',
    },
  }
};
