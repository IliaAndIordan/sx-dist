import { NgModule } from '@angular/core';

import { AppRoutingModule, routableComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { SxCoreModule } from './@core/@core.module';
import { SxShareModule } from './@share/@share.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    //
    AppRoutingModule,
    SxShareModule,
    //
    SxCoreModule,
  ],
  declarations: [
    AppComponent,
  ],
  exports:[ ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
