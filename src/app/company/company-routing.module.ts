import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { SxCompanyComponent } from './company.component';
import { SxCompanyHomeComponent } from './home.component';
import { SxCompanyDashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxCompanyComponent,
    children: [
      { path: AppRoutes.Root, component: SxCompanyHomeComponent,
        
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: SxCompanyDashboardComponent },
          /*
          { path: AppRoutes.usersList, component: AmsUserListComponent },
          { path: AppRoutes.profile, component: AdminUserProfileComponent },

          { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
          { path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent},
          ]},
          { path: AppRoutes.mac, component: AmsAdminMacHomeComponent , children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminMacHomeTabsComponent},
          ]},
          { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
          */
        ]
        
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SxCompanyRoutingModule { }

export const routedComponents = [
  SxCompanyComponent, 
  SxCompanyHomeComponent,
  SxCompanyDashboardComponent,
];
