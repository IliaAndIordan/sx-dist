import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// -Sx Modules
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxCoreModule } from '../@core/@core.module';
// -Services
import { SxCompanyService } from './company.service';
// -Models
import { routedComponents, SxCompanyRoutingModule } from './company-routing.module';
import { SxCompanyMenuMainComponent } from './menu-main/menu-main.component';


@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
    //
    SxCompanyRoutingModule,
  ],
  declarations: [
    routedComponents,
    SxCompanyMenuMainComponent,
  ],
  exports: [],
  entryComponents: [
  ],
  providers: [
    SxCompanyService,
  ]
})
export class SxCompanyModule { }
