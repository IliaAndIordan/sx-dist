import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { environment } from 'src/environments/environment';
import { SxCompanyService } from '../company.service';

@Component({
    selector: 'sx-company-menu-main',
    templateUrl: './menu-main.component.html',
    // animations: [PageTransition]
})
export class SxCompanyMenuMainComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Fields
     */
    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    user: UserModel;
    userName: string;
    userChanged: Subscription;
    company: CompanyModel;
    companyChanged: Subscription;


    avatarUrl: string;
    env: string;
    isAdmin: boolean;

    constructor(private router: Router,
                private cus: CurrentUserService,
                private cService: SxCompanyService) { }

    ngOnInit(): void {
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.companyChanged = this.cus.companyChanged.subscribe(region => {
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.env = environment.abreviation;
        this.avatarUrl = this.cus.avatarUrl;
        this.company = this.cus.company;
        let name = 'Guest';
        if (this.cus.user) {
            name = this.cus.user.user_name ? this.cus.user.user_name : this.cus.user.e_mail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    spmRegionOpen() {
        // this.wadService.regionPanelOpen.next(true);
    }
    spmSubegionOpen() {
        // this.wadService.subregionPanelOpen.next(true);
    }
}
