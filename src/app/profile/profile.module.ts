import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// -Sx Modules
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxCoreModule } from '../@core/@core.module';
// -Services
import { SxProfileService } from './profile.service';
// -Models
import { routedComponents, SxProfileRoutingModule } from './profile-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
    //
    SxProfileRoutingModule,
  ],
  declarations: [
    routedComponents,
  ],
  exports: [],
  entryComponents: [
  ],
  providers: [
    SxProfileService,
  ]
})
export class SxProfileModule { }
