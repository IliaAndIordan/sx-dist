import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { SxProfileComponent } from './profile.component';
import { SxProfileHomeComponent } from './home.component';
import { ProfileDashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxProfileComponent,
    children: [
      { path: AppRoutes.Root, component: SxProfileHomeComponent,
        
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: ProfileDashboardComponent },
          /*
          { path: AppRoutes.usersList, component: AmsUserListComponent },
          { path: AppRoutes.profile, component: AdminUserProfileComponent },

          { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
          { path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent},
          ]},
          { path: AppRoutes.mac, component: AmsAdminMacHomeComponent , children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminMacHomeTabsComponent},
          ]},
          { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
          */
        ]
        
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SxProfileRoutingModule { }

export const routedComponents = [
  SxProfileComponent, 
  SxProfileHomeComponent,
  ProfileDashboardComponent,
];
