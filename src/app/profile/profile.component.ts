import { Component, OnInit } from '@angular/core';

@Component({
  template: '<div class="modus-layout"> <router-outlet></router-outlet> </div>'
})
export class SxProfileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
