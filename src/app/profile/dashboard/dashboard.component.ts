

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { Animate } from 'src/app/@core/const/animation.const';
import { COMMON_IMG_AVATAR, NO_IMG_URL, Tall, Wide } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { MatDialog } from '@angular/material/dialog';
import { AmsUserEditDialog } from 'src/app/@share/components/dialogs/user-edit/user-edit.dialog';


@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class ProfileDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    state: string = Animate.in;

    productImageClass: string;
    cardImage: string;
    imageNotAvailable = COMMON_IMG_AVATAR;

    redirectTo: string;
    copyrightCurrentYear = new Date().getFullYear();
    copyrightStartYear = 2018;

    user: UserModel;

    errMsg: string;
    env: string;

    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        public dialogService: MatDialog,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private pClient: SxApiProjectClient) { }


    ngOnInit(): void {
        this.state = (this.state === Animate.in ? Animate.out : Animate.in);
        this.env = environment.abreviation;
        this.cardImage = this.cus.avatarUrl;
        this.user = this.cus.user;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    //#region Image

    onImageLoad(): void {
        this.productImageClass = this.getImageClass();
    }

    getImageClass(): string {
        const image = new Image();
        image.src = this.cardImage;

        if (image.width > image.height + 20 || image.width === image.height) {
            return Wide;
        } else {
            return Tall;
        }
    }

    //#endregion

    //#region  data

    public gatGvdtTsoSearchHits(): void {
        /*
        const gvdt: GvDataTable = GvdtTsoSearchHits.GvdtTsoSearchHitsTotalDefinition();
        // console.log('getCompanyBaseInfo-> gvdt', gvdt);
        this.lService.getGvDataTable(gvdt)
            .subscribe((resp: GetGvDataTableResponse) => {
                console.log('gatGvdtTsoSearchHits-> resp', resp);
                this.gvdtHits = resp;
                this.chartData = GvdtTsoSearchHits.getLineChartData(resp.dataTable);
                this.chartLabels = GvdtTsoSearchHits.getLineChartLabels(resp.dataTable);
                console.log('chartData=', this.chartData);
            });
            */
    }
    //#endregion

    //#region  Actions

    editUser(){
        
        const dialogRef = this.dialogService.open(AmsUserEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                user:this.user,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUser-> res:', res);
            if (res) {
                this.user = res;
                //this.table.renderRows();
                //this.loadPage();
            }
            //this.initFields();
        });
        
    }

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    //#endregion
}
