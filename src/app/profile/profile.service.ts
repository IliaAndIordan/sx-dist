import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { ApiAuthClient } from '../@core/services/auth/api/api-auth.client';
// -Models
import { AppStore, URL_NO_IMG_SQ } from '../@core/const/app-storage.const';
import { CompanyModel, CompanyProductModel } from '../@core/models/common/sx-company.model';
import { UserModel } from '../@core/services/auth/api/dto';
import { SxApiCompanyClient } from '../@core/services/api/company/api-client';


@Injectable({
    providedIn: 'root',
})
export class SxProfileService {

    /**
     *  Fields
     */


    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private authClient: ApiAuthClient,
        private comClient: SxApiCompanyClient) {
    }

    //#region user

    get user(): UserModel {
        return this.cus.user;
    }

    set user(value: UserModel) {
        this.cus.user = value;
    }

    get avatarUrl(): string {
        return this.cus.avatarUrl;
    }

    //#endregion

    //#region company

    get company(): CompanyModel {
        return this.cus.company;
    }

    set company(value: CompanyModel) {
        this.cus.company = value;
    }

    //#endregion


    //#region Admin User Charts
    /*
    usersCount: number;
    loginsCountData: ResponseLoginsCount;
    public loadLoginsCount(): Observable<LoginsCountModel[]> {

        return new Observable<LoginsCountModel[]>(subscriber => {

            this.aUserClient.loginsCountSeventDays()
                .subscribe((resp: ResponseLoginsCount) => {
                    //console.log('loadLoginsCount-> resp', resp);
                    this.loginsCountData = resp;
                    if (resp) {
                        if (resp.data && resp.data.totals) {
                            this.usersCount = resp.data.totals[0].total_rows;
                        }
                        const data = LoginsCountModel.dataToList(resp.data);
                        //console.log('loadLoginsCount-> data', resp.data);

                        subscriber.next(data);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                    err => {

                        throw err;
                    });
        });

    }
    */
    //#endregion


}
