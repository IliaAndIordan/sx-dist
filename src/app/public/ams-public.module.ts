import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { routedComponents, SxPublicRoutingModule } from './ams-public-routing.module';
import { RouterModule } from '@angular/router';
import { AmsPublicComponent } from './ams-public.component';
import { LoginModal } from './dialogs/login/login.dialog';
import { RegisterModal } from './dialogs/register/register.dialog';



@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxPublicRoutingModule,
    //
  ],
  declarations: [
    AmsPublicComponent,
    routedComponents,
    //-Dialogs
    LoginModal,
    RegisterModal,
  ],
  exports: [],
  entryComponents: [
    LoginModal,
    RegisterModal,
]
})
export class AmsPublicModule { }
