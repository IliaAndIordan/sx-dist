import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { LatLng, ResponseAuthenticate } from 'src/app/@core/services/auth/api/dto';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

@Component({
  templateUrl: './login.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class LoginModal implements OnInit {

  formGrp: FormGroup;

  email: string;
  password: string;
  latlon: LatLng;

  hasSpinner = false;
  errorMessage: string;
  redirectTo: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private authClient: ApiAuthClient,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    public dialogRef: MatDialogRef<LoginModal>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  get ctr_email() { return this.formGrp.get('ctr_email'); }
  get ctr_password() { return this.formGrp.get('ctr_password'); }

  get errorEMail(): string {
    return this.ctr_email.hasError('required') ? 'E-Mail is required.' :
      this.ctr_email.hasError('email') ? 'Please enter a valid email address' : '';
  }

  get errorPassword(): string {
    return this.ctr_password.hasError('required') ? 'Password is required.' :
      this.ctr_password.hasError('minlength') ? 'Password must be et least 3 characters lon' : '';
  }

  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.route.queryParams
      .subscribe(params => this.redirectTo = params.redirectTo || AppRoutes.profile);

    this.formGrp = this.fb.group({
      ctr_email: new FormControl(this.email, [Validators.required, Validators.email]),
      ctr_password: new FormControl(this.password, [Validators.required, Validators.minLength(3)]),
    });
    this.cus.getLocation(true).subscribe(location => {
      this.latlon = location;
    });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  // tslint:disable-next-line: typedef
  onLoginClick() {

    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.email = this.ctr_email.value;
      this.password = this.ctr_password.value;

      this.authClient.autenticate(this.email, this.password, this.latlon )
        .subscribe((resp: ResponseAuthenticate) => {
          console.log('autenticate-> resp', resp);
          const user = this.cus.user;
          console.log('autenticate-> resp user', user);
          if (user) {
            // this.spinerService.display(false);
            this.hasSpinner = false;
            const name = user.user_name ? user.user_name : user.e_mail;
            this.toastr.success('Welcome ' + name + '!', 'Login success');
            setTimeout((router: Router, container = this) => {
              if (this.redirectTo) {
                this.router.navigate([this.redirectTo]);
              } else {
                this.router.navigate([AppRoutes.Root, AppRoutes.profile]);
              }

              this.dialogRef.close(user);
            }, 1000);
          } else {
            this.hasSpinner = false;
            this.toastr.error('Login Failed', 'Login Failed');
          }
        },
          err => {
            console.error('autenticate-> got an error: ' + err);
            this.errorMessage = 'Please enter valid values for fields';
            this.hasSpinner = false;
          },
          () => {
            console.log('autenticate-> got a complete notification');
          });
    } else {
      this.errorMessage = 'Please enter valid values for fields';
      this.hasSpinner = false;
    }
  }


}
