import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { WelcomeComponent } from './welcome/welcome-component';
import { AmsPublicComponent } from './ams-public.component';
import { AmsPublicHomeComponent } from './home.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: AmsPublicComponent,
    children: [
      { path: AppRoutes.Root, component: AmsPublicHomeComponent,
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: WelcomeComponent },
          // { path: AppRoutes.Login, pathMatch: 'full', component: LoginComponent },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SxPublicRoutingModule { }

export const routedComponents = [AmsPublicComponent, AmsPublicHomeComponent, WelcomeComponent];
