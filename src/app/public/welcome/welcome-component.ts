import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { URL_COMMON_IMAGE_SX, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';

@Component({
    templateUrl: './welcome-component.html',
})

export class WelcomeComponent implements OnInit, OnDestroy {

    cdImgUrl = URL_COMMON_IMAGE_TILE + 'sunset-construction-workers.jpg';
    bomImgUrl = URL_COMMON_IMAGE_SX + 'bom.png';
    rfqImgUrl = URL_COMMON_IMAGE_SX + 'rfq.jpg';

    constructor(private router: Router,
                private route: ActivatedRoute,
                private toastr: ToastrService,) { }



    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    showMsg(): void {
        console.log('showMsg->');
        this.toastr.success('Not Very Important Message', 'INFORMATION')
    }

}
