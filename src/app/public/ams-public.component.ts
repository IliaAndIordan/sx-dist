import { Component } from '@angular/core';


@Component({
  template: '<div class="login-page-container"> <router-outlet></router-outlet> </div>'
})

export class AmsPublicComponent  {

}
