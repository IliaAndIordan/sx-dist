import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { SxBomComponent } from './bom.component';
import { SxBomHomeComponent } from './home.component';
import { SxBomDashboardComponent } from './dashboard/dashboard.component';
import { SxBomListHomeComponent } from './list/bom-list.component';
import { SxGridBomComponent } from './list/grids/grid-bom.component';
import { SxBomItemsHomeComponent } from './items/bom-items.component';
import { SxBomItemsHomeTabsComponent } from './items/tabs/bom-items-home-tabs.component';
import { SxBomFileImportHomeComponent } from './file-import/bom-file-import-home.component';
import { SxBomFileImportSelectFileComponent } from './file-import/select-file.component';
import { SxBomFileImportMapFieldsComponent } from './file-import/map-fields.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxBomComponent,
    children: [
      {
        path: AppRoutes.Root, component: SxBomHomeComponent,

        children: [
          { path: AppRoutes.Root, pathMatch: 'full', component: SxBomDashboardComponent},
          { path: AppRoutes.list, component: SxBomListHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: SxGridBomComponent},
          ]},
          { path: AppRoutes.items, component: SxBomItemsHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: SxBomItemsHomeTabsComponent},
          ]},
          { path: AppRoutes.file_select, component: SxBomFileImportHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: SxBomFileImportSelectFileComponent},
            { path: AppRoutes.map_fields, component: SxBomFileImportMapFieldsComponent },
          ]},
          /*
          { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
          { path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent},
          ]},
          { path: AppRoutes.mac, component: AmsAdminMacHomeComponent , children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminMacHomeTabsComponent},
          ]},
          { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
          */
        ]

      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SxBomRoutingModule { }

export const routedComponents = [
  SxBomComponent,
  SxBomHomeComponent,
  SxBomDashboardComponent,
  // -LIST
  SxBomListHomeComponent,
  SxGridBomComponent,
  // -BOM
  SxBomItemsHomeComponent,
  SxBomItemsHomeTabsComponent,
  // -FILE IMPORT
  SxBomFileImportHomeComponent,
  SxBomFileImportSelectFileComponent,
  SxBomFileImportMapFieldsComponent,
];
