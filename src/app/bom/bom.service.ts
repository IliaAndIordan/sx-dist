import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { SxApiBomClient } from '../@core/services/api/bom/api-client';

// -Models
import { AppStore, URL_NO_IMG_SQ } from '../@core/const/app-storage.const';
import { CompanyModel, CompanyProductModel } from '../@core/models/common/sx-company.model';
import { UserModel } from '../@core/services/auth/api/dto';
import { Bom } from '../@core/services/api/bom/dto';
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root', })
export class SxBomService {

    /**
     *  Fields
     */
    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private client: SxApiBomClient) {
    }

    //#region user

    get user(): UserModel {
        return this.cus.user;
    }

    set user(value: UserModel) {
        this.cus.user = value;
    }

    //#endregion

    //#region company

    get company(): CompanyModel {
        return this.cus.company;
    }

    set company(value: CompanyModel) {
        this.cus.company = value;
    }

    //#endregion

    //#region BehaviorSubjects

    dashboardRefresh = new BehaviorSubject<boolean>(true);

    //#endregion

    //#region mac

    bomChanged = new Subject<Bom>();

    get bom(): Bom {
        const onjStr = localStorage.getItem(AppStore.bom);
        let obj: Bom;
        if (onjStr) {
            obj = Object.assign(new Bom(), JSON.parse(onjStr));
        }
        return obj;
    }

    set bom(value: Bom) {
        const oldValue = this.bom;
        if (value) {
            localStorage.setItem(AppStore.bom, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.bom);
        }

        this.bomChanged.next(value);
    }

    //#endregion

    //#region Admin User Charts
    /*
    usersCount: number;
    loginsCountData: ResponseLoginsCount;
    public loadLoginsCount(): Observable<LoginsCountModel[]> {

        return new Observable<LoginsCountModel[]>(subscriber => {

            this.aUserClient.loginsCountSeventDays()
                .subscribe((resp: ResponseLoginsCount) => {
                    //console.log('loadLoginsCount-> resp', resp);
                    this.loginsCountData = resp;
                    if (resp) {
                        if (resp.data && resp.data.totals) {
                            this.usersCount = resp.data.totals[0].total_rows;
                        }
                        const data = LoginsCountModel.dataToList(resp.data);
                        //console.log('loadLoginsCount-> data', resp.data);

                        subscriber.next(data);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                    err => {

                        throw err;
                    });
        });

    }
    */
    //#endregion


}
