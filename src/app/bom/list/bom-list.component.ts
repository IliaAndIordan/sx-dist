

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { SxBomService } from '../bom.service';
import { SxBomListService } from './bom-list.service';


@Component({
  templateUrl: './bom-list.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class SxBomListHomeComponent implements OnInit, OnDestroy {


  /**
   * Fields
   */
  panelInVar = Animate.in;
  panelInChanged: Subscription;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  bom: Bom;
  bomChanged: Subscription;

  get panelIn(): boolean {
    const rv = this.blService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    // console.log('panelIn-> hps:', this.hps);
    return this.blService.panelIn;
  }

  set panelIn(value: boolean) {
    this.blService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private bService: SxBomService,
    private blService: SxBomListService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    const tmp = this.panelIn;
    this.panelInChanged = this.blService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });


    this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
      this.initFields();
    });

    this.initFields();

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.bomChanged) { this.bomChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields(): void  {
    this.bom = this.blService.bom;

    // console.log('initFields-> country:', this.country);
  }

  //#region  Actions

  sidebarToggle(): void  {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoBom(value: Bom): void  {
    if (value) {
      this.blService.bom = value;
      // this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    //this.blService.regionPanelOpen.next(false);
  }

  spmRegionOpen() {
    /*
    this.region = this.wadService.region;
    if (this.region) {
      this.wadService.regionPanelOpen.next(true);
    }
*/
  }

 
  //#endregion
}
