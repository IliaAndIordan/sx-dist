import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomService } from '../bom.service';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { Bom } from 'src/app/@core/services/api/bom/dto';

// -Nodels


export const SR_LEFT_PANEL_IN_KEY = 'sx_bom_list_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'sx_bom_list_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class SxBomListService {

    /**
     *  Fields
     */

    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private client: SxApiBomClient,
        private bService: SxBomService) {
    }

    get bom(): Bom {
        return this.bService.bom;
    }

    set bom(value: Bom) {
        this.bService.bom = value;
    }

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        // console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data

    public loadBom(bomId: number): Observable<Bom> {

        return new Observable<Bom>(subscriber => {

            this.client.getBom(bomId)
                .subscribe((resp: Bom) => {
                    console.log('loadBom-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {
                        throw err;
                    });
        });

    }

    //#endregion


}
