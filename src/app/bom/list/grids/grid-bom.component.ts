import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomService } from '../../bom.service';
// --- Models
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Bom, BomTableCriteria } from 'src/app/@core/services/api/bom/dto';
import { SxBomListService } from '../bom-list.service';
import { SxBomTableDataSource } from './grid-bom.datasource';
import { SxBomEditDialog } from 'src/app/@share/components/dialogs/bom-edit/bom-edit.dialog';



@Component({
    selector: 'sx-grid-bom',
    templateUrl: './grid-bom.component.html',
})
export class SxGridBomComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<Bom>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    selected: Bom;
    bomChanged: Subscription;

    criteria: BomTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'bomId', 'bomName', 'description', 'userName', 'adate', 'itemsCount', 'udate'];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private lService: SxBomListService,
        private bService: SxBomService,
        public tableds: SxBomTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<Bom>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: BomTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields(): void {
        // this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        if (this.criteria.companyId !== this.cus.company.company_id) {
            this.criteria.companyId = this.cus.company.company_id;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit(): void {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage(): void {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

    spmBomOpen(bom: Bom): void {
        // this.wadService.airportPanelOpen.next(airport);
    }


    ediBom(data: Bom): void {
        console.log('ediBom-> data:', data);
        // console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            data.userId = data.userId ? data.userId : this.cus.user.userId;
            const dialogRef = this.dialogService.open(SxBomEditDialog, {
                width: '721px',
                height: '360px',
                data: {
                    bom: data,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediBom-> res:', res);
                if (res) {
                    // this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(data => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select airport first.', 'Select Airport');
        }


    }

    addBom(): void {
        // console.log('inviteUser-> ');
        const dto = new Bom();
        dto.companyId = this.cus.company.company_id;
        dto.userId = this.cus.user.userId;
        dto.userName = this.cus.user.user_name;
        dto.userEmail = this.cus.user.e_mail;

        this.ediBom(dto);
    }

    gotoBom(data: Bom): void {

        console.log('gotoBom-> data:', data);

        if (data) {
            this.bService.bom = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.items]);
        }

    }

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
    
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
        console.log('gotoAirport-> airport:', data);

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

