import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomListService } from '../bom-list.service';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { Bom, BomTableCriteria, ResponseBomTableData } from 'src/app/@core/services/api/bom/dto';

export const KEY_CRITERIA = 'sx_grid_bom_list_criteria';
@Injectable()
export class SxBomTableDataSource extends DataSource<Bom> {

    selected: Bom;

    private _data: Array<Bom>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<Bom[]> = new BehaviorSubject<Bom[]>([]);
    listSubject = new BehaviorSubject<Bom[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<Bom> {
        return this._data;
    }

    set data(value: Array<Bom>) {
        this._data = value;
        this.listSubject.next(this._data as Bom[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: SxApiBomClient,
        private lService: SxBomListService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<BomTableCriteria>();

    public get criteria(): BomTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: BomTableCriteria;
        if (onjStr) {
            obj = Object.assign(new BomTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new BomTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'macId';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: BomTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<Bom[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<Bom>> {
        // console.log('loadData->');
        // console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;

        return new Observable<Array<Bom>>(subscriber => {

            this.client.bomTable(this.criteria)
                .subscribe((resp: ResponseBomTableData) => {
                    // const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    // console.log('loadData-> resp=', resp);
                    this.data = new Array<Bom>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.boms && resp.data.boms.length > 0) {
                            resp.data.boms.forEach(iu => {
                                const obj = Bom.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount ? resp.data.rowsCount.totalRows : 0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    // console.log('loadData-> data=', this.data);
                    // console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<Bom>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
