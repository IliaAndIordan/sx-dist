import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// -Sx Modules
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxCoreModule } from '../@core/@core.module';
// -Services
import { SxBomService } from './bom.service';
import { SxBomFileImportService } from './file-import/bom-file-import-service';
import { SxBomItemsTableDataSource } from './items/grids/grid-bom-items';
import { SxBomItemsService } from './items/bom-items.service';
import { SxBomListService } from './list/bom-list.service';
import { SxBomTableDataSource } from './list/grids/grid-bom.datasource';
// -Models
import { routedComponents, SxBomRoutingModule } from './bom-routing.module';
import { SxBomMenuMainComponent } from './menu-main/menu-main.component';
import { SxBomItemsFilterPanelBodyComponent } from './items/filter-panel/bom-items-filter-pane.component';
import { SxBomItemsHomeTabGridComponent } from './items/tabs/bom-items-home-tab-grid.component';
import { SxGridBomItemsComponent } from './items/grids/grid-bom-items.component';
import { SxBomFileImportFilterPanelBodyComponent } from './file-import/filter-panel/bom-file-import-filter-pane.component';


@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxPipesModule,
    //
    SxBomRoutingModule,
  ],
  declarations: [
    routedComponents,
    SxBomMenuMainComponent,
    SxBomItemsFilterPanelBodyComponent,
    SxBomItemsHomeTabGridComponent,
    SxGridBomItemsComponent,
    SxBomFileImportFilterPanelBodyComponent,
  ],
  exports: [],
  entryComponents: [
    SxBomMenuMainComponent,
  ],
  providers: [
    SxBomService,
    SxBomListService,
    SxBomTableDataSource,
    SxBomItemsService,
    SxBomItemsTableDataSource,
    SxBomFileImportService,
  ]
})
export class SxBomModule { }
