

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { SxBomService } from '../bom.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';


@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class SxBomDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = COMMON_IMG_LOGO_RED;
    tileImgUrl = URL_COMMON_IMAGE_TILE;
    acTypeImg = URL_COMMON_IMAGE_AIRCRAFT + 'aircraft_0_tableimg.png';
    env: string;
    // loginsCountData: ResponseLoginsCount;
    usersCount: number;
    // loginsLis: LoginsCountModel[];

    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private toastr: ToastrService,
                private spinerService: SpinnerService,
                private cus: CurrentUserService,
                private bService: SxBomService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
    
        this.initFields();


    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields(): void {


        // console.log('loadLoginsCount-> data', resp.data);
    }

    //#region  data

    public loadLoginsCount(): void {
        /*
        this.spinerService.show();
        this.aService.loadLoginsCount()
            .subscribe((list: LoginsCountModel[]) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.spinerService.hide();
                this.initFields();
                /*
                this.loginsLis = list;
                this.usersCount = this.aService.usersCount;
                //console.log('gatGvdtTsoSearchHits-> usersCount', this.usersCount);
                this.chartDataUsers = LoginsCountModel.getLineChartData(this.loginsLis);
                this.chartLabelsUsers = LoginsCountModel.getLineChartLabels(this.loginsLis);
                //console.log('chartLabelsUsers=', this.chartLabelsUsers);
                *
            },
                err => {
                    this.spinerService.hide();
                    console.log('autenticate-> err: ', err);
                });
*/
    }


    //#endregion

    //#region  Actions

    onChartClick(event): void {
        console.log('onChartClick -> event:', event);
    }

    gotoBomList(): void {
        console.log('gotoBomList ->');
        this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.list]);
    }

    gotoManufacturers(): void {
        // this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturers]);
    }

    //#endregion
}
