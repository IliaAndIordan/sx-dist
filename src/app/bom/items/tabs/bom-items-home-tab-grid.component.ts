import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { SxBomService } from '../../bom.service';
import { SxBomItemsService } from '../bom-items.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { Bom } from 'src/app/@core/services/api/bom/dto';


export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'sx-bom-items-home-tab-grid',
    templateUrl: './bom-items-home-tab-grid.component.html',
})
export class SxBomItemsHomeTabGridComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    //@Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    //@Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    @Input() tabIdx: number;
    /**
     * Fields
     */
    bom: Bom;
    bomChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private bService: SxBomService,
        private biService: SxBomItemsService) {

    }


    ngOnInit(): void {

        this.totalCount = 0;

        this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
            this.initFields();
        });


        this.tabIdxChanged = this.biService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.bomChanged) { this.bomChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields(): void {
        this.selTabIdx = this.biService.tabIdx;
        this.bom = this.biService.bom;
    }



    //#region Action Methods

    spmBomOpenClick(value: any): void {

        console.log('spmCountryOpenClick -> value=', value);
        if (value) {
            // this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}