import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { SxBomService } from '../../bom.service';
import { SxBomItemsService } from '../bom-items.service';
// ---Models
import { Bom, BomItem } from 'src/app/@core/services/api/bom/dto';


@Component({
    selector: 'sx-bom-items-home-tabs',
    templateUrl: './bom-items-home-tabs.component.html',
})
export class SxBomItemsHomeTabsComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmBomOpen: EventEmitter<Bom> = new EventEmitter<Bom>();

    /**
     * Fields
     */
    panelIChanged: Subscription;
    selTabIdx: number;
    tabIdxChanged: Subscription;

    bom: Bom;
    bomChanged: Subscription;

    get panelIn(): boolean {
        return this.biService.panelIn;
    }

    set panelIn(value: boolean) {
        this.biService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private biService: SxBomItemsService,
        private bService: SxBomService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.biService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.biService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
            this.initFields();
          });


        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.bomChanged) { this.bomChanged.unsubscribe(); }

    }

    initFields(): void  {
        this.selTabIdx = this.biService.tabIdx;
        this.bom = this.biService.bom;


        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editBomItem(data: BomItem): void  {
        console.log('editBomItem-> data:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number): void  {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.biService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen(): void  {
        this.biService.panelIn = true;
    }

    spmBomOpenClick(): void  {
        if (this.bom) {
            this.spmBomOpen.emit(this.bom);
        }
    }

    //#endregion

}
