import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { SxBomItemsService } from '../bom-items.service';

// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { SxBomService } from '../../bom.service';
import { SxBomEditDialog } from 'src/app/@share/components/dialogs/bom-edit/bom-edit.dialog';
import { ValueTypes } from 'src/app/@share/components/common/side-panel-modal/row/side-panel-row-value.component';


@Component({
    selector: 'sx-bom-items-filter-panel-body',
    templateUrl: './bom-items-filter-pane.component.html',
})
export class SxBomItemsFilterPanelBodyComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmBomOpenClick: EventEmitter<any> = new EventEmitter<any>();
    /**
     * FIELDS
     */
    vtypes = ValueTypes;
    roots = AppRoutes;
    bom: Bom;
    bomChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private bService: SxBomService,
        private biService: SxBomItemsService) {

    }


    ngOnInit(): void {

        this.bomChanged = this.bService.bomChanged.subscribe((ap: Bom) => {
            this.initFields();
        });

        this.tabIdxChanged = this.biService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.bomChanged) { this.bomChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields(): void {
        this.tabIdx = this.biService.tabIdx;
        this.bom = this.biService.bom;
    }

    //#region Dialogs

    ediBom(data: Bom): void {
        console.log('ediBom-> data:', data);
        // console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            data.userId = data.userId ? data.userId : this.cus.user.userId;
            const dialogRef = this.dialogService.open(SxBomEditDialog, {
                width: '721px',
                height: '360px',
                data: {
                    bom: data,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediBom-> res:', res);
                if (res) {
                    // this.table.renderRows();
                    this.bService.bom = res;
                }
                this.initFields();
            });

        }
        else {
            this.toastr.info('Please select BOM first.', 'Select BOM');
        }


    }

    fileUpload(): void {
        console.log('fileUpload-> bom:', this.bom);
        this.bService.bom = this.bom;
        this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.file_select]);
    }

    //#endregion

    //#region SPM

    spmBomOpen(): void {
        this.spmBomOpenClick.emit(this.bom);
    }

    gotoBomList(): void {
        this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.list]);
    }

    //#endregion

}
