

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { SxSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { SxBomService } from '../bom.service';
import { SxBomItemsService } from './bom-items.service';
import { COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';


@Component({
  templateUrl: './bom-items.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class SxBomItemsHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmBom') spmBom: SxSidePanelModalComponent;
  /**
   * Fields
   */
  panelInVar = Animate.in;
  panelInChanged: Subscription;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  bom: Bom;
  bomChanged: Subscription;

  get panelIn(): boolean {
    const rv = this.biService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    // console.log('panelIn-> hps:', this.hps);
    return this.biService.panelIn;
  }

  set panelIn(value: boolean) {
    this.biService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private bService: SxBomService,
    private biService: SxBomItemsService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    const tmp = this.panelIn;
    this.panelInChanged = this.biService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });


    this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
      this.initFields();
    });

    this.bomChanged = this.biService.spmBomOpen.subscribe((open: boolean) => {
      open ? this.spmBomOpen(this.bom) : this.spmBomClose();
    });


    this.initFields();

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.bomChanged) { this.bomChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields(): void {
    this.bom = this.biService.bom;

    // console.log('initFields-> country:', this.country);
  }

  //#region  Actions

  sidebarToggle(): void {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoBom(value: Bom): void {
    if (value) {
      this.biService.bom = value;
      // this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmBomClose(): void {
    if (this.spmBom) {
      this.spmBom.closePanel();
    }
  }

  spmBomOpen(data: Bom): void {

    this.bom = data ? data : this.bom;
    if (this.bom && this.spmBom) {
      this.spmBom.expandPanel();
    }

  }


  //#endregion
}
