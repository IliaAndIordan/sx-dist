import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomService } from '../../bom.service';
import { SxBomItemsService } from '../bom-items.service';
import { SxBomItemsTableDataSource } from './grid-bom-items';
// --- Models
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Bom, BomItem, BomItemTableCriteria, BomTableCriteria, ResponseBomItemDelete } from 'src/app/@core/services/api/bom/dto';
import { SxBomEditDialog } from 'src/app/@share/components/dialogs/bom-edit/bom-edit.dialog';
import { SxBomItemEditDialog } from 'src/app/@share/components/dialogs/bom-item-edit/bom-item-edit.dialog';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { HttpErrorResponse } from '@angular/common/http';



@Component({
    selector: 'sx-grid-bom-items',
    templateUrl: './grid-bom-items.component.html',
})
export class SxGridBomItemsComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<Bom>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    bom: Bom;
    bomChanged: Subscription;
    selected: BomItem;

    criteria: BomItemTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: string;

    dataCount = 0;
    dataChanged: Subscription;
    //  'itemName', 'description', 'distributorCode',
    displayedColumns = ['action', 'bomItemId', 'sequenceNr', 'qauntity', 'upc', 'gtin', 'ean', 'mfrName', 'mfrCatalogCode', 'itemName', 'distributorCode', 'udate'];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private client: SxApiBomClient,
        private biService: SxBomItemsService,
        private bService: SxBomService,
        public tableds: SxBomItemsTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
            this.initFields();
        });
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<BomItem>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: BomItemTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields(): void {
        // this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        if (this.criteria.bomId !== this.bService.bom.bomId) {
            this.criteria.bomId = this.bService.bom.bomId;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit(): void {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage(): void {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

    spmBomItemOpen(data: BomItem): void {
        console.log('spmBomItemOpen-> data:', data);
        // this.wadService.airportPanelOpen.next(airport);
    }


    ediBomItem(data: BomItem): void {
        console.log('ediBomItem-> data:', data);
        // console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const sequenceNr = this.bService.bom.itemsCount ? this.bService.bom.itemsCount + 1 : 1;
            data.bomId = data.bomId ? data.bomId : this.bService.bom.bomId;
            const dialogRef = this.dialogService.open(SxBomItemEditDialog, {
                width: '721px',
                height: '480px',
                data: {
                    bomItem: data,
                    bom: this.bService.bom,
                    nextSequenceNr: sequenceNr
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediBom-> res:', res);
                if (res) {
                    // this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(() => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select airport first.', 'Select Airport');
        }


    }

    addBomItem(): void {
        // console.log('inviteUser-> ');
        const dto = new BomItem();
        const sequenceNr = this.bService.bom.itemsCount ? this.bService.bom.itemsCount + 1 : 1;
        dto.bomId = this.bService.bom.bomId;
        dto.sequenceNr = sequenceNr;

        this.ediBomItem(dto);
    }

    deleteBomItem(data: BomItem): void {
        console.log('deleteBomItem-> data:', data);
        this.client.bomItemDelete(data.bomItemId)
            .subscribe((resp: number) => {
                // console.log('loadData-> resp=', resp);
                this.tableds.loadData()
                    .subscribe(() => {
                        this.initFields();
                    });
            }, (msg: HttpErrorResponse) => {
                console.log('loadData -> ' + msg);
                if (msg && msg.error) {
                    this.toastr.error(msg.error.detail, msg.error.title);
                }
                // component.errorMessage = msg;
            });
    }

    gotoBom(data: Bom): void {

        console.log('gotoBom-> data:', data);

        if (data) {
            this.bService.bom = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.items]);
        }

    }


}

