import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { SxBomItemsService } from '../bom-items.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { Bom, BomItem, BomItemTableCriteria, BomTableCriteria, ResponseBomItemTableData, ResponseBomTableData } from 'src/app/@core/services/api/bom/dto';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

export const KEY_CRITERIA = 'sx_grid_bom_items_criteria';
@Injectable()
export class SxBomItemsTableDataSource extends DataSource<BomItem> {

    selected: BomItem;

    private _data: Array<BomItem>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<BomItem[]> = new BehaviorSubject<BomItem[]>([]);
    listSubject = new BehaviorSubject<BomItem[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<BomItem> {
        return this._data;
    }

    set data(value: Array<BomItem>) {
        this._data = value;
        this.listSubject.next(this._data as BomItem[]);
    }

    constructor(
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private client: SxApiBomClient,
        private biService: SxBomItemsService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<BomItemTableCriteria>();

    public get criteria(): BomItemTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: BomItemTableCriteria;
        if (onjStr) {
            obj = Object.assign(new BomItemTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new BomItemTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'macId';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: BomItemTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<BomItem[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<BomItem>> {
        // console.log('loadData->');
        // console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;

        return new Observable<Array<BomItem>>(subscriber => {

            this.client.bomItemTable(this.criteria)
                .subscribe((resp: ResponseBomItemTableData) => {
                    // const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    // console.log('loadData-> resp=', resp);
                    this.data = new Array<BomItem>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.items && resp.data.items.length > 0) {
                            resp.data.items.forEach(iu => {
                                const obj = BomItem.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount ? resp.data.rowsCount.totalRows : 0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    // console.log('loadData-> data=', this.data);
                    // console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, (msg: HttpErrorResponse) => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<BomItem>();
                    this.isLoading = false;
                    if(msg && msg.error){
                        this.toastr.error(msg.error.detail, msg.error.title);
                    }
                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
