import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AppStore } from 'src/app/@core/const/app-storage.const';
import { DataField, ItemImportColumnModel } from 'src/app/@core/models/common/item-import-column.model';
import { SxBomItemImportModel, SxFileImport } from 'src/app/@core/models/common/tso-file-import.model';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { Bom, BomItem } from 'src/app/@core/services/api/bom/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomService } from '../bom.service';

const ITEM_RAW_DATA_KEY = 'sx-bom-file-import-items-raw-data';
const COLUMNS_MAPPING_KEY = 'sx-bom-file-import-col-mappings';
export const SR_LEFT_PANEL_IN_KEY = 'sx_bom_file_import_tree_panel_in';

@Injectable({ providedIn: 'root', })
export class SxBomFileImportService {

    constructor(
        private cus: CurrentUserService,
        private bService: SxBomService,
        private client: SxApiBomClient) {
    }

    //#region  

    public clearLocalStorage(): void {
        localStorage.removeItem(AppStore.bomFileImportInfo);
    }

    //#endregion

    //#region BomModel


    get bom(): Bom {

        return this.bService.bom;
    }

    set bom(value: Bom) {
        this.bService.bom = value;
    }

    //#endregion

    //#region subregion tree panel

    // tslint:disable-next-line: member-ordering
    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    //#endregion

    //#region file info

    // tslint:disable-next-line: member-ordering
    fileInfoChanged = new Subject<SxFileImport>();

    get fileInfo(): SxFileImport {
        const onjStr = localStorage.getItem(AppStore.bomFileImportInfo);
        let obj: SxFileImport;
        if (onjStr) {
            obj = Object.assign(new SxFileImport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set fileInfo(value: SxFileImport) {
        const oldValue = this.fileInfo;
        if (value) {
            localStorage.setItem(AppStore.bomFileImportInfo, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.bomFileImportInfo);
        }

        this.fileInfoChanged.next(value);
    }

    //#endregion

    //#region rawLineData

    get fileDataRows(): Array<any> {
        const onjStr = localStorage.getItem(ITEM_RAW_DATA_KEY);
        let obj: Array<any>;
        if (onjStr) {
            obj = Object.assign(new Array<any>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set fileDataRows(value: Array<any>) {
        if (value) {
            localStorage.setItem(ITEM_RAW_DATA_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(ITEM_RAW_DATA_KEY);
        }
    }

    gatDataFields(headerRowIndex: number = 0): Array<DataField> {
        console.log('gatDataFields ->  fileDataRows:', this.fileDataRows);
        const columns = new Array<DataField>();
        if (this.fileDataRows && this.fileDataRows.length > headerRowIndex) {
            const header: [] = this.fileDataRows[headerRowIndex];
            console.log('gatDataFields -> header:', header);
            if (header && header.length > 0) {
                for (let idx = 0; idx < header.length; idx++) {
                    const fieldName = header[idx];
                    const df = new DataField();
                    df.idx = idx;
                    df.name = fieldName;
                    df.value = headerRowIndex < this.fileDataRows.length ?
                        this.fileDataRows[headerRowIndex + 1][idx] ?
                            this.fileDataRows[headerRowIndex + 1][idx] : 'EMPTY' : 'END OF FILE';
                    console.log('gatDataFields -> df:', df);
                    columns.push(df);
                }

            }

        }
        console.log('gatDataFields -> columns:', columns);
        return columns;
    }

    //#endregion

    //#region columnMappings


    get columnMappings(): Array<ItemImportColumnModel> {
        const onjStr = localStorage.getItem(COLUMNS_MAPPING_KEY);
        let obj: Array<ItemImportColumnModel>;
        if (onjStr) {
            obj = Object.assign(new Array<ItemImportColumnModel>(), JSON.parse(onjStr));
        }
        return obj;
    }

    set columnMappings(value: Array<ItemImportColumnModel>) {
        if (value) {
            localStorage.setItem(COLUMNS_MAPPING_KEY, JSON.stringify(value));
        } else {
            localStorage.removeItem(COLUMNS_MAPPING_KEY);
        }
    }

    // tslint:disable-next-line: member-ordering
    itemImportData: Array<SxBomItemImportModel>;

    genMfrItemImportArray(): Array<SxBomItemImportModel> {
        this.itemImportData = new Array<SxBomItemImportModel>();
        const columns = this.columnMappings;
        const fileData = this.fileDataRows;
        let canProceed = fileData && fileData.length > 0 && columns && columns.length > 0;
        if (!canProceed) {
            return this.itemImportData;
        }

        const required: ItemImportColumnModel[] = columns.filter(col => col.required && !col.dataField);
        const mapped: ItemImportColumnModel[] = columns.filter(col => col.dataField);
        console.log('mapped -> ', mapped);
        canProceed = required.length === 0 && mapped.length > 0;

        console.log('canProceed -> ', canProceed);
        if (!canProceed) {
            return this.itemImportData;
        }

        for (let idx = 1; idx < fileData.length; idx++) {

            const row: Array<any> = fileData[idx];
            // console.log('row -> ', row);
            const item = this.getBomItemImportModel(row, idx);
            // console.log('item -> ', item);
            this.itemImportData.push(item);
        }

        return this.itemImportData;
    }

    private getStringValue(value: string): string {
        const rv = value.replace(/(^")|("$)/g, '');
        // JSON.stringify(row[col.dataField.idx]) 
        return rv;
    }

    private getBomItemImportModel(row: Array<any>, sequenceId: number): SxBomItemImportModel {

        const item = new SxBomItemImportModel();
        const columns = this.columnMappings;
        item.bomId = this.bom.bomId;
        item.sequenceNr = sequenceId;
        let col = columns.find(x => x.dbColumn === 'sequence_nr');
        if (col && col.dataField) {
            item.sequenceNr = parseInt(row[col.dataField.idx] ? row[col.dataField.idx] : '2', 10);
        }
        col = columns.find(x => x.dbColumn === 'quantity');
        if (col && col.dataField) {

            item.qauntity =  parseInt(row[col.dataField.idx] ? row[col.dataField.idx] : '2', 10);
        }
        col = columns.find(x => x.dbColumn === 'quantityUom');
        if (col && col.dataField) {

            item.quantityUom = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx]) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'upc');
        if (col && col.dataField) {

            item.upc = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx]) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'upc');
        if (col && col.dataField) {

            item.upc = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx]) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'ean');
        if (col && col.dataField) {
            item.ean = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx]) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'gtin');
        if (col && col.dataField) {
            item.gtin = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }
        col = columns.find(x => x.dbColumn === 'manufacturerName');
        if (col && col.dataField) {
            item.mfrName = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx]) : undefined;
        }
        col = columns.find(x => x.dbColumn === 'manufacturerCatalogNumber');
        if (col && col.dataField) {
            item.mfrCatalogCode = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }
        col = columns.find(x => x.dbColumn === 'itemName');
        if (col && col.dataField) {
            item.itemName = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }
        col = columns.find(x => x.dbColumn === 'description');
        if (col && col.dataField) {
            item.description = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }
        col = columns.find(x => x.dbColumn === 'distributor_code');
        if (col && col.dataField) {
            item.distributorCode = row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }
        col = columns.find(x => x.dbColumn === 'notes');
        if (col && col.dataField) {
            item.notes =  row[col.dataField.idx] ? this.getStringValue(row[col.dataField.idx])  : undefined;
        }



        return item;
    }

    //#endregion

    //#region  Data import

    importItems(items: BomItem[]): Observable<any> {

        return new Observable<any>(subscriber => {

            this.client.bomItemImport(this.bom.bomId, items)
                .subscribe((resp: any) => {
                    console.log('importItems -> resp:', resp);
                    // this.spinerService.display(false);
                    // console.log('importItems -> resp:', resp);
                    subscriber.next(resp);
                });
            subscriber.next(undefined);
        });

    }
    //#endregion

}
