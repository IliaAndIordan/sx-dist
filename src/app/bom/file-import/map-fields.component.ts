import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDrag, CdkDragDrop, CdkDropList, } from '@angular/cdk/drag-drop';
import { ToastrService } from 'ngx-toastr';
// -Services
import { SxBomService } from '../bom.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxBomItemImportModel, SxFileImport } from 'src/app/@core/models/common/tso-file-import.model';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { environment } from 'src/environments/environment';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { SxBomFileImportService } from './bom-file-import-service';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';
import { DataField, ItemColumnsOpt, ItemImportColumnModel } from 'src/app/@core/models/common/item-import-column.model';

@Component({
    templateUrl: './map-fields.component.html',
})
export class SxBomFileImportMapFieldsComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    dbColumnsOpt = ItemColumnsOpt;
    env: string;

    bom: Bom;
    company: CompanyModel;
    fileInfo: SxFileImport;
    dataFields: DataField[];
    columsIds: Array<string>;
    isValid = true;
    isUploading: boolean;
    fileDataRows: Array<any>;

    hasSpinner = false;
    errorMessage: string;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private toastr: ToastrService,
                private spinerService: SpinnerService,
                private importService: SxBomFileImportService,
                private cus: CurrentUserService,
                private bService: SxBomService) {

    }


    ngOnInit(): void {
        this.spinerService.display(true);
        this.env = environment.abreviation;
        this.isUploading = false;
        this.initFields();
    }

    ngOnDestroy(): void {
    }

    initFields(): void{
        this.bom = this.importService.bom;
        this.company = this.cus.company;
        this.fileDataRows = this.importService.fileDataRows;
        this.fileInfo = this.importService.fileInfo;

        this.columsIds = new Array<string>();
        this.dbColumnsOpt = new Array<ItemImportColumnModel>();
        ItemColumnsOpt.forEach(element => {
            element.dataField = undefined;
            this.dbColumnsOpt.push(element);
            this.columsIds.push(element.dbColumn);
        });
        this.dataFields = this.importService.gatDataFields();
        this.fileInfo.dataFields = this.dataFields;
        this.fileInfo.fieldsCount = this.dataFields ? this.dataFields.length : 0;
        this.importService.fileInfo = this.fileInfo;
        this.spinerService.display(false);
        this.verifyIsValid();
    }

    verifyIsValid(): void {
        const required: ItemImportColumnModel[] = this.dbColumnsOpt.filter(col => col.required && !col.dataField);
        this.isValid = required.length === 0;
        console.log('verifyIsValid -> isValid: ', this.isValid);
    }

    //#region Actions

    drop(event: CdkDragDrop<any>): void {
        //  console.log('drop -> event:', event);
        const df = event.item.data as DataField;
        const dbColumn = event.container.data ? event.container.data[0] : undefined;
        // console.log('drop -> dbColumn:', dbColumn);
        // console.log('drop -> previousContainer:', event.previousContainer.data);
        // console.log('drop -> container:', event.container.data);
        if (event.previousContainer === event.container) {
            // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            if (df && dbColumn) {
                const fIdx = this.dataFields.findIndex(x => x.idx === df.idx);
                this.dataFields.splice(fIdx, 1);
                if (dbColumn.dataField) {
                    this.dataFields.push(dbColumn.dataField);
                }
                // this.dataFields = this.dataFields.filter(x => x.idx !== df.idx);
                dbColumn.dataField = df;
                console.log('drop -> dbColumn 2:', dbColumn);
            }
        }
        this.verifyIsValid();
    }

    deleteField(field: DataField): void {
        // console.log('deleteField-> field:', field);
        if (field) {
            this.dataFields.splice(field.idx, 1);
        }
        this.verifyIsValid();
    }

    deleteDataField(column: ItemImportColumnModel): void {
        console.log('deleteDataField-> column:', column);
        if (column && column.dataField) {
            this.dataFields.splice(column.dataField.idx, 0, column.dataField);
        }
        column.dataField = undefined;
        this.verifyIsValid();
    }



    /** Predicate function that only allows even numbers to be dropped into a list. */
    evenPredicate(item: CdkDrag<ItemImportColumnModel>, list: CdkDropList): boolean {
        const dataField = list.data[0].dataField;
        const rv = !dataField ? true : false;
        return rv;
    }

    importItemsClick(): void {
        console.log('importItemsClick-> dbColumnsOpt:', this.dbColumnsOpt);
        this.importService.columnMappings = this.dbColumnsOpt;
        const fi = this.importService.fileInfo;
        const data: Array<SxBomItemImportModel> = this.importService.genMfrItemImportArray();
        console.log('importItemsClick-> data:', data);
        
        if (data && data.length > 0 && this.bom) {
            this.spinerService.message.next('Importing ' + data.length.toString() + ' items i bom No ' + this.bom.bomId);
            this.spinerService.display(true);
            this.importService.importItems(data)
                .subscribe((resp: any) => {
                    console.log('importItems -> resp:', resp);
                    this.spinerService.display(false);
                    this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.items]);
                }, msg => {
                    console.log('importItems -> ' + msg);
                    this.spinerService.message.next('Error ' + msg);
                    this.spinerService.display(false);
                    // component.errorMessage = msg;
                });
        }

        //fi.dataFields = this.dbColumnsOpt && this.dbColumnsOpt.length > 0 ? this.dbColumnsOpt.length : 0;
        //this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ItemsImportResult]);
    }


    //#endregion
}