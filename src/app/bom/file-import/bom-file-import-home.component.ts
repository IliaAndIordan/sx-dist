import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
// Services
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SpinnerService } from '../../@core/services/spinner.service';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from '../../@core/const/app-routes.const';
import { Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';
import { environment } from 'src/environments/environment';
import { SxBomFileImportService } from './bom-file-import-service';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';
import { SxBomService } from '../bom.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxFileImport } from 'src/app/@core/models/common/tso-file-import.model';

@Component({
    templateUrl: './bom-file-import-home.component.html',
    animations: [PageTransition,
        ShowHideTriggerBlock,
        ShowHideTriggerFlex,
        SpinExpandIconTrigger,
        ExpandTab, TogleBtnTopRev]
})
export class SxBomFileImportHomeComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    panelInVar = Animate.in;
    panelInChanged: Subscription;
    sidebarClass = 'sidebar-closed';

    env: string;

    bom: Bom;
    bomChanged: Subscription;

    get panelIn(): boolean {
        const rv = this.importService.panelIn;
        this.panelInVar = rv ? Animate.out : Animate.in;
        // console.log('panelIn-> hps:', this.hps);
        return this.importService.panelIn;
    }

    set panelIn(value: boolean) {
        this.importService.panelIn = value;
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    }

    constructor(private router: Router,
        private route: ActivatedRoute,
        private spinerService: SpinnerService,
        private bService: SxBomService,
        private importService: SxBomFileImportService,
        private cus: CurrentUserService) {

    }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
            this.initFields();
        });

        this.panelInChanged = this.importService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });


        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
        if (this.bomChanged) { this.bomChanged.unsubscribe(); }
    }

    initFields(): void {
        this.bom = this.importService.bom;
    }

    //#region  Actions

    sidebarToggle(): void {
        this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
    }

    //#region Tree Panel

    public closePanelLeftClick(): void {
        this.panelIn = false;
    }

    public expandPanelLeftClick(): void {
        // this.treeComponent.openPanelClick();
        this.panelIn = true;
    }

    //#endregion

    //#endregion

    //#region SPM

    spmBomOpen(): void {

    }

    //#endregion
}
