import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';
// Services
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SpinnerService } from '../../@core/services/spinner.service';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from '../../@core/const/app-routes.const';
import { Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';
import { environment } from 'src/environments/environment';
import { SxBomFileImportService } from './bom-file-import-service';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';
import { SxBomService } from '../bom.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxFileImport } from 'src/app/@core/models/common/tso-file-import.model';

@Component({
    templateUrl: './select-file.component.html',
    animations: [PageTransition,
        ShowHideTriggerBlock,
        ShowHideTriggerFlex,
        SpinExpandIconTrigger,
        ExpandTab, TogleBtnTopRev]
})
export class SxBomFileImportSelectFileComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    env: string;

    bom: Bom;
    bomChanged: Subscription;

    fgSelectFile: FormGroup;
    itemsData: Array<any>;
    company: CompanyModel;
    file: any;
    selFileName: string;
    selFileSize: number;
    isUploading: boolean;

    hasSpinner = false;
    errorMessage: string;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private fb: FormBuilder,
                private toastr: ToastrService,
                private spinerService: SpinnerService,
                private bService: SxBomService,
                private importService: SxBomFileImportService,
                private cus: CurrentUserService) {

    }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.isUploading = false;
        this.fgSelectFile = this.fb.group({
            priceFileIn: ['', Validators.required],
        });

        this.importService.fileInfo = undefined;
        this.importService.fileDataRows = undefined;

        this.fgSelectFile.updateValueAndValidity();

        this.bomChanged = this.bService.bomChanged.subscribe((bom: Bom) => {
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.bomChanged) { this.bomChanged.unsubscribe(); }
    }

    initFields(): void {
        this.bom = this.importService.bom;
        this.company = this.cus.company;
    }

    //#region file select methods

    get priceFileIn(): AbstractControl { return this.fgSelectFile.get('priceFileIn'); }

    fError(fname: string): string {
        const field = this.fgSelectFile.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    priceFileChange(event: any): void {
        // console.log('priceFileChange-> event=', event);
        const file = event.target.files[0]; // <--- File Object for future use.
        if (file) {
            this.spinerService.show();
            // this.spinerService.setMessage('Loading file ...');
            this.file = file;
            // console.log('priceFileChange-> file=', this.file);
            const fnameExt: string = file.name.replace('C:\\fakepath\\', '');
            // console.log('priceFileChange-> fnameExt=', fnameExt);
            this.checkFile(this.file)
                .then(res => {
                    this.selFileName = fnameExt;
                    this.selFileSize = this.file.size;
                    const fs = new SxFileImport();
                    fs.name = this.selFileName;
                    fs.size = this.selFileSize;

                    fs.rows = this.itemsData && this.itemsData.length > 0 ? this.itemsData.length : 0;
                    this.importService.fileDataRows = this.itemsData;
                    this.importService.fileInfo = fs;
                    // console.log('this.selFileName: ', this.selFileName);
                    this.priceFileIn.setValue(this.file);
                    this.fgSelectFile.updateValueAndValidity();
                    if (this.fgSelectFile.valid) {
                    }
                    this.spinerService.hide();
                }, msg => {
                    this.toastr.error(msg);
                    console.log('fileUploadChange: msg:', msg);
                    this.spinerService.hide();
                })
                .catch(err => {
                    this.toastr.error('ERROR importing file: ', file.name);
                    console.log('fileUploadChange: err:', err);
                    this.spinerService.hide();
                });

        }
    }

    checkFile(file: any): Promise<void> {
        const promise = new Promise<void>((resolve, reject) => {
            if (file) {
                this.hasSpinner = true;
                const reader: FileReader = new FileReader();
                reader.readAsBinaryString(file);

                reader.onload = (e: any) => {
                    /* read workbook */
                    const bstr: string = e.target.result;
                    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
                    /* grab first sheet */
                    const wsname: string = wb.SheetNames[0];
                    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
                    /* save data from first sheet*/
                    this.itemsData = new Array<any>();
                    const data: Array<any> = XLSX.utils.sheet_to_json(ws, { header: 1, raw: true });

                    if (data && data.length > 0) {
                        data.forEach(element => {
                            if (element && element.length > 0) {
                                this.itemsData.push(element);
                            }
                        });
                    }
                    console.log('checkFile-> itemsData:', this.itemsData);
                    this.hasSpinner = false;
                    resolve();
                };
            }
        });

        return promise;
    }

    get isImportReady(): boolean {
        return this.itemsData && this.itemsData.length > 0 ? true : false;
    }

    onSubmit(): void {

        if (this.fgSelectFile.valid) {

            this.errorMessage = undefined;
            this.router.navigate([AppRoutes.bom, AppRoutes.file_select, AppRoutes.map_fields]);
            /*
            if(this.company && this.company.company_type_id === CompanyType.Manufacturer){
                this.mfrIService.selCompany = this.company;
                this.mfrIService.rawItemData = this.itemsData;
                this.router.navigate([AppRoutes.Root, AppRoutes.Manufacturer, AppRoutes.ImportItems]);
            }*/
            //this.dialogRef.close(this.itemsData);
            /*
            this.preloader.show();
            this.isUploading = true;
            const company_id = this.company.company_id;

            console.log('onSubmit -> company_id:', company_id);

            this.priceFileclient.priceFileUpload(company_id, this.file)
                .then((resp: ResponcePriceFileUpload) => {
                    // console.log('priceFileUpload -> resp:', resp);
                    this.isUploading = false;
                    this.preloader.hide();
                    if (resp && resp.success) {
                        this.toastr.success('Price file has been received and scheduled for processing.', 'Price File Uploads');
                        this.dialogRef.close(resp);
                    }

                }, msg => {
                    this.preloader.hide();
                    this.errorMessage = msg;
                    this.toastr.error(msg);
                    this.isUploading = false;
                })
                .catch(ex => {
                    this.preloader.hide();
                    this.errorMessage = ex.errorMessage;
                    this.toastr.error(ex);
                    this.isUploading = false;
                });
                */
        }
    }


    //#endregion

}
