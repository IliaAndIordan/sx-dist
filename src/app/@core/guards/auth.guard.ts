import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad } from '@angular/router';
import { AppRoutes } from '../const/app-routes.const';
import { CurrentUserService } from '../services/auth/current-user.service';
import { TokenService } from '../services/auth/token.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router,
              private cus: CurrentUserService,
              private tokenService: TokenService) { }

  canLoad(route: Route): boolean  {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    if (this.cus.isAuthenticated && this.cus.user) {
      return true;
    }

    this.router.navigate(['/', AppRoutes.public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    return this.canActivate(route, state);
  }
}
