
export const PageViewType = {
    Card: 'card',
    List: 'list',
    Table: 'table'
};

export const AppRoutes = {
    Root: '',
    // -Baase
    public: 'public',
    login: 'login',
    profile: 'profile',
    company: 'company',
    distributor: 'distributor',
    bom: 'bom',
    // -- Admin
    users: 'user',
    // ----- Sub Roots
    list: 'list',
    items: 'items',
    file_select: 'file-select',
    map_fields: 'map-fields',
};
