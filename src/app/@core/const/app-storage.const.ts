import { environment } from 'src/environments/environment';

export const AppStore = {
    location: 'sx_curr_user_location',
    settings: 'sx_curr_user_settings',
    bom: 'sx_bom',
    bomFileImportInfo: 'sx-bom-import-file-info'
};

export const SessionStore = {
    BarerToken: 'sx_barer_token_key',
    RefreshToken: 'sx_refresh_token_key',
    user: 'sx_current_user',
    comany: 'sx_curr_user_company',
    products: 'sx_curr_user_products',
};

export const Wide = 'image-wide';
export const Tall = 'image-tall';

export const HOUR_MS = (1 * 60 * 60 * 1000);
export const DAY_MS = (24 * HOUR_MS);

export const PAGE_SIZE_OPTIONS = [12, 25, 50, 100, 200];

export const GMAP_API_KEY = 'AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc';


export const AVATAR_IMG_URL = '/assets/images/common/noimage.png';
export const NO_IMG_URL = '/assets/images/common/noimage.png';
export const MOBILE_MEDIAQUERY = 'screen and (max-width: 599px)';
export const TABLET_MEDIAQUERY = 'screen and (min-width: 600px) and (max-width: 959px)';
export const MONITOR_MEDIAQUERY = 'screen and (min-width: 960px)';

export const URL_COMMON = 'https://common.iordanov.info/';
export const URL_COMMON_IMAGES = URL_COMMON + 'images/';
export const URL_COMMON_IMAGES_COMPANY = URL_COMMON_IMAGES + 'company/';
export const URL_COMMON_IMAGE_AMS = URL_COMMON_IMAGES + 'ams/';
export const URL_COMMON_IMAGE_AMS_COMMON = URL_COMMON_IMAGE_AMS + 'common/';
export const URL_COMMON_IMAGE_TILE = URL_COMMON_IMAGES + 'tile/';
export const URL_COMMON_IMAGE_SX = URL_COMMON_IMAGES + 'sx/';
export const URL_COMMON_IMAGE_FLAGS = URL_COMMON_IMAGES + 'flags/';
export const URL_COMMON_IMAGE_COMMODITY = URL_COMMON_IMAGES + 'commodity/';
export const URL_COMMON_IMAGE_AIRCRAFT = URL_COMMON_IMAGES + 'aircraft/';

export const COMMON_IMG_LOGO_RED = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_LOGO_CERCLE = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_AVATAR = URL_COMMON_IMAGES + 'iziordanov_sd_128_bw.png';

export const COMMON_IMG_LOGO_IZIORDAN = URL_COMMON_IMAGES + 'iziordanov_logo.png';

export const URL_NO_IMG = 'assets/images/common/noimage.png';
export const URL_NO_IMG_SQ = 'assets/images/common/noimage-sq.jpg';

export const URL_IMG_IATA = URL_COMMON_IMAGE_AMS_COMMON + 'iata.png';
export const URL_IMG_ICAO = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
export const URL_IMG_WIKI = URL_COMMON_IMAGE_AMS_COMMON + 'wikipedia.ico';
export const URL_IMG_OUAP = URL_COMMON_IMAGE_AMS_COMMON + 'ourairports.png';
export const URL_IMG_AP = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';

export const WEB_OURAP_BASE_URL = 'http://ourairports.com/';
export const WEB_OURAP_AP = WEB_OURAP_BASE_URL + 'airports/';
export const WEB_OURAP_REGION = WEB_OURAP_BASE_URL + 'countries/';
