import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of, Subscription, throwError } from 'rxjs';


// Services
import { ApiBaseClient } from '../api-base.client';
import { CurrentUserService } from '../current-user.service';
import { TokenService } from '../token.service';
// Models
import { UserModel, ResponseAuthenticate, LatLng, ResponseUserSave } from './dto';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { LocalUserSettings } from 'src/app/@core/models/app/local-user-settings';
import { CompanyModel, CompanyProductModel } from 'src/app/@core/models/common/sx-company.model';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY } from '@angular/cdk/a11y';


@Injectable()
export class ApiAuthClient extends ApiBaseClient {

  constructor(
    private router: Router,
    private client: HttpClient,
    private tokenService: TokenService,
    private cus: CurrentUserService) {
    super();

  }

  private get baseUrl(): string {
    return environment.services.url.auth;
  }

  //#region JWT Tocken

  autenticate(name: string, pwd: string, latlon?: LatLng): Observable<any> {
    console.log('user: ' + name + ', ped: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'login',
      { username: name, password: pwd, location: latlon },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => {
          const resp: ResponseAuthenticate = Object.assign(new ResponseAuthenticate(), responce.body);
          console.log('resp:', resp);
          // console.log('success:', resp.success);
          if (resp.success) {
            return this.convertAutenticateToUser(responce);
          }
          else {
            return resp;
          }
        }),
        catchError(this.handleError)
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj;
    let userSettings: LocalUserSettings;
    let token;
    let rv: ResponseAuthenticate;
    const products = new Array<CompanyProductModel>();
    // console.log('response:', response);
    if (response) {

      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }
      rv = Object.assign(new ResponseAuthenticate(), response.body);

      // console.log('convertAutenticateToUser ResponseAuthenticate:', rv);
      if (rv && rv.success) {

        if (rv.data && rv.data.current_user) {
          userObj = UserModel.fromJSON(rv.data.current_user);
          // console.log('convertAutenticateToUser -> userObj:', userObj);
        }

        if (rv.data && rv.data.company) {
          this.cus.company = CompanyModel.fromJSON(rv.data.company);
          // console.log('convertAutenticateToUser -> userObj:', userObj);
        }

        if (userObj && rv.data.products && rv.data.products.length > 0) {
          rv.data.products.forEach(el => {
            const obj = CompanyProductModel.fromJSON(el);
            console.log('products-> obj=', obj);
            products.push(obj);
          });

          // console.log('convertAutenticateToUser -> products:', products);
        }

        if (rv.data.settings) {
          userSettings = LocalUserSettings.fromJSON(rv.data.settings);
          // console.log(' User Settings: ', userSettings);
        }
      }


      if (token) {
        if (rv && rv.success) {
          this.tokenService.barerToken = token;
          this.cus.user = userObj;
          this.cus.localSettings = userSettings;
          console.log('decodeToken:', this.tokenService.decodeToken());
        }

      }

    }

    // console.dir(retvalue);
    return rv;
  }

  refreshToken(): Observable<any> {

    console.log('refreshToken -> ');
    console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
    console.log('refreshToken -> refreshToken:', this.tokenService.refreshToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'jwtrefresh',
      { refresh: this.tokenService.refreshToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          const ra: ResponseAuthenticate = this.convertAutenticateToUser(response);
          console.log('refreshToken -> ResponseAuthenticate:', ra);
          console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
          return this.tokenService.barerToken;
        }),
        catchError(this.handleError)
      );
  }

  logout(): Observable<any> {
    console.log('logout -> ');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/authenticate',
      { provider: 'logout' },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          console.log('logout -> responce=', response);
          this.tokenService.clearToken();
          this.cus.user = undefined;
          this.cus.clearLocalStorage();
          this.cus.clearSession();
          this.router.navigate(['/', AppRoutes.public]);
          return of(response);
        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
  }

  register(mail: string, pwd: string, latlon?: LatLng): Observable<any> {
    console.log('e-mail: ' + mail + ', psw: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/register',
      { email: mail, password: pwd, location: latlon },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => {
          if (responce) {
            const resp: ResponseAuthenticate = Object.assign(new ResponseAuthenticate(), responce.body);
            console.log('resp:', resp);
            console.log('success:', resp.success);
            if (resp.success) {
              this.convertAutenticateToUser(responce);
            }
            else {
              return resp;
            }
          }

        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  userSave(value: UserModel, location?: LatLng): Observable<UserModel> {

    if (!location) {
      location = new LatLng(-1, -1, -1);
    }
    location.action = "Updated from " + this.cus.user.userId.toString();

    value.ip_address = JSON.stringify(location);
    console.log('userSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.baseUrl + 'user_save',
      { user: value },
      { headers: hdrs }).pipe(
        map((response: ResponseUserSave) => {
          console.log('userSave-> response=', response);
          const res = Object.assign(new ResponseUserSave(), response);
          console.log('companySave-> res=', res);
          let rv: UserModel;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.user) {
              rv = UserModel.fromJSON(res.data.user);
            }
          }
          console.log('companySave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap Project save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  //#endregion


}
