import { ICompanyModel, ICompanyProductModel } from 'src/app/@core/models/common/sx-company.model';
import { ILocalUserSettings } from 'src/app/@core/models/app/local-user-settings';
import { UserRole } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ResponseModel } from 'src/app/@core/models/common/response.model';



export class RefreshTokenModel {
    client_id: number;
    exp: number;
    iss: string;
    guid: string;
}

export class UserModel {

    public userId: number;
    public user_name: string;
    public e_mail: string;
    public u_password: string;
    public company_id: number;
    public user_role: UserRole;
    public is_receive_emails: boolean;
    public ip_address: string;
    public udate: Date;
    public adate: Date;

    company_name?: string;
    branch_code?: string;
    company_type_id?: number;
    country_id?: number;

    public static fromJSON(json: IUserModel): UserModel {
        const vs = Object.create(UserModel.prototype);
        return Object.assign(vs, json, {
            company_id: !json.company_id || json.company_id === null ? undefined : json.company_id,
            userId: json.user_id ? json.user_id : json.userId ? json.userId : undefined,
            user_role: json.role ? json.role as UserRole : UserRole.User,
            is_receive_emails: json.is_receive_emails && json.is_receive_emails > 0 ? true : false,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static fromTimestampJSON(json: IUserTimestampModel): UserModel {
        const vs = Object.create(UserModel.prototype);
        return Object.assign(vs, json, {
            company_id: !json.company_id || json.company_id === null ? undefined : json.company_id,
            user_role: json.user_role ? json.user_role as UserRole : UserRole.User,
            is_receive_emails: json.is_receive_emails && json.is_receive_emails > 0 ? true : false,
            adate: (json && json.adate) ? new Date().setTime(json.adate) : undefined,
            udate: (json && json.udate) ? new Date().setTime(json.udate) : undefined
        });
    }
    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? UserModel.fromJSON(value) : value;
    }
    /*
    public toJSON(): IUserModel {
    
        return Object.assign({}, this, {
            is_receive_emails:this.is_receive_emails?1:0;
            adate: this.adate ? (this.adate instanceof Date ?
                this.adate.toJSON() : undefined) : undefined,
            udate: this.udate ?  (this.udate instanceof Date ?
                this.udate.toJSON() : undefined) : undefined,
        });

    }
    */
}

export interface IUserModel {

    user_id: number;
    user_name: string;
    e_mail: string;
    u_password: string;
    company_id: number;
    role: number;
    is_receive_emails: number;
    ip_address: string;
    adate: string;
    udate: string;
    userId?: number;

}

export interface ResponseAuthenticateUser {
    current_user: IUserModel;
    company: ICompanyModel;
    settings: ILocalUserSettings;
    products?: Array<ICompanyProductModel>;
}

export class ResponseAuthenticate extends ResponseModel {
    data: ResponseAuthenticateUser;
}

export class LatLng {
    constructor(latitude: number, longitude: number, altitude?: number) {
        this.lat = latitude;
        this.lng = longitude;
        this.alt = altitude;
    }
    lat: number;
    lng: number;
    alt?: number;
    ipAddress?: string;
    action?: string;
    time?: string;
}


export class ResponseGetUserDatao {
    public user: IUserModel[];
}

interface ResponseGetUserData {
    user: IUserModel;
}

export class ResponseGetUser extends ResponseModel {
    data: ResponseGetUserData;
}


export class ResponseUserSaveData {
    user: IUserModel;

}

export class ResponseUserSave extends ResponseModel {
    data: ResponseUserSaveData;
}



export interface IUserTimestampModel {

    user_id: number;
    user_name: string;
    e_mail: string;
    u_password: string;
    company_id: number;
    user_role: number;
    is_receive_emails: number;
    ip_address: string;
    adate: number;
    udate: number;
    userName: string;

    company_name?: string;
    branch_code?: string;
    company_type_id?: number;
    country_id?: number;
}

export class RespCompanyUsersData {
    users: IUserTimestampModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyUsers extends ResponseModel {
    data: RespCompanyUsersData;
}

