import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { catchError, switchMap, filter, take } from 'rxjs/operators';
// Services
import { TokenService } from './token.service';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { ApiAuthClient } from './api/api-auth.client';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(
        public authClient: ApiAuthClient,
        private tokenService: TokenService,
        private jwtInterceptor: JwtInterceptor) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('intercept ->');

        if (this.jwtInterceptor.isAllowedDomain(request) &&
            !this.jwtInterceptor.isDisallowedRoute(request)) {

            const accessExpired = this.tokenService.isAccessTokenExpired();
            const refreshExpired = this.tokenService.isRefreshTokenExpired();
            // console.log('intercept -> refreshExpired:', refreshExpired);
            const isRefreshCall = this.isRefreshCall(request);
            // console.log('intercept -> accessExpired:', accessExpired);
            // console.log('intercept -> isRefreshCall:', isRefreshCall);

            if (isRefreshCall || !accessExpired || refreshExpired) {

                return next.handle(request).pipe(
                    catchError((err) => {
                        // console.log('intercept -> catchError err', err);
                        if (err instanceof HttpErrorResponse) {
                            const errorResponse = err as HttpErrorResponse;
                            // && errorResponse.error.message === 'Expired JWT Token') {
                            if (errorResponse.status === 401 &&
                                errorResponse.error &&
                                errorResponse.error.message) {
                                    this.tokenService.refreshToken = errorResponse.error.message;
                                console.log('intercept -> refreshToken', this.tokenService.decode(this.tokenService.refreshToken));
                                return this.handle401Error(request, next);
                            } else {
                                return throwError(err);
                            }


                        } else {
                            return throwError(err);
                        }
                    }));
            } else {

                if (!this.isRefreshing) {
                    this.isRefreshing = true;
                    console.log('refreshToken -> ');
                    // this.refreshTokenSubject.next(null);
                    return this.authClient.refreshToken().pipe(
                        switchMap((res: any) => {
                            console.log('refreshToken -> res', res);
                            this.isRefreshing = false;
                            this.refreshTokenSubject.next(this.tokenService.barerToken);
                            return this.jwtInterceptor.intercept(request, next);
                        }),
                        catchError(err => {
                            this.isRefreshing = false;
                            return throwError(err);
                        }));
                } else {
                    return this.refreshTokenSubject.pipe(
                        filter(result => result !== null),
                        take(1),
                        switchMap((res) => {
                            return this.jwtInterceptor.intercept(request, next);
                        }));
                }
            }

        } else {
            return next.handle(request);
        }
    }

    isRefreshCall(request: HttpRequest<any>): boolean {
        let rv = false;
        rv = ((request.url.indexOf('jwtrefresh') > -1) ||
            (request.url.indexOf('login') > -1)) ? true : false;
        return rv;
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        // console.log('handle401Error -> request', request);
        // console.log('handle401Error -> isRefreshing', this.isRefreshing);
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authClient.refreshToken().pipe(
                switchMap((token: any) => {
                    console.log('handle401Error -> token', token);
                    this.isRefreshing = false;
                    this.refreshTokenSubject.next(this.tokenService.barerToken);
                    return this.jwtInterceptor.intercept(request, next);
                }),
                catchError(err => {
                    this.isRefreshing = false;
                    return throwError(err);
                }));

        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(jwt => {
                    return this.jwtInterceptor.intercept(request, next);
                }));
        }
    }

}
