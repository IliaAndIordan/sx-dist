import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, of, Subject } from 'rxjs';
// --- Services
import { GravatarService } from '@infinitycube/gravatar';
// Models
import { UserModel, LatLng } from './api/dto';
import { TokenService } from './token.service';
import { AppStore, SessionStore, URL_NO_IMG_SQ } from '../../const/app-storage.const';
import { UserRole } from '../../models/pipes/sx-user-role.pipe';
import { CompanyType } from '../../models/pipes/sx-company-type.pipe';
import { CompanyModel, CompanyProductModel } from '../../models/common/sx-company.model';
import { LocalUserSettings } from '../../models/app/local-user-settings';
import { SxProjectModel } from '../api/project/dto';

@Injectable()
export class CurrentUserService {

    currHubAirportSubject = new BehaviorSubject<any>(null);

    constructor(
        private tockenService: TokenService,
        private gravatarService: GravatarService) {
    }


    //#region Location Geo

    getLocation(refresh = false): Observable<LatLng> {
        return new Observable(obs => {
            const str = sessionStorage.getItem(AppStore.location);
            if (!str || refresh) {
                navigator.geolocation.getCurrentPosition(
                    pos => {
                        const rv = new LatLng(pos.coords.latitude, pos.coords.longitude);
                        sessionStorage.setItem(AppStore.location, JSON.stringify(rv));
                        obs.next(rv);
                        obs.complete();
                    },
                    error => {
                        console.log('error', error);
                        const rv = new LatLng(42.704198, 23.2840454);
                        obs.next(rv);
                        obs.complete();
                    }
                );
            } else {
                const rv = JSON.parse(str) as LatLng;
                obs.next(rv);
                obs.complete();
            }

        });
    }

    clearLocation(): void {

        sessionStorage.removeItem(AppStore.location);
    }

    //#endregion

    //#region User

    // tslint:disable-next-line: member-ordering
    userChanged = new Subject<UserModel>();

    get user(): UserModel {
        const userStr = sessionStorage.getItem(SessionStore.user);
        let userObj: UserModel;
        // console.log('userStr: ', userStr);
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        // console.log('userObj: ', userObj);
        return userObj;
    }

    set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(SessionStore.user, JSON.stringify(userObj));
        } else {
            //sessionStorage.removeItem(SessionStore.user);
            //this.clearLocalStorage();
            //this.clearLocation();
        }
        this.userChanged.next(userObj);
    }

    get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj) ? true : false;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            // console.log('isLogged user: ', userObj);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (userObj && userObj.user_role === UserRole.Admin);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }

    get isEditor(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        if (userObj) {
            retValue = (userObj.user_role === UserRole.Editor);
        }
        // console.log('isCompanyAdmin: ' + retValue);
        return retValue;
    }

    get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    get avatarUrl(): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (this.user && this.user.e_mail) {

            const gravatarImgUrl = this.gravatarService.url(userObj.e_mail, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    getAvatarUrl(e_mail: string): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (e_mail) {

            const gravatarImgUrl = this.gravatarService.url(e_mail, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    clearLocalStorage(): void {
        console.log('clearLocalStorage ->');
        Object.keys(AppStore).forEach((key: string) => {
            if (AppStore.hasOwnProperty(key)) {
                const val = (AppStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                localStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    clearSession(): void {
        console.log('clearSession ->');
        Object.keys(SessionStore).forEach((key: string) => {
            if (SessionStore.hasOwnProperty(key)) {
                const val = (SessionStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                sessionStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    logout(): Observable<boolean> {
        console.log('cus.logout ->');
        return new Observable<boolean>(subscriber => {
            this.tockenService.clearToken();
            this.clearLocalStorage();
            this.clearSession();
            this.user = undefined;
            subscriber.next(true);
        });

    }
    //#endregion

    //#region Local User Settings

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(AppStore.settings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(AppStore.settings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(AppStore.settings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AppStore.settings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion


    //#region CompanyModel

    // tslint:disable-next-line: member-ordering
    companyChanged = new Subject<CompanyModel>();

    get company(): CompanyModel {
        const objStr = sessionStorage.getItem(SessionStore.comany);
        let obj: CompanyModel;
        if (objStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(objStr));
        }
        // console.log('company-> obj', obj);
        return obj;
    }

    set company(value: CompanyModel) {
        if (value) {
            sessionStorage.setItem(SessionStore.comany, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(SessionStore.comany);
        }
        this.companyChanged.next(value);
    }

    get isDistributor(): boolean {
        let retValue = false;
        const company: CompanyModel = this.company;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (company && company.company_type_id === CompanyType.Distributor);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }

    get isContractor(): boolean {
        let retValue = false;
        const company: CompanyModel = this.company;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (company && company.company_type_id === CompanyType.Contractor);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }

    get isManufacturer(): boolean {
        let retValue = false;
        const company: CompanyModel = this.company;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (company && company.company_type_id === CompanyType.Manufacturer);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }
    
    //#endregion

    //#region Company Products

    // tslint:disable-next-line: member-ordering
    productsChanged = new Subject<Array<CompanyProductModel>>();

    get products(): Array<CompanyProductModel> {
        const objStr = sessionStorage.getItem(SessionStore.products);
        let obj: Array<CompanyProductModel>;
        if (objStr) {
            obj = Object.assign(new Array<CompanyProductModel>(), JSON.parse(objStr));
        }
        return obj;
    }

    set products(value: Array<CompanyProductModel>) {
        const oldValue = this.products;
        if (value) {
            sessionStorage.setItem(SessionStore.products, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(SessionStore.products);
        }

        this.productsChanged.next(value);
    }

    //#endregion


    //#region Airports



    //#endregion
}
