import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AppStore, SessionStore } from '../../const/app-storage.const';
import { RefreshTokenModel } from './api/dto';
import { TokenFactory } from './token-factory';

@Injectable()
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    const retVal = this.barerToken ? this.jwtHelper.isTokenExpired() : true;
    if (retVal) {
      // console.log('isTokenExpired: ' + retVal);
    }
    return retVal;
  }

  public isAccessTokenExpired(): boolean {
    return this.isTokenExpired();
  }

  public isRefreshTokenExpired(): boolean {
    let rv = true;
    if (this.refreshToken) {
      const refToken: RefreshTokenModel = Object.assign(new RefreshTokenModel(), this.decode(this.refreshToken));
      // console.log('isRefreshTokenExpired-> refToken:', refToken);
      if (refToken) {
        const curDate: Date = new Date();
        // console.log('isRefreshTokenExpired-> curDate ms:', curDate.getTime());
        const diff_ms = ((refToken.exp * 1000) - (curDate.getTime() + 500));
        // console.log('isRefreshTokenExpired-> diff_ms:', diff_ms.toFixed());
        rv = diff_ms > 0 ? false : true;
      }
    }


    return rv;
  }

  public getTokenExpirationDate(): Date {
    return this.jwtHelper.getTokenExpirationDate();
  }

  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  public decode(token: string): string {
    return token ? this.jwtHelper.decodeToken(token) : undefined;
  }

  public get expIntervalMs(): number {
    let rv = 0;
    const curDate: Date = new Date();
    const jwtExp: Date = this.getTokenExpirationDate();
    rv = jwtExp.getTime() - curDate.getTime();
    // console.log('ExpIntervalMs-> rv=', rv);
    return rv;
  }

  //#endregion

  //#region Token



  public get barerToken(): string {
    return sessionStorage.getItem(SessionStore.BarerToken);
  }

  public set barerToken(value: string) {
    if (value) {
      sessionStorage.setItem(SessionStore.BarerToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(SessionStore.BarerToken);
    }
  }

  public get refreshToken(): string {
    return sessionStorage.getItem(SessionStore.RefreshToken);
  }

  public set refreshToken(value: string) {
    if (value) {
      sessionStorage.setItem(SessionStore.RefreshToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(SessionStore.RefreshToken);
    }
  }


  public clearToken() {
    sessionStorage.removeItem(SessionStore.RefreshToken);
    sessionStorage.removeItem(SessionStore.BarerToken);
  }


  //#endregion
}
