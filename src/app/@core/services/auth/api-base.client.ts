import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ApiBaseClient {

    constructor() { }

    //#region Base

    protected errorHandler(error: HttpErrorResponse) {
        console.log('errorHandler-> error:', error);
        if (error) {
            // HTTP 401 Unauthorized
            if (error.status === 401) {
                /*
                if (this.apiHelper.refreshToken) {
                    this.authService.refreshToken();
                }*/
            }
        }
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(error);
    }

    protected handleError(error: HttpErrorResponse) {

        console.error(error); // log to console instead
        console.log('handleError-> error:', error);
        if (error.error instanceof ErrorEvent) {
            // Aclient-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error ? error.error.message : error.message}`);
            if (error.error && error.error.statusCode === 401) {
                console.error('handleError -> Go to Logout:');
                return throwError(error);
            }
        }

        // Let the app keep running by returning an empty result.
        // return of(result as T);
        return throwError(error);

    }

    /** Log a Service message  */
    protected log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
