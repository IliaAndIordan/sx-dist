import { Injectable, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AppStore, SessionStore } from '../../const/app-storage.const';

@Injectable()
export class TokenFactory {

    // public headerName: string =  'Authorization';
    // public authScheme: string = 'Bearer ';
    // tslint:disable-next-line:no-inferrable-types
    public skipWhenExpired: boolean = false;
    public throwNoTokenError = false;
    public allowedDomains: string[] = [
        'sx.ws.iordanov.info',
        'sx.iordanov.info',
        'common.ams.iord',
        'localhost:4312',
        'maps.googleapis.com',
    ];

    public disallowedRoutes: string[] = [environment.services.url.auth + 'login',];

    // constructor(@Inject(localStorage) protected localStorage: any) { }
    constructor() { }
    public tokenGetter = () => {
        const token = sessionStorage.getItem(SessionStore.BarerToken);
        // console.log('TokenFactory.tokenGetter token:' + token);
        return token;
    }
}
