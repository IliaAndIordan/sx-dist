import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerService } from './spinner.service';
import { SxAuthModule } from './auth/auth.module';
import { SxApiProjectClient } from './api/project/api-client';
import { SxApiCompanyClient } from './api/company/api-client';
import { SxApiBomClient } from './api/bom/api-client';


@NgModule({
  imports: [
    CommonModule,
    SxAuthModule,
  ],
  declarations: [],
  providers:[
    SpinnerService,
    SxApiProjectClient,
    SxApiCompanyClient,
    SxApiBomClient,
    // -Admin
  ]
})

export class SxCoreServicesModule { }
