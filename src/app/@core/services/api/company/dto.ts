import { IUserTimestampModel } from '../../auth/api/dto';
import { DateModel, IDateModel } from 'src/app/@core/models/common/date.model';
import { NO_IMG_URL } from 'src/app/@core/const/app-storage.const';
import { ResponseModel } from 'src/app/@core/models/common/response.model';
import { SxProduct } from 'src/app/@core/models/pipes/sx-product.pipe';
import { CompanyStatus, CompanyType } from 'src/app/@core/models/pipes/sx-company-type.pipe';
import { RowsCountModel, TableCriteriaBase } from 'src/app/@core/models/common/table-criteria-base.model';
import { IDateValuePoint } from 'src/app/@core/models/common/pont.model';


export class CompanyModel {
    public company_id: number;
    public company_name: string;
    public company_type_id: CompanyType;
    public parent_company_id: number;
    public branch_code: string;

    public logo_url: string;
    public web_url: string;

    public sxLiveryUrl: string;
    public sxLogoUrl: string;

    public notes: string;

    public adate: DateModel;
    public created: DateModel;
    public udate: DateModel;
    public updated: DateModel;

    public productsCount: number;
    public usersCount: number;

    public ean_mfr_code: string;
    public country_id: number;
    public actor_id: number;
    public company_status_id: CompanyStatus;


    public static fromJSON(json: ICompanyModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            sxLogoUrl: json.sx_logo_url ? json.sx_logo_url : NO_IMG_URL,
            sxLiveryUrl: json.sx_livery_url ? json.sx_livery_url : NO_IMG_URL,
            web_url: json.web_url && json.web_url.length > 2 ? json.web_url : undefined,
            logo_url: json.logo_url && json.logo_url.length > 2 ? json.logo_url : undefined,
            created: json.created ? DateModel.fromJSON(json.created) : undefined,
            updated: json.created ? DateModel.fromJSON(json.updated) : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : (json && json.created ? new Date(json.created.date) : undefined),
            udate: (json && json.udate) ? new Date(json.udate) : (json && json.updated ? new Date(json.updated.date) : undefined),
        });
    }



    public static fromJSONOpt(json: ICompanyOptModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            logo_url: json.logo_url && json.logo_url.length > 2 ? json.logo_url : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICompanyModel {
    company_id: number;
    company_name: string;
    company_type_id: number;
    company_status_id: number;
    parent_company_id: number;
    branch_code: string;

    logo_url: string;
    web_url: string;

    sx_livery_url: string;
    sx_logo_url: string;

    notes: string;

    adate: string;
    created: IDateModel;
    udate: string;
    updated: IDateModel;
    usersCount: number;
    productsCount: number;

    ean_mfr_code: string;
    country_id: number;
    actor_id: number;
}

export class ResponseCompanySaveData {
    company: ICompanyModel;

}


export class ResponseCompanySave extends ResponseModel {
    data: ResponseCompanySaveData;
}

export interface ICompanyOptModel {
    company_id: number;
    company_name: string;
    company_type_id: number;
    branch_code: string;
    logo_url: string;
    country_id: number;
}

export class ResponseCompanyOptData {
    company_opt: ICompanyOptModel[];

}


export class ResponseCompanyOpt extends ResponseModel {
    data: ResponseCompanyOptData;
}



export class RespCompanyListData {
    company_list: ICompanyModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyList extends ResponseModel {
    data: RespCompanyListData;
}

export class RespUserListData {
    user_list: IUserTimestampModel[];
    totals: {
        total_rows: number;
    };
}

export class RespUserList extends ResponseModel {
    data: RespUserListData;
}

//#region  Company Product

export class CompanyProductModel {
    public company_product_id: number;
    public company_id: number;
    public product_id: SxProduct;

    public adate: DateModel;


    public static fromJSON(json: ICompanyProductModel): CompanyProductModel {
        const vs = Object.create(CompanyProductModel.prototype);
        return Object.assign(vs, json, {
            product_id: json.product_id as SxProduct,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyProductModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyProductModel {
        const vs = Object.create(CompanyProductModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICompanyProductModel {
    company_product_id: number;
    company_id: number;
    product_id: number;
    adate: string;
}

export class RespCompanyProductData {
    company_products: ICompanyProductModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyProductList extends ResponseModel {
    data: RespCompanyProductData;
}
export class CompanyDashboardStat {
    public itemsCount: number;
}

export class CompanyDashboardStatData {
    companyDashboard: CompanyDashboardStat;
    chartItemsByDate: IDateValuePoint[];
}

export class RespCompanyDashboardStat extends ResponseModel {
    data: CompanyDashboardStatData;
}

//#endregion



//#region BomItem

export class CompanyItem {
    public citemId: number;
    public companyId: number;
    public itemId: number;
    public upc: string;
    public gtin: string;
    public ean: string;
    public mfrName: string;
    public mfrCatalogCode: string;
    public mfrId: number;
    public itemName: string;
    public description: string;
    public vendorCode: string;
    public imageUrl: string;
    public productName: string;
    public productId: number;
    public mfrNameMapId: number;
    public wipId: number;
    public itemStatusId: number;
    public itemProductId: number;
    public itemStatusUdate: Date;
    public adate: Date;
    public udate: Date;
    public cName: string;
    public cBranch: string;

    public static fromJSON(json: ICompanyItem): CompanyItem {
        const vs = Object.create(CompanyItem.prototype);
        return Object.assign(vs, json, {
            itemStatusUdate: (json && json.itemStatusUdate) ? new Date(json.itemStatusUdate) : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyItem.fromJSON(value) : value;
    }

}

export interface ICompanyItem {
    citemId: number;
    companyId: number;
    itemId: number;
    upc: string;
    gtin: string;
    ean: string;
    mfrName: string;
    mfrCatalogCode: string;
    mfrId: number;
    itemName: string;
    description: string;
    vendorCode: string;
    imageUrl: string;
    productName: string;
    productId: number;
    mfrNameMapId: number;
    wipId: number;
    itemStatusId: number;
    itemProductId: number;
    itemStatusUdate: string;
    adate: string;
    udate: string;
    cName: string;
    cBranch: string;
}

export class CompanyItemTableCriteria extends TableCriteriaBase {
    bomId?: number;
}

export class CompanyItemTableData {
    items: ICompanyItem[];
    rowsCount: RowsCountModel;
}

export class ResponseCompanyItemTableData extends ResponseModel {
    data: CompanyItemTableData;
}


export class CompanyItemSaveData {
    item: ICompanyItem;
}

export class ResponseCompanyItemSave extends ResponseModel {
    data: CompanyItemSaveData;
}

export class CompanyItemDeleteData {
    citemId: number;
}

export class ResponseBCompanyItemDelete extends ResponseModel {
    data: CompanyItemDeleteData;
}

export class CompanyItemImportData {
    itemIds: number[];
    companyId: number;
}
export class ResponseCompanyItemImport extends ResponseModel {
    data: CompanyItemImportData;
}


//#endregion

