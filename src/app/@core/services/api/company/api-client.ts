import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../auth/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import {
    CompanyModel, ResponseCompanySave, ResponseCompanySaveData,
    ResponseCompanyOpt, RespCompanyList, RespUserList, RespCompanyProductList,
    CompanyProductModel,
    CompanyItemTableCriteria,
    ResponseCompanyItemTableData,
    CompanyItem,
    ResponseCompanyItemSave,
    ResponseBCompanyItemDelete,
    ResponseCompanyItemImport,
    RespCompanyDashboardStat
} from './dto';
import { RespCompanyUsers, UserModel } from '../../auth/api/dto';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { CompanyStatus } from 'src/app/@core/models/pipes/sx-company-type.pipe';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable()
export class SxApiCompanyClient extends ApiBaseClient {

    // refreshTockenSubscriver: Subscription;

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return environment.services.url.company;
    }

    //#region Company

    companyById(id: number): Observable<CompanyModel> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.company) {
                            rv = CompanyModel.fromJSON(res.data.company);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    companySave(value: CompanyModel): Observable<CompanyModel> {


        value.actor_id = this.cus.user.userId;
        value.company_status_id = value.company_id ? CompanyStatus.ChangedNotConfirmed : CompanyStatus.CreatedNotConfirmed;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_save',
            { company: value },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.company) {
                            rv = CompanyModel.fromJSON(res.data.company);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    liveryUpdate(id: number): Observable<any> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_livery_update',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('liveryUpdate-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('liveryUpdate-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        // console.log('liveryUpdate-> res=', res);
                        if (res && res.data && res.data.company) {
                            rv = CompanyModel.fromJSON(res.data.company);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyOpt(id: number): Observable<Array<CompanyModel>> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_options',
            undefined,
            { headers: hdrs }).pipe(
                map((resp: ResponseCompanyOpt) => {
                    console.log('companyOpt-> resp=', resp);
                    const res = Object.assign(new ResponseCompanyOpt(), resp);
                    const rv: Array<CompanyModel> = new Array<CompanyModel>();
                    if (res && res.success) {

                        if (res && res.data &&
                            res.data.company_opt && res.data.company_opt.length > 0) {
                            res.data.company_opt.forEach(iu => {
                                const company = CompanyModel.fromJSONOpt(iu);
                                rv.push(company);
                            });
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap companyOpt event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    companyListTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        filter?: string): Observable<RespCompanyList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_list',
            {
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyList) => {
                    // console.log('companyListTable -> response=', resp);
                    const res = Object.assign(new RespCompanyList(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap companyListTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    userListTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        filter?: string): Observable<RespUserList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'user_list',
            {
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: RespUserList) => {
                    console.log('userListTable -> response=', resp);
                    const res = Object.assign(new RespUserList(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap userListTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyDashboardStat(id: number): Observable<RespCompanyDashboardStat> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_dashboard',
            {
                companyId: id,
            },
            { headers: hdrs }).pipe(
                map((resp: any) => {
                    console.log('companyDashboardStat-> resp=', resp);
                    const res = Object.assign(new RespCompanyDashboardStat(), resp);
                    return res;
                }),
                tap(event => {
                    this.log(`tap companyOpt event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }
    //#endregion

    //#region  Company Users

    companyUsers(id: number): Observable<RespCompanyUsers> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_users',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyUsers) => {
                    // console.log('companyUsers-> response=', resp);
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyUsersTable(
        // tslint:disable-next-line: variable-name
        com_id: number,
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        filter?: string): Observable<RespCompanyUsers> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_users_table',
            {
                company_id: com_id,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyUsers) => {
                    // console.log('companyUsersTable-> response=', resp);
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    companyUserList(id: number): Observable<Array<UserModel>> {
        return new Observable<Array<UserModel>>(subscriber => {
            this.companyUsers(id)
                .subscribe((resp: RespCompanyUsers) => {
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    // console.log('companyUserList-> res=', res);
                    const rv: Array<UserModel> = new Array<UserModel>();
                    if (res && res.success) {

                        if (res && res.data &&
                            res.data.users && res.data.users.length > 0) {
                            res.data.users.forEach(iu => {
                                //const user = UserModel.fromJSON(iu);
                                // rv.push(iu);
                            });
                        }
                    }
                    // console.log('companyUsers-> rv=', rv);
                    subscriber.next(rv);
                });
        });

    }

    //#endregion

    //#region Company Products

    private companyProducts(companyId: number): Observable<RespCompanyProductList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_products',
            {
                company_id: companyId
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyProductList) => {
                    console.log('companyProducts -> response=', resp);
                    const res = Object.assign(new RespCompanyProductList(), resp);
                    return res;
                }),
                tap(event => {
                    this.log(`tap companyProducts event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyProductList(id: number): Observable<Array<CompanyProductModel>> {
        return new Observable<Array<CompanyProductModel>>(subscriber => {
            this.companyProducts(id)
                .subscribe({
                    next: (res: RespCompanyProductList) => {

                        console.log('companyProductList-> success=', res.success);
                        const rv: Array<CompanyProductModel> = new Array<CompanyProductModel>();
                        if (res && res.success) {
                            console.log('companyProductList-> res=', res);

                            if (res && res.data &&
                                res.data.company_products && res.data.company_products.length > 0) {
                                res.data.company_products.forEach(iu => {
                                    const user = CompanyProductModel.fromJSON(iu);
                                    rv.push(user);
                                });
                            }
                        }
                        subscriber.next(rv);
                    },
                    error: (err: any) => {
                        console.log(err);
                    },
                    complete: () => {
                        console.log('complete');
                    }
                }).add(() => {
                    // Do some work after complete...
                    console.log('At this point the success or error callbacks has been completed.');
                  });
        });

    }

    productAdd(id: number, productId: number): Observable<Array<CompanyProductModel>> {
        return new Observable<Array<CompanyProductModel>>(subscriber => {
            this.companyProductAdd(id, productId)
                .subscribe((res: RespCompanyProductList) => {
                    // console.log('companyProductList-> res=', res);
                    const rv: Array<CompanyProductModel> = new Array<CompanyProductModel>();
                    if (res) {
                        const response = Object.assign(new RespCompanyProductList(), res);

                        // console.log('companyProductList-> response=', response);
                        // console.log('companyProductList-> success=', response.success);

                        if (response && response.data &&
                            response.data.company_products && response.data.company_products.length > 0) {
                            response.data.company_products.forEach(iu => {
                                const user = CompanyProductModel.fromJSON(iu);
                                rv.push(user);
                            });
                        }

                    }
                    subscriber.next(rv);
                });
        });

    }
    private companyProductAdd(companyId: number, productId: number): Observable<RespCompanyProductList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_products_add',
            {
                company_id: companyId,
                product_id: productId
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyProductList) => {
                    console.log('companyProductAdd -> response=', resp);
                    const res = Object.assign(new RespCompanyProductList(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap companyProductAdd event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    productDelete(id: number, companyProductId: number): Observable<Array<CompanyProductModel>> {
        return new Observable<Array<CompanyProductModel>>(subscriber => {
            this.companyProductDelete(id, companyProductId)
                .subscribe((res: RespCompanyProductList) => {
                    // console.log('productDelete-> res=', res);
                    const rv: Array<CompanyProductModel> = new Array<CompanyProductModel>();
                    if (res) {
                        const response = Object.assign(new RespCompanyProductList(), res);

                        // console.log('productDelete-> response=', response);
                        // console.log('productDelete-> success=', response.success);

                        if (response && response.data &&
                            response.data.company_products && response.data.company_products.length > 0) {
                            response.data.company_products.forEach(iu => {
                                const user = CompanyProductModel.fromJSON(iu);
                                rv.push(user);
                            });
                        }

                    }
                    subscriber.next(rv);
                });
        });

    }

    private companyProductDelete(companyId: number, companyProductId: number): Observable<RespCompanyProductList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'company_products_delete',
            {
                company_id: companyId,
                company_product_id: companyProductId
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyProductList) => {
                    console.log('companyProductDelete -> response=', resp);
                    // const res = Object.assign(new RespCompanyProductList(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap companyProductDelete event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion


    //#region Company Item

    companyItemTable(criteria: CompanyItemTableCriteria): Observable<ResponseCompanyItemTableData> {

        console.log('companyItemTable-> criteria=', criteria);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'citem_table',
            {

                qry_offset: criteria.offset,
                qry_limit: criteria.limit,
                qry_orderCol: criteria.sortCol,
                qry_isDesc: criteria.sortDesc,
                qry_filter: criteria.filter,
                bomId: criteria.bomId
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseCompanyItemTableData) => {
                    const res = Object.assign(new ResponseCompanyItemTableData(), resp);
                    console.log('companyItemTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap companyItemTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    getCompanyItem(value: number): Observable<CompanyItem> {
        console.log('getCompanyItem-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'citem_get',
            { apId: value },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanyItemSave) => {
                    console.log('getCompanyItem-> response=', response);
                    const res = Object.assign(new ResponseCompanyItemSave(), response);
                    console.log('getCompanyItem-> res=', res);
                    let rv: CompanyItem;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.item) {
                            rv = CompanyItem.fromJSON(res.data.item);
                        }
                    }
                    console.log('getCompanyItem-> rv=', rv);
                    return rv;
                }),
                tap(event => {
                    this.log(`tap getCompanyItem get event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );



    }


    companyItemSave(value: CompanyItem): Observable<CompanyItem> {
        console.log('companyItemSave-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'citem_save',
            { bom: value },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanyItemSave) => {
                    console.log('companyItemSave-> response=', response);
                    const res = Object.assign(new ResponseCompanyItemSave(), response);
                    // console.log('companyItemSave-> res=', res);
                    let rv: CompanyItem;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.item) {
                            rv = CompanyItem.fromJSON(res.data.item);
                        }
                    }
                    console.log('companyItemSave-> rv=', rv);
                    return rv;
                }),
                tap(event => {
                    this.log(`tap companyItemSave save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );



    }

    companyItemDelete(value: number): Observable<number> {
        console.log('companyItemDelete-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'citem_delete',
            { bomItemId: value },
            { headers: hdrs }).pipe(
                map((response: ResponseBCompanyItemDelete) => {
                    console.log('companyItemDelete-> response=', response);
                    const res = Object.assign(new ResponseBCompanyItemDelete(), response);
                    console.log('companyItemDelete-> res=', res);
                    let rv: number;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.citemId) {
                            rv = res.data.citemId;
                        }
                    }
                    console.log('companyItemDelete-> rv=', rv);
                    return rv;
                }),
                tap(event => {
                    this.log(`tap companyItemDelete delete event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                }),
                catchError(this.handleError)
            );



    }

    companyItemImport(id: number, value: CompanyItem[]): Observable<boolean> {
        console.log('companyItemImport-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'citem_import',
            { bomId: id, items: value },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanyItemImport) => {
                    console.log('companyItemImport-> response=', response);
                    const res = Object.assign(new ResponseCompanyItemImport(), response);
                    // console.log('companyItemImport-> res=', res);
                    return res.success;
                }),
                tap(event => {
                    this.log(`tap bomSave save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                }),
                catchError(this.handleError)
            );



    }

    //#endregion

}
