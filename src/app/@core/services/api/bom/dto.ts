import { ResponseModel } from 'src/app/@core/models/common/response.model';
import { RowsCountModel, TableCriteriaBase } from 'src/app/@core/models/common/table-criteria-base.model';

//#region Bom

export class Bom {
    bomId: number;
    bomName: string;
    description: string;
    companyId: number;
    userId: number;
    userName: string;
    userEmail: string;
    itemsCount: number;
    adate: Date;
    udate: Date;


    public static fromJSON(json: IBom): Bom {
        const vs = Object.create(Bom.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? Bom.fromJSON(value) : value;
    }

}

export interface IBom {
    bomId: number;
    bomName: string;
    description: string;
    companyId: number;
    userId: number;
    userName: string;
    userEmail: string;
    itemsCount: number;
    adate: string;
    udate: string;
}

export class BomTableCriteria extends TableCriteriaBase {
    companyId?: number;
    userId?: number;
}

export class BomTableData {
    boms: IBom[];
    rowsCount: RowsCountModel;
}

export class ResponseBomTableData extends ResponseModel {
    data: BomTableData;
}


export class BomSaveData {
    bom: IBom;
}

export class ResponseBomSave extends ResponseModel {
    data: BomSaveData;
}

export class BomDeleteData {
    bomId: number;
}

export class ResponseBomDelete extends ResponseModel {
    data: BomDeleteData;
}

//#endregion

//#region BomItem

export class BomItem {
    bomItemId: number;
    bomId: number;
    itemId: number;
    sequenceNr: number;
    qauntity: number;
    quantityUom: string;
    upc: string;
    gtin: string;
    ean: string;
    mfrName: string;
    mfrCatalogCode: string;
    itemName: string;
    description: string;
    distributorCode: string;
    notes: string;
    adate: Date;
    udate: Date;


    public static fromJSON(json: IBomItem): BomItem {
        const vs = Object.create(BomItem.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? Bom.fromJSON(value) : value;
    }

}

export interface IBomItem {
    bomItemId: number;
    bomId: number;
    itemId: number;
    sequenceNr: number;
    qauntity: number;
    quantityUom: string;
    upc: string;
    gtin: string;
    ean: string;
    mfrName: string;
    mfrCatalogCode: string;
    itemName: string;
    description: string;
    distributorCode: string;
    notes: string;
    adate: string;
    udate: string;
}

export class BomItemTableCriteria extends TableCriteriaBase {
    bomId?: number;
}

export class BomItemTableData {
    items: IBomItem[];
    rowsCount: RowsCountModel;
}

export class ResponseBomItemTableData extends ResponseModel {
    data: BomItemTableData;
}


export class BomItemSaveData {
    item: IBomItem;
}

export class ResponseBomItemSave extends ResponseModel {
    data: BomItemSaveData;
}

export class BomItemDeleteData {
    bomItemId: number;
}

export class ResponseBomItemDelete extends ResponseModel {
    data: BomItemDeleteData;
}

export class BomItemImportData {
    bomItemIds: number[];
    bomId: number;
}
export class ResponseBomItemImport extends ResponseModel {
    data: BomItemImportData;
}


//#endregion
