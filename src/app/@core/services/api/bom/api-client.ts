import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../auth/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import { Bom, BomItem, BomItemTableCriteria, BomTableCriteria,
  ResponseBomDelete, ResponseBomItemDelete, ResponseBomItemImport, ResponseBomItemSave,
  ResponseBomItemTableData, ResponseBomSave, ResponseBomTableData } from './dto';

@Injectable()
export class SxApiBomClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.bom;
  }


  //#region BOM

  bomTable(criteria: BomTableCriteria): Observable<ResponseBomTableData> {

    // console.log('bomTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bom_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        companyId: criteria.companyId,
        userId: criteria.userId
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseBomTableData) => {
          const res = Object.assign(new ResponseBomTableData(), resp);
          // console.log('bomTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap bomTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getBom(value: number): Observable<Bom> {
    // console.log('getBom-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bom_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomSave) => {
          // console.log('getBom-> response=', response);
          const res = Object.assign(new ResponseBomSave(), response);
          // console.log('getBom-> res=', res);
          let rv: Bom;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.bom) {
              rv = Bom.fromJSON(res.data.bom);
            }
          }
          // console.log('getBom-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap getBom get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  bomSave(value: Bom): Observable<Bom> {
    // console.log('bomSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bom_save',
      { bom: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomSave) => {
          // console.log('bomSave-> response=', response);
          const res = Object.assign(new ResponseBomSave(), response);
          // console.log('bomSave-> res=', res);
          let rv: Bom;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.bom) {
              rv = Bom.fromJSON(res.data.bom);
            }
          }
          // console.log('bomSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap bomSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  bomDelete(value: number): Observable<number> {
    console.log('bomDelete-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bom_delete',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomDelete) => {
          console.log('bomDelete-> response=', response);
          const res = Object.assign(new ResponseBomDelete(), response);
          console.log('bomDelete-> res=', res);
          let rv: number;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.bomId) {
              rv = res.data.bomId;
            }
          }
          console.log('bomDelete-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap bomDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region BOM Item

  bomItemTable(criteria: BomItemTableCriteria): Observable<ResponseBomItemTableData> {

    console.log('bomItemTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bomitem_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        bomId: criteria.bomId
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseBomItemTableData) => {
          const res = Object.assign(new ResponseBomItemTableData(), resp);
          console.log('bomItemTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap bomItemTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getBomItem(value: number): Observable<BomItem> {
    console.log('getBomItem-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bomitem_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomItemSave) => {
          console.log('getBomItem-> response=', response);
          const res = Object.assign(new ResponseBomItemSave(), response);
          console.log('getBomItem-> res=', res);
          let rv: BomItem;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.item) {
              rv = BomItem.fromJSON(res.data.item);
            }
          }
          console.log('getBomItem-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap getBom get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  bomItemSave(value: BomItem): Observable<BomItem> {
    console.log('bomItemSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bomitem_save',
      { bom: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomItemSave) => {
          console.log('bomItemSave-> response=', response);
          const res = Object.assign(new ResponseBomItemSave(), response);
          // console.log('bomItemSave-> res=', res);
          let rv: BomItem;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.item) {
              rv = BomItem.fromJSON(res.data.item);
            }
          }
          console.log('bomItemSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap bomSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  bomItemDelete(value: number): Observable<number> {
    console.log('bomItemDelete-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bomitem_delete',
      { bomItemId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomItemDelete) => {
          console.log('bomItemDelete-> response=', response);
          const res = Object.assign(new ResponseBomItemDelete(), response);
          console.log('bomItemDelete-> res=', res);
          let rv: number;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.bomItemId) {
              rv = res.data.bomItemId;
            }
          }
          console.log('bomItemDelete-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap bomDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  bomItemImport(id: number, value: BomItem[]): Observable<Bom> {
    console.log('bomItemImport-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'bomitem_import',
      { bomId: id, items: value },
      { headers: hdrs }).pipe(
        map((response: ResponseBomItemImport) => {
          console.log('bomItemImport-> response=', response);
          const res = Object.assign(new ResponseBomItemImport(), response);
          // console.log('bomItemSave-> res=', res);
          let rv: Bom;
          if (res && res.success) {
            // console.log('companySave-> res=', res);
            if (res && res.data && res.data.bomId) {
              this.getBom(res.data.bomId)
                .subscribe((resp: Bom) => {
                    console.log('bomItemImport-> resp', resp);
                    rv = resp;
                    return rv;
                },
                err => {
                    throw err;
                });
            }
          }
          console.log('bomItemSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap bomSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

}
