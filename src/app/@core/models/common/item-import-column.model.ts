import { ReadVarExpr } from '@angular/compiler';

export class DataField {
    idx: number;
    name: string;
    value: any;
}

export class ItemImportColumnModel {
    dbColumn: string;
    name: string;
    description?: string;

    dataField?: DataField;
    columnIndex?: number = -1;
    example?: any;
    required?: boolean = false;
}




export const ItemColumnsOpt: ItemImportColumnModel[] = [
    {
        dbColumn: 'sequence_nr',
        name: 'Sequence',
        description: 'Sequence Number',
    },
    {
        dbColumn: 'quantity',
        name: 'Quantity',
        description: 'Quantity',
    },
    {
        dbColumn: 'quantityUom',
        name: 'Quantity UOM',
        description: 'AMSI quantity unit of measure.',
    },
    {
        dbColumn: 'upc',
        name: 'UPC',
        description: 'Standard 11-digit Universal Product Code (UPC) of the item.  If SX PIK is not present, then matching is performed by UPC.',
        required: true,
    },
    {
        dbColumn: 'ean',
        name: 'EAN',
        description: 'Thirteen-digit EAN-13, a superset of the original 11-digit Universal Product Code (UPC) of the item.  If SX PIK is not present, then matching is performed by EAN.',
    },
    {
        dbColumn: 'gtin',
        name: 'GTIN',
        description: '13-digit number called a Global Trade Item Number (GTIN).  If SX PIK is not present, then matching is performed by GTIN.',
    },
    {
        dbColumn: 'manufacturerName',
        name: 'Manufacturer',
        required: true,
    },
    {
        dbColumn: 'manufacturerCatalogNumber',
        name: 'Manufacturer Catalog Code',
        description: 'The catalog code of the item.  If UPC is not present, then matching is performed by ManufacturerName and CatalogCode.  If UPC and ManufacturerName are not present, then matching is performed by CatalogCode only.',
    },
    {
        dbColumn: 'itemName',
        name: 'Item Name',
        description: 'Name of the product.',
    },
    {
        dbColumn: 'description',
        name: 'Description',
    },
    {
        dbColumn: 'distributor_code',
        name: 'Distributor Code',
        description: 'Unique identifier for this item used from distributor.',
    },
    {
        dbColumn: 'notes',
        name: 'Notes',
        description: 'User notes',
    },

];
