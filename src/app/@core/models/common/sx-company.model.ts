import { NO_IMG_URL } from '../../const/app-storage.const';
import { CompanyStatus, CompanyType } from '../pipes/sx-company-type.pipe';
import { SxProduct } from '../pipes/sx-product.pipe';
import { DateModel, IDateModel } from './date.model';
import { ResponseModel } from './response.model';

export class CompanyModel {
    public company_id: number;
    public company_name: string;
    public company_type_id: CompanyType;
    public parent_company_id: number;
    public branch_code: string;

    public logo_url: string;
    public web_url: string;

    public sxLiveryUrl: string;
    public sxLogoUrl: string;

    public notes: string;

    public adate: DateModel;
    public created: DateModel;
    public udate: DateModel;
    public updated: DateModel;

    public productsCount: number;
    public usersCount: number;

    public ean_mfr_code: string;
    public country_id: number;
    public actor_id: number;
    public company_status_id: CompanyStatus;


    public static fromJSON(json: ICompanyModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            sxLogoUrl: json.sx_logo_url ? json.sx_logo_url : NO_IMG_URL,
            sxLiveryUrl: json.sx_livery_url ? json.sx_livery_url : NO_IMG_URL,
            web_url: json.web_url && json.web_url.length > 2 ? json.web_url : undefined,
            logo_url: json.logo_url && json.logo_url.length > 2 ? json.logo_url : undefined,
            usersCount: json.usersCount ? json.usersCount : 1,
            created: json.created ? DateModel.fromJSON(json.created) : undefined,
            updated: json.created ? DateModel.fromJSON(json.updated) : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : (json && json.created ? new Date(json.created.date) : undefined),
            udate: (json && json.udate) ? new Date(json.udate) : (json && json.updated ? new Date(json.updated.date) : undefined),
        });
    }



    public static fromJSONOpt(json: ICompanyOptModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            logo_url: json.logo_url && json.logo_url.length > 2 ? json.logo_url : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICompanyModel {
    company_id: number;
    company_name: string;
    company_type_id: number;
    company_status_id: number;
    parent_company_id: number;
    branch_code: string;

    logo_url: string;
    web_url: string;

    sx_livery_url: string;
    sx_logo_url: string;

    notes: string;

    adate: string;
    created: IDateModel;
    udate: string;
    updated: IDateModel;
    usersCount: number;
    productsCount: number;

    ean_mfr_code: string;
    country_id: number;
    actor_id: number;
}


export interface ICompanyOptModel {
    company_id: number;
    company_name: string;
    company_type_id: number;
    branch_code: string;
    logo_url: string;
    country_id: number;
}


//#region  Company Product

export class CompanyProductModel {
    public company_product_id: number;
    public company_id: number;
    public product_id: SxProduct;

    public adate: DateModel;


    public static fromJSON(json: ICompanyProductModel): CompanyProductModel {
        const vs = Object.create(CompanyProductModel.prototype);
        return Object.assign(vs, json, {
            product_id: json.product_id as SxProduct,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyProductModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyProductModel {
        const vs = Object.create(CompanyProductModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICompanyProductModel {
    company_product_id: number;
    company_id: number;
    product_id: number;
    adate: string;
}

export class RespCompanyProductData {
    company_products: ICompanyProductModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyProductList extends ResponseModel {
    data: RespCompanyProductData;
}

//#endregion
