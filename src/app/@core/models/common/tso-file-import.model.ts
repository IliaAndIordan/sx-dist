

import { BomItem } from '../../services/api/bom/dto';
import { DataField } from './item-import-column.model';
import { ResponseModel } from './response.model';

//#region SxFileImport

export class SxFileImport {
    public name: string;
    public size: number;
    public rows: number;

    public header: string;
    public companyId: number;
    public userId: number;
    public headerRowIdx = 0;

    public dataFields: DataField[];
    public fieldsCount: number;

    public rowsTotal: number;
    public rowsProcessedOk: number;
    public adate: Date;

    public userName?: string;
    public companyName?: string;

    public static fromJSON(json: ISxFileImport): SxFileImport {
        const vs = Object.create(SxFileImport.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? SxFileImport.fromJSON(value) : value;
    }
    /*
    public toJSON(): ITsFileImportModel {
        const vs = Object.create(TsFileImportModel.prototype);
        return Object.assign(vs, this);
    }*/
}

export interface ISxFileImport {
    name: string;
    size: number;
    rows: number;
    fieldsCount: number;
    header: string;
    companyId: number;
    userId: number;
    rowsTotal: number;
    rowsProcessedOk: number;
    adate: Date;

    userName?: string;
    companyName?: string;
}


export class SxBomItemImportModel extends BomItem {
    public lineNumber: number;
    public line: string;
}

//#endregion
