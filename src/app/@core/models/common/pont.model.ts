export class DateValuePoint {
    adate: Date;
    value: number;

    public static fromJSON(json: IDateValuePoint): DateValuePoint {
        const vs = Object.create(DateValuePoint.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? DateValuePoint.fromJSON(value) : value;
    }

    public static getXAxesData(dt: IDateValuePoint[]): Array<string> {
        const xAxesData = new Array<string>();

        if (dt && dt.length > 0) {
            for (let idx = dt.length - 1; idx > 0; idx--) {
                const row = DateValuePoint.fromJSON(dt[idx]);
                xAxesData.push(row.adate.toLocaleDateString('en-US'));
            }
        }
        return xAxesData;
    }

    public static eChartDataSeries(dt: IDateValuePoint[]): any[] {
        let num = Math.random() * 100;
        const lineChartData = [[new Date(), num]];
        if (dt && dt.length > 0) {
            for (let idx = dt.length - 1; idx > 0; idx--) {
                const row = DateValuePoint.fromJSON(dt[idx]);
                lineChartData.push([row.adate, row.value]);
            }
        }
        num = Math.random() * 100;
        lineChartData.push([new Date(), num]);
        return lineChartData;
    }

    public static eChartMiniDataSeriesTime(dt: IDateValuePoint[], pointsCount: number = 7): any[] {
        let num = Math.random() * 100;
        const lineChartData = [];
        for (let idx = 0; idx < pointsCount; idx++) {
            if (dt && dt.length > idx) {
                const row = DateValuePoint.fromJSON(dt[idx]);
                lineChartData.push([row.adate, row.value]);
            } else {
                num = Math.random() * 100;
                const time = new Date();
                time.setTime(time.getTime() - ((pointsCount - idx) * (24 * 3600 * 1000)));
                lineChartData.push([time, num]);
            }
        }
        return lineChartData;
    }

    public static getYAxesData(dt: IDateValuePoint[]): Array<number> {
        const yAxesData = new Array<number>();

        if (dt && dt.length > 0) {
            for (let idx = dt.length - 1; idx > 0; idx--) {
                const row = DateValuePoint.fromJSON(dt[idx]);
                yAxesData.push(row.value);
            }
        }
        return yAxesData;
    }
}

export interface IDateValuePoint {
    adate: string;
    value: number;
}
