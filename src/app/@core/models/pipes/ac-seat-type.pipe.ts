import { Pipe, PipeTransform } from "@angular/core";
import { URL_COMMON_IMAGE_AIRCRAFT } from "../../const/app-storage.const";
import { EnumViewModel } from "../common/enum-view.model";

//#region ApType
export class AmsAcSeatType {
    public class: string;
    public confort: number;
    public image: string;
    public maxDisanceKm: number;
    public price: number;
    public seatTypeId: number;
    public stName: string;
}

export enum AcSeatType {
    Standart = 1,
    Slimline = 2,
    Plus = 3,
    Premium = 4,
    Extra = 5,
    ShostHaul = 6,
    Eurobusiness = 7,
    Recliner = 8,
    CradleSleeper = 9,
    FullyFlat = 10,
    AngledLieFlat = 11,
    OffsetFullyFlat = 12,
    HerringboneSisleAccess = 13,
    SofaSeatAisleAccess = 14,
    SuperFirstClass = 15

}

export const AcSeatTypeOpt: AmsAcSeatType[] = [
    {
        class: "E",
        confort: 0.5,
        image: "Seat_E_1.jpg",
        maxDisanceKm: 500,
        price: 2500,
        seatTypeId: 1,
        stName: "Standart",
    }, {
        class: "E",
        confort: 0.8,
        image: "Seat_E_2.jpg",
        maxDisanceKm: 1500,
        price: 3000,
        seatTypeId: 2,
        stName: "Slimline",
    },
    {
        class: "E",
        confort: 1,
        image: "Seat_E_3.jpg",
        maxDisanceKm: 2500,
        price: 4000,
        seatTypeId: 3,
        stName: "Plus",
    },
    {
        class: "E",
        confort: 1.05,
        image: "Seat_E_4.jpg",
        maxDisanceKm: 5000,
        price: 4500,
        seatTypeId: 4,
        stName: "Premium",
    },
    {
        class: "E",
        confort: 1.2,
        image: "Seat_E_5.jpg",
        maxDisanceKm: 20000,
        price: 5000,
        seatTypeId: 5,
        stName: "Extra",
    },
    {
        class: "B",
        confort: 0.5,
        image: "Seat_B_1.jpg",
        maxDisanceKm: 500,
        price: 4000,
        seatTypeId: 6,
        stName: "Shost Haul",
    },
    {
        class: "B",
        confort: 0.8,
        image: "Seat_B_2.jpg",
        maxDisanceKm: 1500,
        price: 6000,
        seatTypeId: 7,
        stName: "Eurobusiness",
    },
    {
        class: "B",
        confort: 1,
        image: "Seat_B_3.jpg",
        maxDisanceKm: 2500,
        price: 8000,
        seatTypeId: 8,
        stName: "Recliner",
    },
    {
        class: "B",
        confort: 1.1,
        image: "Seat_B_4.jpg",
        maxDisanceKm: 5000,
        price: 12000,
        seatTypeId: 9,
        stName: "Cradle sleeper",
    },
    {
        class: "B",
        confort: 1.2,
        image: "Seat_B_5.jpg",
        maxDisanceKm: 20000,
        price: 18000,
        seatTypeId: 10,
        stName: "Fully flat",
    },
    {
        class: "F",
        confort: 0.8,
        image: "Seat_F_1.jpg",
        maxDisanceKm: 1500,
        price: 12000,
        seatTypeId: 11,
        stName: "Angled lie-flat",
    },
    {
        class: "F",
        confort: 0.9,
        image: "Seat_F_2.jpg",
        maxDisanceKm: 2500,
        price: 15000,
        seatTypeId: 12,
        stName: "Offset fully flat",
    },
    {
        class: "F",
        confort: 1,
        image: "Seat_F_3.jpg",
        maxDisanceKm: 5000,
        price: 18000,
        seatTypeId: 13,
        stName: "Herringbone aisle access",
    },
    {
        class: "F",
        confort: 1.2,
        image: "Seat_F_4.jpg",
        maxDisanceKm: 10000,
        price: 24000,
        seatTypeId: 14,
        stName: "Sofa-seat aisle access",
    },
    {
        class: "F",
        confort: 1.5,
        image: "Seat_F_5.jpg",
        maxDisanceKm: 20000,
        price: 48000,
        seatTypeId: 15,
        stName: "Super First Class suites",
    }
];


@Pipe({ name: 'seattype' })
export class AmsAcSeatTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AcSeatType[value];
        const data: AmsAcSeatType = AcSeatTypeOpt.find(x => x.seatTypeId === value);
        if (data) {
            rv = data.stName;
        }
        return rv;
    }
}

@Pipe({ name: 'seatimg' })
export class AmsAcSeatTypeImageDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        //let rv: string = AcSeatType[value];
        let rv = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/seat_icon_3.png';
        const data: AmsAcSeatType = AcSeatTypeOpt.find(x => x.seatTypeId === value);
        if (data) {
            rv = rv = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/' + data.image;
        }
        return rv;
    }
}

//#endregion
