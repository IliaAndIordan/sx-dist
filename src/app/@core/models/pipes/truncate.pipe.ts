import { Pipe, PipeTransform } from '@angular/core';

@Pipe({  name: 'truncate'})
export class TruncatePipe {
  transform(value: string, args: number): string {

    if (args === 0) {
      return value;
    }
    //Set limit to 10 if none declared 
    let limit = Number(args) > 0 ? args : 10;
    // Trail string
    let trail = '...';

    if (value) {
      if (value.length > limit) {
        return value.substring(0, limit) + trail;
      } else {
        return value;
      }
    }

  }
}