import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../common/enum-view.model";

//#region ApType
export class AmsAcType {
    public acTypeId: number;
    public name: string;
    public hangarCostWeek: number;
    public airportCostWeek: number;
    public insuranceCostWeek: number;
    public notes: string;
    public amorization_PerFlH: number;
    public maxMtowKg: number;
    public rentPerH: number;
}

export enum AcType {
    Piston = 1,
    TurboProp = 2,
    LargeTurboProp = 3,
    SmallJet = 4,
    MediumJet = 5,
    LargeJet = 6,
}

export const AcTypeOpt: AmsAcType[] = [
    {
        acTypeId: 1,
        name: 'Piston',
        hangarCostWeek: 100,
        airportCostWeek: 81.08,
        insuranceCostWeek: 25,
        notes: '',
        amorization_PerFlH: 0.01,
        maxMtowKg: 1700,
        rentPerH: 0

    },
    {
        acTypeId: 2,
        name: 'Turbo Prop',
        hangarCostWeek: 143.75,
        airportCostWeek: 116.51,
        insuranceCostWeek: 35.92,
        notes: 'Twin',
        amorization_PerFlH: 0.01,
        maxMtowKg: 3700,
        rentPerH: 0
    },
    {
        acTypeId: 3,
        name: 'Large Turbo Prop',
        hangarCostWeek: 300,
        airportCostWeek: 243.24,
        insuranceCostWeek: 75,
        notes: 'Turbo Prop (King Air/PC12)',
        amorization_PerFlH: 0.01,
        maxMtowKg: 5700,
        rentPerH: 0
    },
    {
        acTypeId: 4,
        name: 'Small Jet',
        hangarCostWeek: 375,
        airportCostWeek: 304.05,
        insuranceCostWeek: 93.75,
        notes: 'Small Jet (12,500-20,000 lbs)',
        amorization_PerFlH: 0.03,
        maxMtowKg: 9100,
        rentPerH: 0
    },
    {
        acTypeId: 5,
        name: 'Medium Jet',
        hangarCostWeek: 500,
        airportCostWeek: 405.4,
        insuranceCostWeek: 125,
        notes: 'Medium Jet (20,001 - 40,000 lbs)',
        amorization_PerFlH: 0.02,
        maxMtowKg: 18150,
        rentPerH: 0
    },
    {
        acTypeId: 6,
        name: 'Large Jet',
        hangarCostWeek: 700,
        airportCostWeek: 567.56,
        insuranceCostWeek: 175,
        notes: 'Large Jet (<40,000 lbs)',
        amorization_PerFlH: 0.007,
        maxMtowKg: undefined,
        rentPerH: 0
    },

];


@Pipe({ name: 'actype' })
export class AcTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AcType[value];
        const data: AmsAcType = AcTypeOpt.find(x => x.acTypeId === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion
