import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../common/enum-view.model";

//#region ApType

export enum ApType {
    Heliport = 1,
    Airfield = 2,
    Local = 3,
    Regional = 4,
    SmallInt = 5,
    Int = 6,
    LargeInt = 7,
    MegaInt = 8
}

export const ApTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Heliport'
    },
    {
        id: 2,
        name: 'Airfield'
    },
    {
        id: 3,
        name: 'Local'
    },
    {
        id: 4,
        name: 'Regional'
    },
    {
        id: 5,
        name: 'Small International'
    },
    {
        id: 6,
        name: 'International'
    },
    {
        id: 7,
        name: 'Large International'
    },
    {
        id: 8,
        name: 'Mega International'
    },
];


@Pipe({ name: 'aptype' })
export class ApTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = ApType[value];
        const data: EnumViewModel = ApTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

export class ApTypeInfo {
    id: number;
    name: string;
    trafic_min: number;
    map_symbol_radius: number;
    staticmap_zoom: number
    description: string
}

export const ApTypeInfoOpt: ApTypeInfo[] = [
    {
        id: 1,
        name: 'Heliport',
        trafic_min: 10,
        map_symbol_radius: 4,
        staticmap_zoom: 16,
        description: 'Heliport'
    },
    {
        id: 2,
        name: 'Airfield',
        trafic_min: 30,
        map_symbol_radius: 6,
        staticmap_zoom: 15,
        description: 'Very small airport that serve only local area, no First class passengers will be generated'
    },
    {
        id: 3,
        name: 'Local',
        trafic_min: 50,
        map_symbol_radius: 6,
        staticmap_zoom: 14,
        description: 'Very small airport that serve only local area, no First class passengers will be generated'
    },
    {
        id: 4,
        name: 'Regional',
        trafic_min: 100,
        map_symbol_radius: 8,
        staticmap_zoom: 13,
        description: 'small airport that serve only region of 800 nm, no First class passengers will be generated'
    },
    {
        id: 5,
        name: 'Small International',
        trafic_min: 1500,
        map_symbol_radius: 8,
        staticmap_zoom: 13,
        description: 'SI'
    },
    {
        id: 6,
        name: 'International',
        trafic_min: 50000,
        map_symbol_radius: 12,
        staticmap_zoom: 12,
        description: 'I'
    },
    {
        id: 7,
        name: 'Large International',
        trafic_min: 500000,
        map_symbol_radius: 14,
        staticmap_zoom: 11,
        description: 'LI'
    },
    {
        id: 8,
        name: 'Mega International',
        trafic_min: 10000000,
        map_symbol_radius: 14,
        staticmap_zoom: 11,
        description: 'MI'
    },
];

//#endregion

//#region RwType

export enum RwType {
    Helipad = 1,
    Runway = 2,
}

export const RwTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Helipad'
    },
    {
        id: 2,
        name: 'Runway'
    },
];




@Pipe({ name: 'rwtype' })
export class RwTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = RwType[value];
        const data: EnumViewModel = RwTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

//#region RwSerface

export enum RwSerface {
    WaterRunway = 1,
    SnowOrIce = 2,
    TurfOrGrass = 3,
    DirtOrGravel = 4,
    Concrete = 5,
    Asphalt = 6

}

export const RwSerfaceOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Water Runway'
    },
    {
        id: 2,
        name: 'Snow or Ice'
    },
    {
        id: 3,
        name: 'Turf or Grass'
    },
    {
        id: 4,
        name: 'Dirt or Gravel'
    },
    {
        id: 5,
        name: 'Concrete'
    },
    {
        id: 6,
        name: 'Asphalt'
    },
];


@Pipe({ name: 'rwserface' })
export class RwSerfaceDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = RwSerface[value];
        const data: EnumViewModel = RwSerfaceOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

