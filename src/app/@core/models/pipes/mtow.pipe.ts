import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../../const/app-storage.const';
import { LocalUserSettings } from '../app/local-user-settings';

@Pipe({
    name: 'mtow'
})
export class MtowPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.settings);
        let kgToLb = 1;
        let label = 'kg';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                kgToLb = settings.displayWeightLb ? 2.20462 : 1;
                label = settings.displayWeightLb ? 'lb' : 'kg';
            }
        }
        const num = value * kgToLb;

        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;

        /*
        let abs = Math.abs(num);
        const rounder = Math.pow(10, 1);
        const isNegative = num < 0; // will also work for Negetive numbers
        let key = '';


        const powers = [
            { key: 'Q', num: Math.pow(10, 15) },
            { key: 'T', num: Math.pow(10, 12) },
            { key: 'B', num: Math.pow(10, 9) },
            { key: 'M', num: Math.pow(10, 6) },
            { key: 'k', num: 1000 }
        ];


        for (let i = 0; i < powers.length; i++) {
            let reduced = abs / powers[i].num;
            reduced = Math.round(reduced * rounder) / rounder;
            if (reduced >= 1) {
                abs = reduced;
                key = powers[i].key;
                break;
            }
        }
        return (isNegative ? '-' : '') + abs + key + ' ' + label;
        */
    }
}
