import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyStatusDisplayPipe, CompanyTypeDisplayPipe } from './sx-company-type.pipe';
import { SxProductNamePipe } from './sx-product.pipe';
import { UserRoleDisplayPipe } from './sx-user-role.pipe';
import { SxProjectStatusDisplayPipe } from '../../services/api/project/enums';
import { TsDatePipe } from './ts-date.pipe';
import { BooleanPipe } from './boolean.pipe';
import { LatLonPipe } from './latlon.pipe';
import { LengthPipe } from './length.pipe';
import { RsRunwaySurfacePipe, RsRunwayTypePipe } from './rs-runway.pipe';
import { MtowPipe } from './mtow.pipe';
import { UtcPipe } from './utc.pipe';
import { AmsStatusDisplayPipe, WadStatusDisplayPipe } from './ams-status.enums';
import { ApTypeDisplayPipe, RwSerfaceDisplayPipe, RwTypeDisplayPipe } from './ap-type.pipe';
import { AcTypeDisplayPipe } from './ac-type.pipe';
import { AmsAcSeatTypeDisplayPipe, AmsAcSeatTypeImageDisplayPipe } from './ac-seat-type.pipe';
import { TsCurrencyPipe } from './ts-currency-display.pipe';
import { TruncatePipe } from './truncate.pipe';
import { FileSizePipe } from './file-size.pipe';



@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        CompanyTypeDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        TsCurrencyPipe,
        TruncatePipe,
        FileSizePipe,
    ],
    exports:[
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        TsCurrencyPipe,
        TruncatePipe,
        FileSizePipe,
    ],
    providers: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        TsCurrencyPipe,
        TruncatePipe,
        FileSizePipe,
    ]
})
export class SxPipesModule { }
