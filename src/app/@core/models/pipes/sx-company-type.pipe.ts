import { EnumViewModel } from '../../models/common/enum-view.model';
import { Pipe, PipeTransform } from '@angular/core';

export enum CompanyType {
    Manufacturer = 1,
    Distributor = 2,
    Contractor = 3,
}


export const CompanyTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Manufacturer'
    },
    {
        id: 2,
        name: 'Distributor'
    },
    {
        id: 3,
        name: 'Contractor'
    },
];


@Pipe({ name: 'companytype' })
export class CompanyTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = CompanyType[value];
        const data: EnumViewModel = CompanyTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#region CompanyStatus
export enum CompanyStatus {
    CreatedNotConfirmed = 1,
    ChangedNotConfirmed = 2,
    ConfirmationSend = 3,
    Confirmed = 4,
}

export const CompanyStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Created'
    },
    {
        id: 2,
        name: 'Changed'
    },
    {
        id: 3,
        name: 'Confirmation Send'
    },
    {
        id: 4,
        name: 'Confirmed'
    },
];

@Pipe({ name: 'companystatus' })
export class CompanyStatusDisplayPipe implements PipeTransform {
    transform(value, args: string[]): string {
        let rv: string = CompanyStatus[value];
        const data: EnumViewModel = CompanyStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
//#endregion


