import { EnumViewModel } from '../../models/common/enum-view.model';
import { Pipe, PipeTransform } from '@angular/core';

export enum UserRole {
    Admin = 1,
    Editor = 2,
    User = 3,
    Guest = 4
}

export const UserRoleOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Admin'
    },
    {
        id: 2,
        name: 'Editor'
    },
    {
        id: 3,
        name: 'Airline Manager'
    },
    {
        id: 4,
        name: 'Guest'
    },
];


@Pipe({ name: 'userrole' })
export class UserRoleDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = UserRole[value];
        const data: EnumViewModel = UserRoleOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}


