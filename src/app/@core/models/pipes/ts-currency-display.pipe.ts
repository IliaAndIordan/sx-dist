import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

export enum CurrencyType {
  Usd,
  Cad,
}


/**
 * Get Curreny code based on Trade Service Currency Id
 */
@Pipe({ name: 'tscurrencydisplay' })
export class TsCurrencyDisplayPipe implements PipeTransform {

  constructor(private currencyPipe: CurrencyPipe) { }

  transform(value: any, args?: any[], symbolDisplay?: boolean, digits?: string): string {
    var currencyCode = CurrencyType[value];
    if (!currencyCode) {
      console.error('no currencyCode provided to tscurrencydisplay pipe, defaulting to USD');
      currencyCode = CurrencyType[0]; //USD
    }

    return currencyCode.toUpperCase();
  }
}


/**
 * Get Curreny code based on Trade Service Currency Id
 */
@Pipe({ name: 'tscurrency' })
export class TsCurrencyPipe implements PipeTransform {

  constructor(private currencyPipe: CurrencyPipe) { }

  transform(value: any, args?: any[], symbolDisplay?: boolean, digits?: string): string {
    const usdCurrencyType = CurrencyType[0].toUpperCase(); // Default USD

    if (!args || !args.length) { return this.currencyPipe.transform(value, 'USD', 'symbol', digits); }

    if (typeof args === 'string') {
      return this.currencyPipe.transform(value, args, 'symbol', digits);
    }

    const currencyTypeCode = CurrencyType[args[0]].toUpperCase();
    return this.currencyPipe.transform(value, currencyTypeCode, 'symbol', digits);
  }
}

