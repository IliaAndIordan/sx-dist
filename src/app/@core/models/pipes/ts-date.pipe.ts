import { Pipe, PipeTransform } from '@angular/core';

/*
 * Convert date to mm-dd-yyyy.
 * Usage:
 *   date string | tsdate
 * Example:
 *   {{ \/Date(1444218362190-0000)\/ |  tsdate}}
 *   formats to: 1 KB
*/
@Pipe({ name: 'tsdate' })
export class TsDatePipe implements PipeTransform {

    public static padLeft(str: string, ch, len: number): string {
        ch = ch || ' ';
        len = len - str.length + 1;
        return len > 0 ?
            new Array(len).join(ch) + str : str;
    }



    transform(dateStr: string, precision: number = 2): string {
        let rv = '';
        if (!dateStr || dateStr.length == 0) { return '?'; }

        const dateMs = parseInt(dateStr.replace(/(^.*\()|([+-].*$)/g, ''));

        if (isNaN(parseFloat(String(dateMs))) || !isFinite(dateMs)) { return '?'; }
        const jsDate = new Date(dateMs);


        rv = TsDatePipe.padLeft((jsDate.getMonth() + 1).toString(), '0', 2) +
            '-' + TsDatePipe.padLeft(jsDate.getDate().toString(), '0', 2) +
            '-' + jsDate.getFullYear();


        return rv;
    }


}
/*
 * Convert date to hh:mm.
 * Usage:
 *   date string | tstime
 * Example:
 *   {{ \/Date(1444218362190-0000)\/ |  tstime}}
 *   formats to: 1 KB
*/
@Pipe({ name: 'tstime' })
export class TsTimePipe implements PipeTransform {

    transform(dateStr: string): string {
        let rv = '';

        if (!dateStr || dateStr.length == 0) { return '?'; }

        const dateMs = parseInt(dateStr.replace(/(^.*\()|([+-].*$)/g, ''));
        if (isNaN(parseFloat(String(dateMs))) || !isFinite(dateMs)) { return '?'; }
        const jsDate = new Date(dateMs);
        rv = TsDatePipe.padLeft(jsDate.getHours().toString(), '0', 2) +
            ':' + TsDatePipe.padLeft(jsDate.getMinutes().toString(), '0', 2);

        return rv;
    }
}

/*
 * Convert date to hh:mm.
 * Usage:
 *   date string | tstime
 * Example:
 *   {{ \/Date(2008, 4, 12, 0, 0, 0 )\/ |  gvdate}}
 *   formats to: 1 KB
*/
@Pipe({ name: 'gvdate' })
export class GvDatePipe implements PipeTransform {

    public static getDate(dateStr: string): Date {
        let jsDate: Date;
        if (dateStr && dateStr.length > 0) {
            dateStr = dateStr.replace(/(^,*\()|([+-].*$)/g, '');
            // console.log('GvDatePipe.getDate() dateStr1= ', dateStr);
            dateStr = dateStr.slice(5, dateStr.length - 1);
            // console.log('GvDatePipe.getDate() dateStr2= ', dateStr);
            const dateArr = dateStr.split(',');
            // console.log('GvDatePipe.getDate() dateArr= ', dateArr);
            if (!dateArr || dateArr.length < 5) { return jsDate; }
            jsDate = new Date(parseInt(dateArr[0], 10),
                              parseInt(dateArr[1], 10),
                              parseInt(dateArr[2], 10),
                              parseInt(dateArr[3], 10),
                              parseInt(dateArr[4], 10),
                              parseInt(dateArr[5], 10) );
        }
        return jsDate;
    }

    transform(dateStr: string): string {
        let rv = '';

        if (!dateStr || dateStr.length === 0) { return '?'; }
        const jsDate = GvDatePipe.getDate(dateStr);

        rv = TsDatePipe.padLeft((jsDate.getMonth() + 1).toString(), '0', 2) +
            '-' + TsDatePipe.padLeft(jsDate.getDate().toString(), '0', 2) +
            '-' + jsDate.getFullYear();

        return rv;
    }
}

/*
 * Convert date to hh:mm.
 * Usage:
 *   date string | tstime
 * Example:
 *   {{ \/Date(2008, 4, 12, 0, 0, 0 )\/ |  gvtime}}
 *   formats to: 1 KB
*/
@Pipe({ name: 'gvtime' })
export class GvTimePipe implements PipeTransform {

    transform(dateStr: string): string {
        let rv = '';

        if (!dateStr || dateStr.length === 0) { return '?'; }
        const jsDate = GvDatePipe.getDate(dateStr);

        rv = TsDatePipe.padLeft(jsDate.getHours().toString(), '0', 2) +
            ':' + TsDatePipe.padLeft(jsDate.getMinutes().toString(), '0', 2);

        return rv;
    }
}
/*
 * Convert date to Month dd yyyy
 * Usage:
 *   date string | tsdatemonth
 * Example:
 *   {{ \/Date(1444218362190-0000)\/ |  tsdatemonth}}
 *   formats to: Jan 10 2019
*/
@Pipe({ name: 'tsdatemonth' })
export class TsDateMonthPipe implements PipeTransform {

    private monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


    transform(dateStr: string, precision: number = 2): string {
        let rv = '';
        if (!dateStr || dateStr.length == 0) { return '?'; }

        const dateMs = parseInt(dateStr.replace(/(^.*\()|([+-].*$)/g, ''));

        if (isNaN(parseFloat(String(dateMs))) || !isFinite(dateMs)) { return '?'; }
        const jsDate = new Date(dateMs);


        rv = this.monthNames[jsDate.getMonth()] + ' ' +
            TsDatePipe.padLeft(jsDate.getDate().toString(), '0', 2) +
            ' ' + jsDate.getFullYear();


        return rv;
    }
}
