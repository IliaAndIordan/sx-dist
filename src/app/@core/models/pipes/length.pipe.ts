import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../../const/app-storage.const';
import { LocalUserSettings } from '../app/local-user-settings';


@Pipe({
    name: 'len'
})
export class LengthPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.settings);
        let mToft = 1;
        let label = 'm';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                mToft = settings.displayDistanceMl ? 3.28084 : 1;
                label = settings.displayDistanceMl ? 'ft' : 'm';
            }
        }
        const num = value * mToft;
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }
}
