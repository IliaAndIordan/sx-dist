import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../common/enum-view.model";

export enum WadStatus {
    ToDo = 1,
    InProgress = 2,
    Review = 3,
    Done = 4,
}


export const WadStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'To Do'
    },
    {
        id: 2,
        name: 'WIP'
    },
    {
        id: 3,
        name: 'Review'
    },
    {
        id: 4,
        name: 'Done'
    },
];


@Pipe({ name: 'wadstatus' })
export class WadStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = WadStatus[value];
        const data: EnumViewModel = WadStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}


export enum AmsStatus {
    DataCheckRequired = 1,
    DataCheckInProgress = 2,
    DataError = 3,
    Active = 4
}

export const AmsStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Data Check'
    },
    {
        id: 2,
        name: 'WIP'
    },
    {
        id: 3,
        name: 'Data Error'
    },
    {
        id: 4,
        name: 'Done'
    },
];


@Pipe({ name: 'amsstatus' })
export class AmsStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsStatus[value];
        const data: EnumViewModel = AmsStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}