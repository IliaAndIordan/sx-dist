import { EChartsOption } from 'echarts';
import * as echarts from 'echarts';


export class EasyPieChartModel {
    percent = 0;
    title?: string;
    tooltip?: string;
    options = {
        barColor: 'rgba(255,255,255,0.7)',
        trackColor: 'rgba(255,255,255,0.2)',

        scaleColor: 'rgba(255,255,255,0)',
        scaleLength: 5,
        lineCap: 'butt', // butt, round and square.
        lineWidth: 2,
        size: 90,
        rotate: 0,
        animate: { duration: 2000, enabled: true }
    };
}


export const echartMiniAreaOptions: EChartsOption = {
    color: ['#80FFA5', '#00DDFF', '#37A2FF', '#FF0087', '#FFBF00'],
    grid: {
        show: false,
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            animation: false,
            label: {
                backgroundColor: '#505765'
            }
        }
    },
    xAxis: {
        type: 'time',
        boundaryGap: false,
        axisLine: { onZero: false },
        splitNumber : 10,
        show: false,
    },
    yAxis: {
        type: 'value',
        show: false,
    },
    series: [
        {
            name: 'Distributor Items',
            data : [],
            type: 'line',
            smooth: true,
            showSymbol: false,
            areaStyle: {
                opacity: 0.6,
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: 'rgba(217, 255, 179, 0.4)'
                }, {
                    offset: 1,
                    color: 'rgba(1, 191, 236)'
                }])
            },


        },
    ],
};
