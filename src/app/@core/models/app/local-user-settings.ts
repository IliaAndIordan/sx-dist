
export class LocalUserSettings {
  public userId: number;
  public snavLeftWidth = 4;
  public displayDistanceMl: boolean;
  public displayWeightLb: boolean;

  public static fromJSON(json: ILocalUserSettings): LocalUserSettings {
      const vs = Object.create(LocalUserSettings.prototype);
      return Object.assign(vs, json, {
          snavLeftWidth: json.snavLeftWidth ? json.snavLeftWidth : 4,
          displayDistanceMl: json.displayDistanceMl !== 0 ? true : false,
          displayWeightLb: json.displayWeightLb !== 0 ? true : false,
      });
  }

  // reviver can be passed as the second parameter to JSON.parse
  // to automatically call User.fromJSON on the resulting value.
  public static reviver(key: string, value: any): any {

      return key === '' ? LocalUserSettings.fromJSON(value) : value;
  }

  public toJSON(): ILocalUserSettings {
      const vs = Object.create(LocalUserSettings.prototype);
      return Object.assign(vs, this);
  }
}

export interface ILocalUserSettings {
  userId: number;
  snavLeftWidth: number;
  displayDistanceMl: number;
  displayWeightLb: number;
}

