import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutes } from './@core/const/app-routes.const';
import { AdminGuard } from './@core/guards/admin.guard';
import { AuthGuard } from './@core/guards/auth.guard';
import { PreloadSelectedModulesList } from './@core/guards/preload-strategy';

const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.public },
  {
    path: AppRoutes.public,
    loadChildren: () => import('./public/ams-public.module').then(m => m.AmsPublicModule), data: { preload: false }
  },
  {
    path: AppRoutes.profile,
    loadChildren: () => import('./profile/profile.module').then(m => m.SxProfileModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.company,
    loadChildren: () => import('./company/company.module').then(m => m.SxCompanyModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.bom,
    loadChildren: () => import('./bom/bom.module').then(m => m.SxBomModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.distributor,
    loadChildren: () => import('./distributor/distributor.module').then(m => m.SxDistributorModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  /*
  {
    path: AppRoutes.wad,
    loadChildren: () => import('./wad/wad.module').then(m => m.AmsWadModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
 
  { path: AppRoutes.wad+'/'+AppRoutes.subregion,
    loadChildren: () => import('./subregion/subregion.module').then(m => m.AmsSubregionModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  { path: AppRoutes.wad+'/'+AppRoutes.country,
  loadChildren: () => import('./wad-country/wad-country.module').then(m => m.AmsWadCountryModule), data: { preload: false },
  canActivate: [AuthGuard]
},*/
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      {
    preloadingStrategy: PreloadSelectedModulesList,
    paramsInheritanceStrategy: 'always', onSameUrlNavigation: 'reload',
    relativeLinkResolution: 'legacy'
})
  ],
  exports: [RouterModule],
  providers: [
    PreloadSelectedModulesList
  ]
})
export class AppRoutingModule { }

export const routableComponents = [];
