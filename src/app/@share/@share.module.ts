import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxComponentsModule } from './components/components.module';
import { ToastrModule } from 'ngx-toastr';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AmsChartsModule } from './charts/ams-charts.module';
import { SxMaterialModule } from './components/material.module';
import { SxDialogssModule } from './components/dialogs/dialogs.module';


@NgModule({
  imports: [
    CommonModule,
    //
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
    }),
    //
    SxPipesModule,
    SxMaterialModule,
    SxComponentsModule,
    //
    AmsChartsModule,
    SxDialogssModule,
  ],
  declarations: [],
  exports: [
    SxComponentsModule,
    AmsChartsModule,
  ]
})

export class SxShareModule { }
