import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmsChartLineComponent } from './ams-chart-line/ams-chart-line.component';
import { ChartsModule } from 'ng2-charts';
import { AmsChartPieSmallComponent } from './ams-chart-pie-sm/ams-chart-pie-sm.component';
import { NgxEchartsModule } from 'ngx-echarts';


@NgModule({

    imports: [
        CommonModule,
        NgxEchartsModule.forRoot({
            echarts: () => import('echarts'),
          }),
        ChartsModule,
    ],
    declarations: [
        AmsChartLineComponent,
        AmsChartPieSmallComponent,
    ],
    exports: [
        NgxEchartsModule,
        ChartsModule,
        // Charts
        AmsChartLineComponent,
        AmsChartPieSmallComponent,
    ]
})
export class AmsChartsModule { }
