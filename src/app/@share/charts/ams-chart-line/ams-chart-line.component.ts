import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
// Services
import { BaseChartDirective, Color } from 'ng2-charts';
import { WHITE_ON_BLACK_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

export const lineColorYellow = { // yelow grey
    backgroundColor: 'rgba(255, 221, 153,0.6)',
    borderColor: 'rgba(255, 221, 153,1)',
    pointBackgroundColor: 'rgba(255, 221, 153,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(255, 221, 153,1)'
};
export const lineColorLightGray = { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
};
export const lineColorGreen = {
    backgroundColor: 'rgba(0, 230, 77,0.6)',
    borderColor: 'rgba(0, 230, 77,1)',
    pointBackgroundColor: 'rgba(0, 230, 77,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(0, 230, 77,1)'
};
export const lineColorBlue = {
    backgroundColor: 'rgba(0, 0, 255,0.6)',
    borderColor: 'rgba(0, 0, 255,1)',
    pointBackgroundColor: 'rgba(0, 0, 255,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(0, 0, 255,1)'
};
export const lineColorRed = {
    backgroundColor: 'rgba(255, 0, 0,0.6)',
    borderColor: 'rgba(255, 0, 0,1)',
    pointBackgroundColor: 'rgba(255, 0, 0,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(255, 0, 0,1)'
};
export const lineColorDarkGray = {
    backgroundColor: 'rgba(77,83,96,0.2)',
    borderColor: 'rgba(77,83,96,1)',
    pointBackgroundColor: 'rgba(77,83,96,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(77,83,96,1)'
};

@Component({
    selector: 'ams-chart-line',
    templateUrl: './ams-chart-line.component.html',
})

export class AmsChartLineComponent implements OnInit, OnDestroy {

    /**
     * BINDINGS
     */
    @Input() chartData: any;
    @Input() chartLabels: string[];
    @Input() title: string;
    @Input() legend: boolean;
    @Input() environment: string;
    @Input() sizepx: number;
    @Input() yAxisShow: boolean;
    @Input() xAxisShow: boolean;
    @Input() chartColors: Color[];
    @Input() fontColor: string;

    @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

    /**
     * FIELDS
     */
    chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: 'Tso Search Hits for last 14 days',
            fontColor: 'white',
        },
        scales:
        {
            yAxes: [{
                display: false,
                drawOnChartArea: true,
                color: 'rgba(0, 0, 0, 0.1)',
                ticks: {
                    fontColor: 'white',
                },
                gridLines: {
                    display: true,
                    color: '#5f5e5e',
                }
            }],
            xAxes: [{
                display: true,
                drawOnChartArea: true,
                color: 'white',
                ticks: {
                    fontColor: 'white',
                },
                gridLines: {
                    display: true,
                    color: '#5f5e5e',
                }
            }]
        },
        scaleShowLabels: true,
        legend: {
            display: true,
            labels: {
                fontColor: 'white',
            },
        }
    };

    lineChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // yelow grey
            backgroundColor: 'rgba(255, 221, 153,0.6)',
            borderColor: 'rgba(255, 221, 153,1)',
            pointBackgroundColor: 'rgba(255, 221, 153,1)',
            pointBorderColor: '#ffdd99',
            pointHoverBackgroundColor: '#ffdd99',
            pointHoverBorderColor: 'rgba(255, 221, 153,1)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
            backgroundColor: 'rgba(255,0,0,0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    styleSize = 'height: 100%;width: 100%;';
    constructor(private cus: CurrentUserService) {
    }


    ngOnInit(): void {
        this.chartOptions.title.display = this.title ? true : false;
        this.chartOptions.legend.display = this.legend ? true : false;
        this.chartOptions.title.text = this.title ? this.title : '';

        if (this.environment) {
            this.chartOptions.title.text += ' (' + this.environment + ')';
        }
        if (this.sizepx) {
            this.styleSize = 'height: ' + this.sizepx.toString() + 'px;width: ' + (this.sizepx * 2).toString() + 'px;';
        }
        if (this.fontColor) {
            this.chartOptions.scales.xAxes[0].ticks.fontColor = this.fontColor;
            this.chartOptions.scales.yAxes[0].ticks.fontColor = this.fontColor;
            this.chartOptions.title.fontColor = this.fontColor;
        }
        if (this.yAxisShow) {
            this.chartOptions.scales.yAxes[0].display = this.yAxisShow === true ? true : false;
        }
        if (this.xAxisShow) {
            this.chartOptions.scales.xAxes[0].display = this.xAxisShow === true ? true : false;
        }
        if (this.chartColors) {
            this.lineChartColors = this.chartColors;
        }
    }

    ngOnDestroy(): void {
    }

    onChartClick(event) {
        console.log('onChartClick() -> event: ', event);
        this.chartClick.emit(event);
    }

}
