import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
// Services
import { WHITE_ON_BLACK_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { BaseChartDirective, Color, SingleDataSet } from 'ng2-charts';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

export const chartColorYellow = 'rgba(148,159,177,0.5)';
export const chartColorLightGray = 'rgba(255, 221, 153,0.5)';
export const chartColorGreen = 'rgba(0, 230, 77,0.5)';
export const chartColorBlue = 'rgba(0, 0, 255,0.5)';
export const chartColorRed = 'rgba(255, 0, 0,0.5)';
export const chartColoDarkGray = 'rgba(77,83,96,0.2)';

@Component({
    selector: 'ams-chart-pie-sm',
    templateUrl: './ams-chart-pie-sm.component.html',
})
export class AmsChartPieSmallComponent implements OnInit, OnDestroy {

    /**
     * BINDINGS
     */
    @Input() chartData: SingleDataSet;
    @Input() chartLabels: string[];
    @Input() title: string;
    @Input() sizepx: number;
    @Input() legend: boolean;
    @Input() environment: string;
    @Input() chartColors: string[];
    @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();


    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

    /**
     * FIELDS
     */
    chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: true,
            text: 'Tso Search Hits for last 14 days',
            fontColor: 'blue',
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'blue',
            },
        }
    };
    pieChartColors = [
        {
            backgroundColor: ['rgba(148,159,177,0.5)', 'rgba(255, 221, 153,0.5)', 
            'rgba(0, 230, 77,0.5)', 'rgba(0, 0, 255,0.5)', 'rgba(77,83,96,0.5)'],
        },
    ];
    lineChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // yelow grey
            backgroundColor: 'rgba(255, 221, 153,0.6)',
            borderColor: 'rgba(255, 221, 153,1)',
            pointBackgroundColor: 'rgba(255, 221, 153,1)',
            pointBorderColor: '#ffdd99',
            pointHoverBackgroundColor: '#ffdd99',
            pointHoverBorderColor: 'rgba(255, 221, 153,1)'
        },
        { // green
            backgroundColor: 'rgba(0, 179, 60,0.5)',
            borderColor: 'green',
            pointBackgroundColor: 'rgba(0, 102, 34,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0, 102, 34,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
            backgroundColor: 'rgba(255,0,0,0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    styleSize = 'height: 100px;width: 100px;';
    constructor(private cus: CurrentUserService) {
    }


    ngOnInit(): void {
        this.chartOptions.title.display = this.title ? true : false;
        this.chartOptions.legend.display = this.legend ? true : false;
        this.chartOptions.title.text = (this.title ? this.title : '') + ' (' + this.environment + ')';
        if (this.sizepx) {
            this.styleSize = 'height: ' + this.sizepx.toString() + 'px;width: ' + this.sizepx.toString() + 'px;';
        }
        if (this.chartColors) {
            this.pieChartColors = [{ backgroundColor: this.chartColors, }];
        }
    }

    ngOnDestroy(): void {
    }

    onChartClick(event) {
        console.log('onChartClick() -> event: ', event);
        this.chartClick.emit(event);
    }

}
