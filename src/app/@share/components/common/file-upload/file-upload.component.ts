import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
@Component({
    selector: 'sx-file-upload',
    templateUrl: './file-upload.component.html'
})

export class FileUploadComponent implements OnInit {


    constructor() { }

    /**
     * BIINDING
     */
    @Input() uploadedFile: boolean;
    @Input() position: string = 'above';
    @Input() size: number;


    /**
     * FIELDS
     */

    ngOnInit() { }



}