import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

@Component({
    selector: 'sx-spinner',
    templateUrl: './spinner.component.html',
    animations: [
        trigger('openClose', [
            // ...
            state('open', style({
                opacity: 0.8,
            })),
            state('closed', style({
                height: '0px', opacity: '0', display: 'none'
            })),
            transition('* => closed', [
                animate('500ms',
                    style({ opacity: '*' }),
                ),
            ]),
            transition('* => open', [
                animate('0.8s')
            ]),
        ]),
    ],
})

export class SpinnerComponent implements OnInit, OnDestroy {

    showLoader = true;
    showLoaderSubscr: Subscription;
    loaderMessage = 'Please Wait';
    loaderMessageSubscr: Subscription;
    openLoaderSubscr: Subscription;
    isOpen = true;
    private selector = 'globalLoader';

    constructor(
        private spinnerService: SpinnerService,) {
    }

    ngOnInit(): void {
        this.showLoaderSubscr = this.spinnerService.status.subscribe((val: boolean) => {
            this.showLoader = val;
            val ? this.show() : this.hide();
        });

        this.loaderMessageSubscr = this.spinnerService.message.subscribe((val: string) => {
            this.loaderMessage = val;
        });

    }

    ngOnDestroy(): void {
        if (this.showLoaderSubscr) { this.showLoaderSubscr.unsubscribe(); }
        if (this.loaderMessageSubscr) { this.loaderMessageSubscr.unsubscribe(); }
    }


    private getElement(): any {
        return document.getElementById(this.selector);
    }

    hide(): void {
        this.isOpen = false;
        const el = this.getElement();
        if (el) {
            /*
            el.addEventListener('transitionend', () => {
                el.className = 'global-loader-hidden';
            });
            */
            // el.className += 'global-loader global-loader-hidden';
        }
    }

    show(): void {
        this.isOpen = true;
        const el = this.getElement();
        if (el) {
            // el.className = 'global-loader global-loader-fade-in';
        }
    }

    toggle(): void {
        this.isOpen = !this.isOpen;
    }
}
