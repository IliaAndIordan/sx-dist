import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
    selector: 'sx-sidebar-nav',
    templateUrl: 'sx-sidebar-nav.component.html'
})

export class SxSidebarNavComponent implements OnInit, OnDestroy {



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private spinerService: SpinnerService,
        private cus: CurrentUserService) { }


    /**
     * FIELDS
     */
    roots = AppRoutes;
    isAuthenticated: boolean;
    isAdmin: boolean;
    isDistributor: boolean;
    user: UserModel;
    userChanged: Subscription;

    ngOnInit(): void {
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields(): void{
        this.user = this.cus.user;
        this.isAuthenticated = this.cus.isAuthenticated;
        this.isAdmin = this.cus.isAdmin;
        this.isDistributor = this.cus.isDistributor;
    }

    logout(): void {
        // console.log('logout->');
        this.spinerService.show();
        this.cus.logout().subscribe((res: boolean) => {
            this.spinerService.hide();
            this.router.navigate([AppRoutes.Root, AppRoutes.public])
        });
    }

}
