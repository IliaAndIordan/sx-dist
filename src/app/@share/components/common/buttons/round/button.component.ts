import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'sx-button-round',
  templateUrl: './button.component.html'
})
// tslint:disable-next-line: component-class-suffix
export class SxButtonRound {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() icon: string;
  @Input() message: string;
  @Input() color: string;
  @Input() position: string = 'above';
  @Input() disabled: boolean;



  /**
   * FIELDS
   */

}
