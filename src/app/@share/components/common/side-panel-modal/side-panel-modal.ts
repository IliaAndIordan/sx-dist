import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Animate } from 'src/app/@core/const/animation.const';

// Animations 
import { ExpandSidePanelTrigger, ShowHideTriggerBlock } from './animation';


@Component({
  selector: 'sx-side-panel-modal',
  templateUrl: './side-panel-modal.html',
  styles: [],
  animations: [ExpandSidePanelTrigger,
    ShowHideTriggerBlock]
})
export class SxSidePanelModalComponent implements OnInit {

  constructor() { }


  /**
   * BINDINGS
   */
  @Output() closeSmpClick: EventEmitter<any> = new EventEmitter<any>();



  /**
   * FIELDS
   */
  expandPanelVar: string = Animate.hide;

  ngOnInit(): void { }

  public expandPanel(): void {
    this.expandPanelVar = Animate.show;
  }

  public closePanel(): void {
    this.expandPanelVar = Animate.hide;
  }

  handleClosePanelClick(): void {
    this.closeSmpClick.emit();
  }

}