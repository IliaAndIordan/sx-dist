import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { ValueTypes } from '../row/side-panel-row-value.component';


@Component({
    selector: 'sx-spm-bom-info',
    templateUrl: './spm-bom-info.component.html',
})
export class SxSpmBomInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() bom: Bom;

    /**
     * FIELDS
     */
    vtypes = ValueTypes;

    constructor(
        private gravatarService: GravatarService,
        private cus: CurrentUserService) {
    }

    ngOnInit(): void  {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['bom']) {
            this.bom = changes['bom'].currentValue;
            this.initFields();
        }
    }

    initFields(): void  {
        if (this.bom) {
            /*
            this.managerAvUrl = this.project.manager_email ? 
                this.gravatarService.url(this.project.manager_email, 128, 'identicon') : URL_NO_IMG_SQ;
            this.estimatorAvUrl = this.project.estimator_email ? 
                this.gravatarService.url(this.project.estimator_email, 128, 'identicon') : URL_NO_IMG_SQ;
                */
        }
    }

}
