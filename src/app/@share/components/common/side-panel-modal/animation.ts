import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animate } from 'src/app/@core/const/animation.const';

export const ExpandSidePanelTrigger =
  trigger('expandSidePanelTrigger', [
    state(Animate.hide, style({
      right: -400
    })),
    state(Animate.show, style({
      right: 0
    })),
    transition(Animate.hide + '<=>' + Animate.show, animate('400ms'))
  ])

export const ShowHideTriggerBlock =
  trigger('showHideTriggerBlock', [
    state(Animate.show, style({
      opacity: '1',
      display: 'block'
    })),
    state(Animate.hide, style({
      opacity: '0',
      display: 'none'
    })),
    transition(Animate.show + ' <=> ' + Animate.hide, animate('300ms'))
  ])


