import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
export enum ValueTypes {
  Number = 1,
  Date = 2,
  DateTime = 2,
  Price = 3,
  String = 4,
  BooleanType = 5,
  HTML = 6,
}

@Component({
  selector: 'sx-side-panel-row-value',
  templateUrl: 'side-panel-row-value.component.html',
})
export class SidePanelRowValueComponent implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() label: string;
  @Input() value: any;

  @Input() infoMessage: string;

  @Input() vtype: ValueTypes = ValueTypes.String;
  @Input() size = 'small'; // accepts 'small', 'med' or large'
  @Input() valueCap = 30;

  /**
   * FIELDS
   */
  get valueFloat(): number {
    let rv: number;
    if (this.isPrice) {
      rv = parseFloat(this.value);
    }
    return rv;
  }

  get isDate(): boolean {
    return  this.value  && ( this.value instanceof Date  || this.vtype === ValueTypes.Date);
  }
  get isDateTime(): boolean {
    return  this.value  && ( this.value instanceof Date  && this.vtype === ValueTypes.DateTime);
  }

  get isBoolean(): boolean {
    return  this.value  && ( this.value instanceof Boolean  || this.vtype === ValueTypes.BooleanType);
  }

  get isNumber(): boolean {
    return  this.value  &&  this.vtype === ValueTypes.Number;
  }

  get isPrice(): boolean {
    return this.value  && this.vtype === ValueTypes.Price;
  }

  get isString(): boolean {
    return this.value  && this.vtype === ValueTypes.String;
  }

  get isHtml(): boolean {
    return this.value  && this.vtype === ValueTypes.HTML;
  }


  ngOnInit(): void {
    // console.log(this.size);
    if (this.size === 'large') {
      this.valueCap = 70;
    }
  }

  // isNumber(val) { return typeof val === 'number'; }


}// end class
