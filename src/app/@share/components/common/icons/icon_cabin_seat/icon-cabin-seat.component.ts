import { Component, Input } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-cabin-seat',
    templateUrl: './icon-cabin-seat.component.html',
})

export class IconCabinSeatComponent {

    /**
     * Bindings
     */
    @Input() icon: string;
    @Input() seatE: number;
    @Input() seatB: number;
    @Input() seatF: number;


}
