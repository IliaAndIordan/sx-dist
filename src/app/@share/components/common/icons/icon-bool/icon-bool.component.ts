import { Component, EventEmitter, Input, Output } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-icon-bool',
    templateUrl: './icon-bool.component.html',
})

export class IconBoolComponent {

    /**
     * Bindings
     */
    @Input() value: any;
    @Output() btnClick: EventEmitter<any> = new EventEmitter<any>();

    handleClick(event: any) {
        this.btnClick.emit('emit!');
      }
    
}
