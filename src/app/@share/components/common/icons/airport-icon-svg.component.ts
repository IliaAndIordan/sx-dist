import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

const fill_color_yellow = '#ffffcc';
const fill_color_green = '#ccffcc';
const fill_color_blue = '#b3d1ff';
const fill_color_gray = '#e6e6e6';

@Component({
    selector: 'ams-ap-icon-svg',
    templateUrl: './airport-icon-svg.component.html',
})

export class AmsAirportIconSvgComponent implements OnInit, OnChanges {

    /**
     * Bindings
     */
    @Input() rw_svg_line: string;
    @Input() rw_svg_path_circle: string;
    @Input() ap_active: boolean;
    @Input() size: number;


    /**
     * FIELDS
     */
    fillColor: string;
    ap_circle: string;
    rw_lines: string;
    rw_line_color = 'blue';
    ap_circle_color = 'black';
    cssStyle: string;

    constructor(private router: Router) { }

    ngOnInit(): void {
        this.fillColor = fill_color_gray;
        this.initFields();
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['rw_svg_line']) {
            this.rw_svg_line = changes['rw_svg_line'].currentValue;
            this.initFields();
        }
        if (changes['rw_svg_path_circle']) {
            this.rw_svg_path_circle = changes['rw_svg_path_circle'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        // this.selCountry = this.regionDs.selCountry;
        this.fillColor = this.ap_active ? fill_color_yellow : fill_color_gray;
        this.ap_circle = this.rw_svg_path_circle + ' z';
        this.rw_lines = this.rw_svg_line + ' z';
        this.rw_line_color = this.ap_active ? 'blue' : 'red';
        this.ap_circle_color = this.ap_active ? 'black' : 'orange';
        this.cssStyle = this.getStyle();
    }

    getStyle(): string {
        let rv = 'ap-svg';
        if (this.size) {
            rv = 'ap-svg-' + this.size.toString();
        }
        return rv;
    }

    //#region Actions

    //#endregion
}
