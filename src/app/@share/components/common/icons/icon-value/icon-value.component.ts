import { Component, Input } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-icon-value',
    templateUrl: './icon-value.component.html',
})

export class IconValueComponent {

    /**
     * Bindings
     */
    @Input() icon: string;
    @Input() value: any;
    @Input() label: string;
    @Input() tooltip: string;
    @Input() pipe: string;

    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;

}
