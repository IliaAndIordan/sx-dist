import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'sx-key-value-block',
    templateUrl: 'key-value-block.component.html',
    styleUrls: ['./key-value-block.component.css']
})
export class KeyValueBlockComponent implements OnInit{
 
  constructor( ) { }

  /**
   * BINDINGS
   */
  @Input() key: string;
  @Input() infoMessage: string;
  @Input() value: string;
  @Input() price: boolean;
  @Input() priceId: number;
  @Input() size: string = 'medium'; // accepts 'small', 'medium' or 'large'
  @Input() valueCap:number = 16;
  
  /**
   * FIELDS
   */

  ngOnInit() {
    if(this.size == 'large'){
      this.valueCap = 70;
    }
  }

  isNumber(val) { return typeof val === 'number'; }

  
}// end class
