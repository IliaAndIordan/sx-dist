import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { SxNavbarComponent } from './navbar/sx-navbar.component';
import { SxMaterialModule } from '../material.module';
import { SxSidebarNavComponent } from './sidebar/nav/sx-sidebar-nav.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SxButtonRound } from './buttons/round/button.component';
import { SxSidePanelModalComponent } from './side-panel-modal/side-panel-modal';
import { SxFilterPanelComponent } from './filter-panel/filter-panel.component';
import { SxButtonFlat } from './buttons/flat/button.component';
import { SxFormatControl } from './buttons/format-control/format-control.component';
import { SxSpmProjectInfoComponent } from './side-panel-modal/project/spm-project-info.component';
import { SidePanelRowValueComponent } from './side-panel-modal/row/side-panel-row-value.component';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { ButtonInfo } from './buttons/info/button.component';
import { ImageLinkComponent } from './buttons/image-link/image-link.component';
import { IconValueComponent } from './icons/icon-value/icon-value.component';
import { IconBoolComponent } from './icons/icon-bool/icon-bool.component';
import { IconAirportActiveComponent } from './icons/icon-ap-active/icon-bool.component';
import { AmsAirportIconSvgComponent } from './icons/airport-icon-svg.component';
import { KeyValueBlockComponent } from './key-value-block/key-value-block.component';
import { SxSpmBomInfoComponent } from './side-panel-modal/bom/spm-bom-info.component';
import { FileUploadComponent } from './file-upload/file-upload.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    SxPipesModule,
  ],
  declarations: [
    SpinnerComponent,
    SxNavbarComponent,
    SxSidebarNavComponent,
    // -Buttons
    SxButtonRound,
    SxButtonFlat,
    ButtonInfo,
    SxFormatControl,
    ImageLinkComponent,
    IconValueComponent,
    IconBoolComponent,
    IconAirportActiveComponent,
    // - Tools (Icon, Key-Value, File Select)
    AmsAirportIconSvgComponent,
    KeyValueBlockComponent,
    FileUploadComponent,
    // -SPM Panels
    SxSidePanelModalComponent,
    SidePanelRowValueComponent,
    SxSpmProjectInfoComponent,
    SxSpmBomInfoComponent,
    // -Filter Panels
    SxFilterPanelComponent,
  ],
  exports: [
    SxMaterialModule,
    SpinnerComponent,
    SxNavbarComponent,
    SxSidebarNavComponent,
    // -Buttons
    SxButtonRound,
    SxButtonFlat,
    ButtonInfo,
    SxFormatControl,
    ImageLinkComponent,
    IconValueComponent,
    IconBoolComponent,
    IconAirportActiveComponent,
    // - Tools (Icon, Key-Value, File Select)
    AmsAirportIconSvgComponent,
    KeyValueBlockComponent,
    FileUploadComponent,
    // -SPM Panels
    SxSidePanelModalComponent,
    SidePanelRowValueComponent,
    SxSpmProjectInfoComponent,
    SxSpmBomInfoComponent,
    // -Filter Panels
    SxFilterPanelComponent,
  ]
})

export class SxComponentsCommonModule { }
