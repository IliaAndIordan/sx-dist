import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animate } from 'src/app/@core/const/animation.const';


export const TogglePanelTrigger =
    trigger('TogglePanelTrigger', [
        state(Animate.in, style({
            left: '-450px',
            display: 'none',
            opacity: '0',
        })),
        state(Animate.out, style({
            left: '0px',
            display: 'flex',
            'flex-direction': 'column',
            opacity: '1',
        })),
        transition(Animate.out + '=>' + Animate.in, animate('300ms')),
        transition(Animate.in + '=>' + Animate.out, animate('300ms'))
    ])


export const ToggleSectionListTrigger =
    trigger('toggleSectionListTrigger', [
        state(Animate.hide, style({
            display: 'none',
            transform: 'translateY(-10px)',
            opacity: '0',
        })),
        state('show', style({
            display: 'block',
            transform: 'translateY(0px)',
            opacity: '1'
        })),
        transition(Animate.hide + '<=>' + Animate.show, animate('400ms'))
    ]);


export const SpinIconTrigger =
    trigger('spinIconTrigger', [
        state('normal', style({
            transform: "rotate(0deg)"
        })),
        state('turned', style({
            transform: "rotate(90deg)"
        })),
        transition(Animate.normal + '<=>' + Animate.turned, animate('400ms'))
    ]);


export const ToggleDrillDownTrigger =
    trigger('toggleDrillDownTrigger', [
        state('hide', style({
            display: 'none',
            opacity: '0'
        })),
        state('show', style({
            display: 'flex',
            opacity: '1'
        })),
        transition(Animate.hide + '=>' + Animate.show, animate('500ms 200ms')),
        transition(Animate.show + '=>' + Animate.hide, animate('600ms')),
    ]);





