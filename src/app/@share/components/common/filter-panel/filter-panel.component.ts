import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Animate } from 'src/app/@core/const/animation.const';
import { TogglePanelTrigger } from './animation';

@Component({
  selector: 'sx-filter-panel',
  templateUrl: './filter-panel.component.html',
  animations: [TogglePanelTrigger]
})

export class SxFilterPanelComponent implements OnInit {

  /** Binding */
  @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
  @Output() subtitleClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() panelVar: string;
  @Input() fptitle: string;
  @Input() subtitle: string;
  /** FIELDS */


  constructor() { }


  ngOnInit() {
  }

  public openPanelClick(): void {
    // this.panelVar = Animate.out;
  }

  closePanelClick(): void {
    // this.panelVar = Animate.in;
    this.closePanel.emit();
  }

  subtitleClickEv(): void {
    // this.panelVar = Animate.in;
    this.subtitleClick.emit();
  }

}
