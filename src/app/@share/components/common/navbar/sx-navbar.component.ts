import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscribable, Subscription } from 'rxjs';
// Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// Models
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_IZIORDAN, COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';



@Component({
    selector: 'ts-sx-navbar',
    templateUrl: './sx-navbar.component.html',
    // animations: [PageTransition]
})
export class SxNavbarComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Fields
     */
    roots = AppRoutes;
    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    isAuthenticated: boolean;
    isAdmin: boolean;
    user: UserModel;
    userChanged: Subscription;
    userName: string;
    avatarUrl: string;
    env: string;


    constructor(private router: Router,
                private route: ActivatedRoute,
                private toastr: ToastrService,
                private spinerService: SpinnerService,
                private cus: CurrentUserService) { }

    ngOnInit(): void {
        this.user = this.cus.user;
        this.env = environment.abreviation;
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields(): void {
        this.user = this.cus.user;
        this.avatarUrl = this.cus.avatarUrl;
        this.isAuthenticated = this.cus.isAuthenticated;
        this.isAdmin = this.cus.isAdmin;
        let name = 'Guest';
        if (this.cus.user) {
            name = this.cus.user.user_name ? this.cus.user.user_name : this.cus.user.e_mail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    //#region Action Methods

    gotoProfile(): void {
        this.router.navigate([AppRoutes.Root, AppRoutes.profile]);
    }


    logout(): void {
        // console.log('logout->');
        this.spinerService.show();
        this.cus.logout().subscribe((res: boolean) => {
            this.spinerService.hide();
            this.router.navigate([AppRoutes.Root, AppRoutes.public])
        });
    }
    //#endregion
}
