import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxComponentsCommonModule } from './common/components-common.module';
import { SxMaterialModule } from './material.module';
import { AmsUserEditDialog } from './dialogs/user-edit/user-edit.dialog';
import { SxComponentsCardsModule } from './cards/components-cards.module';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';


@NgModule({
  imports: [
    CommonModule,
    SxPipesModule,
    SxMaterialModule,
    //
    SxComponentsCommonModule,
    SxComponentsCardsModule,
  ],
  declarations: [
  ],
  exports: [
    SxComponentsCommonModule,
    SxComponentsCardsModule,
    SxMaterialModule,
  ],
  entryComponents: [
  ]
})

export class SxComponentsModule { }
