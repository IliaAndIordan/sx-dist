import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


export interface IBomEdit {
    bom: Bom;
}

@Component({
    templateUrl: './bom-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class SxBomEditDialog implements OnInit {

    formGrp: FormGroup;

    bom: Bom;
    company: CompanyModel;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: SxApiBomClient,
        public dialogRef: MatDialogRef<IBomEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IBomEdit) {
        this.bom = data.bom;
    }
    get bomName(): AbstractControl { return this.formGrp.get('bomName'); }
    get description(): AbstractControl { return this.formGrp.get('description'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.company = this.cus.company;

        this.formGrp = this.fb.group({
            bomName: new FormControl(this.bom ? this.bom.bomName : '', [Validators.required, Validators.maxLength(256)]),
            description: new FormControl(this.bom ? this.bom.description : '', [Validators.maxLength(512)]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit(): void {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.bom) {
                this.bom = new Bom();
                this.bom.companyId = this.company.company_id;
                this.bom.userId = this.cus.user.userId;
            }
            const message = this.bom.bomId > 0 ? 'Update' : 'Create';

            this.bom.bomName = this.bomName.value;
            this.bom.description = this.description.value;


            this.spinerService.display(true);

            this.client.bomSave(this.bom)
                .subscribe((res: Bom) => {
                    this.spinerService.display(false);
                    console.log('countrySave -> res:', res);
                    if (res && res.bomId) {
                        this.bom = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success(`Operation Succesfull: BOM ${message}`, 'Bill Of Materials');
                        this.dialogRef.close(this.bom);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'BOM ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            // this.dialogRef.close(undefined);
                        }, 2000);
                        this.spinerService.display(false);
                        this.toastr.error(`Operation Failed: Bom ${message}`, 'Bill Of Materials');
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
