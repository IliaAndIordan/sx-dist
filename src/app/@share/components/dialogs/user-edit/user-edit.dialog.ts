import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';

export interface IAmsUserEdit {
    user: UserModel;
}

@Component({
    templateUrl: './user-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class AmsUserEditDialog implements OnInit {

    formGrp: FormGroup;

    user: UserModel;
    userId: number;
    roleOpt = UserRoleOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private authClient: ApiAuthClient,
        public dialogRef: MatDialogRef<IAmsUserEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsUserEdit) {
        this.user = data.user;
    }

    get userName(): any { return this.formGrp.get('userName'); }
    get userEmail(): any { return this.formGrp.get('userEmail'); }
    get userPwd(): any { return this.formGrp.get('userPwd'); }
    get userRole(): any { return this.formGrp.get('userRole'); }
    get isReceiveEmails(): any { return this.formGrp.get('isReceiveEmails'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.userId = this.user ? this.user.userId : undefined;

        this.formGrp = this.fb.group({
            userName: new FormControl(this.user ? this.user.user_name : '', [Validators.required, Validators.maxLength(256)]),
            userEmail: new FormControl(this.user ? this.user.e_mail : '', 
                [Validators.required, Validators.email, Validators.maxLength(512)]),
            userPwd: new FormControl(this.user ? this.user.u_password : '', [Validators.required, Validators.maxLength(50)]),
            userRole: new FormControl(this.user ? this.user.user_role : UserRole.User, [Validators.required]),
            isReceiveEmails: new FormControl(this.user ? this.user.is_receive_emails : false, []),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit(): void {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.user) {
                this.user = new UserModel();
            }
            const message = this.user.userId > 0 ? 'Update' : 'Create';

            this.user.user_name = this.userName.value;
            this.user.e_mail = this.userEmail.value;
            this.user.u_password = this.userPwd.value;
            this.user.user_role = this.userRole.value;
            this.user.is_receive_emails = this.isReceiveEmails.value;

            // this.spinerService.display(true);

            this.authClient.userSave(this.user)
                .subscribe((res: UserModel) => {
                    // this.spinerService.display(false);
                    console.log('userSave -> res:', res);
                    if (res && res.userId) {
                        this.user = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('User', 'Operation Succesfull: User ' + message);
                        this.dialogRef.close(this.user);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'User ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
