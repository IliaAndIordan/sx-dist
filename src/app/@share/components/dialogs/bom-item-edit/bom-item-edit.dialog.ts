import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Bom, BomItem } from 'src/app/@core/services/api/bom/dto';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


export interface IBomItemEdit {
    bomItem: BomItem;
    nextSequenceNr?: number;
    bom: Bom;
}

@Component({
    templateUrl: './bom-item-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class SxBomItemEditDialog implements OnInit {

    formGrp: FormGroup;

    bomItem: BomItem;
    bom: Bom;
    nextSequenceNr?: number;
    company: CompanyModel;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: SxApiBomClient,
        public dialogRef: MatDialogRef<IBomItemEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IBomItemEdit) {
        this.bomItem = data.bomItem;
        this.bom = data.bom;
        this.nextSequenceNr = data.nextSequenceNr ? data.nextSequenceNr : (this.bom ? this.bom.itemsCount + 1 : 1);
    }
    get sequenceNr(): AbstractControl { return this.formGrp.get('sequenceNr'); }
    get qauntity(): AbstractControl { return this.formGrp.get('qauntity'); }
    get quantityUom(): AbstractControl { return this.formGrp.get('quantityUom'); }
    get upc(): AbstractControl { return this.formGrp.get('upc'); }
    get ean(): AbstractControl { return this.formGrp.get('ean'); }
    get gtin(): AbstractControl { return this.formGrp.get('gtin'); }
    get mfrName(): AbstractControl { return this.formGrp.get('mfrName'); }
    get mfrCatalogCode(): AbstractControl { return this.formGrp.get('mfrCatalogCode'); }
    get itemName(): AbstractControl { return this.formGrp.get('itemName'); }
    get distributorCode(): AbstractControl { return this.formGrp.get('distributorCode'); }
    get notes(): AbstractControl { return this.formGrp.get('notes'); }



    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.company = this.cus.company;

        this.formGrp = this.fb.group({
            sequenceNr: new FormControl(this.bomItem && this.bomItem.sequenceNr ?
                this.bomItem.sequenceNr : this.nextSequenceNr, [Validators.required, Validators.min(0)]),
            qauntity: new FormControl(this.bomItem && this.bomItem.qauntity ?
                this.bomItem.qauntity : 1, [Validators.required, Validators.min(1)]),
            quantityUom: new FormControl(this.bomItem && this.bomItem.quantityUom ?
                this.bomItem.quantityUom : 'EA', [Validators.required, Validators.maxLength(45)]),
            upc: new FormControl(this.bomItem && this.bomItem.upc ? this.bomItem.upc : '', [Validators.maxLength(12)]),
            ean: new FormControl(this.bomItem && this.bomItem.ean ? this.bomItem.ean : '', [Validators.maxLength(13)]),
            gtin: new FormControl(this.bomItem && this.bomItem.gtin ? this.bomItem.gtin : '', [Validators.maxLength(14)]),
            mfrName: new FormControl(this.bomItem && this.bomItem.mfrName ? this.bomItem.mfrName : '', [Validators.maxLength(256)]),
            mfrCatalogCode: new FormControl(this.bomItem && this.bomItem.mfrCatalogCode ?
                this.bomItem.mfrCatalogCode : '', [Validators.maxLength(256)]),
            itemName: new FormControl(this.bomItem && this.bomItem.itemName ? this.bomItem.itemName : '', [Validators.maxLength(256)]),
            distributorCode: new FormControl(this.bomItem && this.bomItem.distributorCode ?
                this.bomItem.distributorCode : '', [Validators.maxLength(256)]),
            notes: new FormControl(this.bomItem && this.bomItem.notes ? this.bomItem.notes : '', [Validators.maxLength(256)]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit(): void {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.bomItem) {
                this.bomItem = new BomItem();
                this.bomItem.bomId = this.bom.bomId;
            }
            const message = this.bomItem.bomItemId > 0 ? 'Update' : 'Create';

            this.bomItem.sequenceNr = this.sequenceNr.value;
            this.bomItem.qauntity = this.qauntity.value;
            this.bomItem.quantityUom = this.quantityUom.value;
            this.bomItem.upc = this.upc.value;
            this.bomItem.ean = this.ean.value;
            this.bomItem.gtin = this.gtin.value;
            this.bomItem.mfrName = this.mfrName.value;
            this.bomItem.mfrCatalogCode = this.mfrCatalogCode.value;
            this.bomItem.itemName = this.itemName.value;
            this.bomItem.distributorCode = this.distributorCode.value;
            this.bomItem.notes = this.notes.value;


            this.spinerService.display(true);

            this.client.bomItemSave(this.bomItem)
                .subscribe((res: BomItem) => {
                    this.spinerService.display(false);
                    console.log('bomItemSave -> res:', res);
                    if (res && res.bomItemId) {
                        this.bomItem = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success(`Operation Succesfull: Item ${message}`, 'Bill Of Materials');
                        this.dialogRef.close(this.bomItem);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Item ' + message + ' Failed. ' + err;
                        setTimeout((router: Router) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            // this.dialogRef.close(undefined);
                        }, 2000);
                        this.spinerService.display(false);
                        this.toastr.error(`Operation Failed: Item ${message}`, 'Bill Of Materials');
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
