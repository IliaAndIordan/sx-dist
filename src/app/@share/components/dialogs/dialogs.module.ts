import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../material.module';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { AmsUserEditDialog } from './user-edit/user-edit.dialog';
import { SxBomEditDialog } from './bom-edit/bom-edit.dialog';
import { SxBomItemEditDialog } from './bom-item-edit/bom-item-edit.dialog';


@NgModule({
  imports: [
    CommonModule,
    SxPipesModule,
    SxMaterialModule,
    //
    SxComponentsCommonModule,
  ],
  declarations: [
    // SxUseCaseEditDialog,
    AmsUserEditDialog,
    SxBomEditDialog,
    SxBomItemEditDialog,
  ],
  exports: [

  ],
  entryComponents: [
    // SxUseCaseEditDialog,
    AmsUserEditDialog,
    SxBomEditDialog,
    SxBomItemEditDialog,
  ]
})

export class SxDialogssModule { }
