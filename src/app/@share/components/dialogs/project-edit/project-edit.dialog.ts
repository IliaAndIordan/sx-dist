import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { SxProjectModel } from 'src/app/@core/services/api/project/dto';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { SxProjectStatus, SxProjectStatusOpt } from 'src/app/@core/services/api/project/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiCompanyClient } from 'src/app/@core/services/api/company/api-client';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';

export interface ISxProjectEdit {
    project: SxProjectModel;
    company: CompanyModel;
}

@Component({
    templateUrl: './project-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class SxProjectEditDialog implements OnInit {

    formGrp: FormGroup;

    project: SxProjectModel;
    company: CompanyModel;
    userList: Array<UserModel>;
    estimatorOpt: Observable<UserModel[]>;
    managerOpt: Observable<UserModel[]>;
    statusOpt = SxProjectStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private cClient: SxApiCompanyClient,
        private pClient: SxApiProjectClient,
        public dialogRef: MatDialogRef<ISxProjectEdit>,
        @Inject(MAT_DIALOG_DATA) public data: ISxProjectEdit) {
        this.project = data.project;
        this.company = data.company;
    }

    get project_name() { return this.formGrp.get('project_name'); }
    get project_account_id() { return this.formGrp.get('project_account_id'); }
    get pstatus_id() { return this.formGrp.get('pstatus_id'); }
    get company_id() { return this.formGrp.get('company_id'); }
    get estimator_id() { return this.formGrp.get('estimator_id'); }
    get manager_id() { return this.formGrp.get('manager_id'); }
    get notes() { return this.formGrp.get('notes'); }
    get start_date() { return this.formGrp.get('start_date'); }
    get end_date() { return this.formGrp.get('end_date'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        if (!this.company) { this.company = this.cus.company; }

        this.formGrp = this.fb.group({
            project_name: new FormControl(this.project ? this.project.project_name : '', [Validators.required, Validators.maxLength(512)]),
            project_account_id: new FormControl(this.project ? this.project.project_account_id : '', [Validators.maxLength(256)]),
            pstatus_id: new FormControl(this.project ? this.project.pstatus_id : SxProjectStatus.Backlog, [Validators.required]),
            company_id: new FormControl(this.company ? this.company.company_id : '', [Validators.required]),
            estimator_id: new FormControl(this.project ? this.project.estimator_id : '', []),
            manager_id: new FormControl(this.project ? this.project.manager_id : '', []),
            notes: new FormControl(this.project ? this.project.notes : '', [Validators.maxLength(2000)]),
            start_date: new FormControl(this.project ? this.project.start_date : '', []),
            end_date: new FormControl(this.project ? this.project.end_date : '', []),

        });

        this.loadUserList();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.project) {
                this.project = new SxProjectModel();
            }
            const message = this.project.project_id > 0 ? 'Update' : 'Create';
            const estimator = this.estimator_id.value as UserModel;
            const manager = this.manager_id.value as UserModel;
            this.project.project_name = this.project_name.value;
            this.project.project_account_id = this.project_account_id.value;
            this.project.company_id = this.company_id.value;
            this.project.pstatus_id = this.pstatus_id.value;
            this.project.notes = this.notes.value;
            this.project.start_date = this.start_date.value;
            this.project.end_date = this.end_date.value;
            this.project.estimator_id = estimator ? estimator.userId : undefined;
            this.project.manager_id = manager ? manager.userId : undefined;

            // this.spinerService.display(true);

            this.pClient.projectSave(this.project)
                .subscribe({
                    next: (res: SxProjectModel) => {
                        // this.spinerService.display(false);
                        console.log('projectSave -> res:', res);
                        if (res && res.project_id) {
                            this.project = res;
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.toastr.success('Project', 'Operation Succesfull: Project ' + message);
                            this.dialogRef.close(this.project);
                        }
                    },
                    error: (err: any) => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Compnay ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(this.project);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    complete: () => {
                        console.log('complete');
                    }
                });

        }


    }

    //#region  Country Filter

    estimatorValueChange(event: UserModel) {
        console.log('estimatorValueChange: event:', event);
        console.log('estimatorValueChange: msg:', this.estimator_id.value);
        if (this.estimator_id.valid) {
            const user: UserModel = this.estimator_id.value;
        }
    }

    managerValueChange(event: UserModel) {
        console.log('managerValueChange: event:', event);
        console.log('managerValueChange: msg:', this.manager_id.value);
        if (this.manager_id.valid) {
            const user: UserModel = this.manager_id.value;
        }
    }

    displayUser(cm: UserModel): string {
        let rv: string = '';
        console.log('displayUser -> cm', cm);
        if (cm) {
            rv = ' ';
            rv = cm.user_name ? cm.user_name : cm.e_mail;
            // rv += cm.e_mail ? ' ' + cm.e_mail : ' ';
        }
        return rv;
    }

    initUserOpt() {
        this.estimatorOpt = of(this.userList);
        this.managerOpt = of(this.userList);
        this.estimatorOpt = this.estimator_id.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterUser(state) :
                    (this.userList ? this.userList.slice() : new Array<UserModel>()))
            );
        this.managerOpt = this.manager_id.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterUser(state) :
                    (this.userList ? this.userList.slice() : new Array<UserModel>()))
            );
        let estimator: UserModel;
        let manager: UserModel;
        if (this.userList && this.userList.length > 0) {
            estimator = this.project && this.project.estimator_id ? this.userList.find(x => x.userId === this.project.estimator_id) : undefined;
            manager = this.project && this.project.manager_id ? this.userList.find(x => x.userId === this.project.manager_id) : undefined;
        }

        this.formGrp.patchValue({ estimator_id: estimator });
        this.formGrp.patchValue({ manager_id: manager });
        this.formGrp.updateValueAndValidity();
    }

    filterUser(val: any): UserModel[] {
        // console.log('filterDpc -> val', val);
        let user: UserModel;
        if (val && val.userId) {
            user = val as UserModel;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.user_name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<UserModel>()
        if (this.userList && this.userList.length) {
            rv = this.userList.filter(x => (
                x.user_name.toLowerCase().indexOf(filterValue) > -1 ||
                x.e_mail.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    loadUserList() {
        // this.spinerService.display(true);
        this.cClient.companyUserList(this.company.company_id)
            .subscribe((resp: Array<UserModel>) => {
                // this.spinerService.display(false);
                console.log('loadUserList -> resp:', resp);
                this.userList = resp;
                this.initUserOpt();
            });
    }

    //#endregion

}
