import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxMaterialModule } from '../material.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
// import { AmsCardCountryComponent } from './country/card-country.component';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { SxSimpleCardComponent } from './simple-card/simple-card.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    SxPipesModule,
    //
    SxComponentsCommonModule,
  ],
  declarations: [
    SxSimpleCardComponent,
    // -Country
    // AmsCardCountryComponent,
  ],
  exports: [
    SxSimpleCardComponent,
    // -Country
    // AmsCardCountryComponent,
  ]
})

export class SxComponentsCardsModule { }
