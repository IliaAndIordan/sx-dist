import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-card-airport-info',
    templateUrl: './card-airport-info.component.html',
})
export class CardAirportInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() airport: AmsAirport;

    /**
     * FIELDS
     */
    roots = AppRoutes;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private gravatarService: GravatarService,
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['airport']) {
            this.airport = changes['airport'].currentValue;
            this.initFields();
        }
    }

    initFields() {
      
    }

    //#region Action Methods

    centerMapOnAirport(){
        console.log('centerMapOnAirport-> ap:', this.airport);
    }

    //#endregion

}
