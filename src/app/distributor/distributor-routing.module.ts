import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { SxDistributorComponent } from './distributor.component';
import { SxDistributorHomeComponent } from './home.component';
import { SxDistributorDashboardComponent } from './dashboard/dashboard.component';
import { SxDistributorItemsHomeComponent } from './citems/distributor-citems.component';
import { SxDistributorCItemsHomeTabsComponent } from './citems/tabs/home-tabs.component';



const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxDistributorComponent,
    children: [
      {
        path: AppRoutes.Root, component: SxDistributorHomeComponent,
       
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', component: SxDistributorDashboardComponent},

          { path: AppRoutes.items, component: SxDistributorItemsHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: SxDistributorCItemsHomeTabsComponent},
          ]},
          /*
          { path: AppRoutes.file_select, component: SxBomFileImportHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: SxBomFileImportSelectFileComponent},
            { path: AppRoutes.map_fields, component: SxBomFileImportMapFieldsComponent },
          ]},
          /*
          { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
          { path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent},
          ]},
          { path: AppRoutes.mac, component: AmsAdminMacHomeComponent , children: [
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminMacHomeTabsComponent},
          ]},
          { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
          */
        ]

      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SxDistributorRoutingModule { }

export const routedComponents = [
  SxDistributorComponent,
  SxDistributorHomeComponent,
  SxDistributorDashboardComponent,
  SxDistributorItemsHomeComponent,
  SxDistributorCItemsHomeTabsComponent,
];
