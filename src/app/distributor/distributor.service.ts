import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { SxApiCompanyClient } from '../@core/services/api/company/api-client';
// -Models
import { AppStore, URL_NO_IMG_SQ } from '../@core/const/app-storage.const';
import { CompanyModel, CompanyProductModel } from '../@core/models/common/sx-company.model';
import { UserModel } from '../@core/services/auth/api/dto';
import { Bom } from '../@core/services/api/bom/dto';
import { CompanyDashboardStat, RespCompanyDashboardStat } from '../@core/services/api/company/dto';
import { IDateValuePoint } from '../@core/models/common/pont.model';


@Injectable({ providedIn: 'root', })
export class SxDistributorService {

    /**
     *  Fields
     */
    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private client: SxApiCompanyClient) {
    }

    //#region user

    get user(): UserModel {
        return this.cus.user;
    }

    set user(value: UserModel) {
        this.cus.user = value;
    }

    //#endregion

    //#region company

    get company(): CompanyModel {
        return this.cus.company;
    }

    set company(value: CompanyModel) {
        this.cus.company = value;
    }

    //#endregion

    //#region BehaviorSubjects

    dashboardRefresh = new BehaviorSubject<boolean>(true);

    //#endregion
    
    //#region mac

    bomChanged = new Subject<Bom>();

    get bom(): Bom {
        const onjStr = localStorage.getItem(AppStore.bom);
        let obj: Bom;
        if (onjStr) {
            obj = Object.assign(new Bom(), JSON.parse(onjStr));
        }
        return obj;
    }

    set bom(value: Bom) {
        const oldValue = this.bom;
        if (value) {
            localStorage.setItem(AppStore.bom, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.bom);
        }

        this.bomChanged.next(value);
    }

    //#endregion

    //#region Admin User Charts

    dashboardStat: CompanyDashboardStat;
    chartItemsByDate: IDateValuePoint[];
    dashboardStatChanged = new BehaviorSubject<CompanyDashboardStat>(undefined);
    public loadDashboardStat(): Observable<CompanyDashboardStat> {

        return new Observable<CompanyDashboardStat>(subscriber => {

            this.client.companyDashboardStat(this.company.company_id)
                .subscribe((resp: RespCompanyDashboardStat) => {
                    // console.log('loadDashboardStat-> resp', resp);
                    this.dashboardStat = resp.data.companyDashboard;
                    this.chartItemsByDate = resp.data.chartItemsByDate;
                    subscriber.next(this.dashboardStat);
                },
            err => {
                throw err;
            });
    });

}

    //#endregion


}
