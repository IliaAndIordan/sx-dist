import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// -Sx Modules
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxCoreModule } from '../@core/@core.module';
// -Services
import { SxDistributorService } from './distributor.service';
// -Models
import { routedComponents, SxDistributorRoutingModule } from './distributor-routing.module';
import { SxDistributorMenuMainComponent } from './menu-main/menu-main.component';
import { SxDistributorCItemsFilterPanelBodyComponent } from './citems/filter-panel/filter-pane.component';
import { SxDistributorCItemsHomeTabGridComponent } from './citems/tabs/tab-grid-items.component';
import { SxGridCItemsComponent } from './citems/grids/citem-table.component';
import { SxDistributorCItemsTableDataSource } from './citems/grids/table.datasource';

@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxPipesModule,
    //
    SxDistributorRoutingModule,
  ],
  declarations: [
    routedComponents,
    SxDistributorMenuMainComponent,
    SxDistributorCItemsFilterPanelBodyComponent,
    SxDistributorCItemsHomeTabGridComponent,
    SxGridCItemsComponent,
  ],
  exports: [],
  entryComponents: [
  ],
  providers: [
    SxDistributorService,
    SxDistributorCItemsTableDataSource,
  ]
})
export class SxDistributorModule { }
