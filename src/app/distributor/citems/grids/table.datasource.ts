import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiBomClient } from 'src/app/@core/services/api/bom/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { Bom, BomItem, BomItemTableCriteria, BomTableCriteria, ResponseBomItemTableData, ResponseBomTableData } from 'src/app/@core/services/api/bom/dto';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { CompanyItem, CompanyItemTableCriteria, ResponseCompanyItemTableData } from 'src/app/@core/services/api/company/dto';
import { SxDistributorCItemsService } from '../distributor-citems.service';
import { SxApiCompanyClient } from 'src/app/@core/services/api/company/api-client';

export const KEY_CRITERIA = 'sx_grid_citems_criteria';
@Injectable()
export class SxDistributorCItemsTableDataSource extends DataSource<CompanyItem> {

    selected: BomItem;

    private _data: Array<CompanyItem>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<CompanyItem[]> = new BehaviorSubject<CompanyItem[]>([]);
    listSubject = new BehaviorSubject<CompanyItem[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<CompanyItem> {
        return this._data;
    }

    set data(value: Array<CompanyItem>) {
        this._data = value;
        this.listSubject.next(this._data as CompanyItem[]);
    }

    constructor(
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private client: SxApiCompanyClient,
        private biService: SxDistributorCItemsService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<CompanyItemTableCriteria>();

    public get criteria(): CompanyItemTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: BomItemTableCriteria;
        if (onjStr) {
            obj = Object.assign(new CompanyItemTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new CompanyItemTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'citemId';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: CompanyItemTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<CompanyItem[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<CompanyItem>> {
        // console.log('loadData->');
        // console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;

        return new Observable<Array<CompanyItem>>(subscriber => {

            const myObserver = this.client.companyItemTable(this.criteria)
                // tslint:disable-next-line: deprecation
                .subscribe(
                    (resp: ResponseCompanyItemTableData) => {
                        // console.log('loadData-> resp=', resp);
                        this.data = new Array<CompanyItem>();

                        if (resp && resp.success) {
                            if (resp && resp.data &&
                                resp.data.items && resp.data.items.length > 0) {
                                resp.data.items.forEach(iu => {
                                    const obj = CompanyItem.fromJSON(iu);
                                    this.data.push(obj);
                                });
                            }

                            this.itemsCount = resp.data.rowsCount ? resp.data.rowsCount.totalRows : 0;
                            this.listSubject.next(this.data);
                            subscriber.next(this.data);
                        }
                        // console.log('loadData-> data=', this.data);
                        // console.log('loadData-> itemsCount=', this.itemsCount);
                        this.isLoading = false;
                    }
                );

        });

    }
    //#endregion
}
