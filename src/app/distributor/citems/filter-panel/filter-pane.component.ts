import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { SxDistributorCItemsService } from '../distributor-citems.service';
import { SxDistributorService } from '../../distributor.service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { ValueTypes } from 'src/app/@share/components/common/side-panel-modal/row/side-panel-row-value.component';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


@Component({
    selector: 'sx-distributor-citems-filter-panel-body',
    templateUrl: './filter-pane.component.html',
})
export class SxDistributorCItemsFilterPanelBodyComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmOpenClick: EventEmitter<any> = new EventEmitter<any>();
    /**
     * FIELDS
     */
    vtypes = ValueTypes;
    roots = AppRoutes;
    company: CompanyModel;
    companyChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private dService: SxDistributorService,
        private diService: SxDistributorCItemsService) {

    }


    ngOnInit(): void {

        this.companyChanged = this.cus.companyChanged.subscribe((com: CompanyModel) => {
            this.dService.company = com;
            this.initFields();
        });

        this.tabIdxChanged = this.diService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields(): void {
        this.tabIdx = this.diService.tabIdx;
        this.company = this.diService.company;
    }

    //#region Dialogs

    ediCompany(data: CompanyModel): void {
        console.log('ediCompany-> data:', data);
        // console.log('ediAirport-> city:', this.stService.city);
        /*
        if (data) {
            data.userId = data.userId ? data.userId : this.cus.user.userId;
            const dialogRef = this.dialogService.open(SxBomEditDialog, {
                width: '721px',
                height: '360px',
                data: {
                    bom: data,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediBom-> res:', res);
                if (res) {
                    // this.table.renderRows();
                    this.bService.bom = res;
                }
                this.initFields();
            });

        }
        else {
            this.toastr.info('Please select BOM first.', 'Select BOM');
        }
        */

    }

    fileUpload(): void {
        console.log('fileUpload-> company:', this.company);
       
        //this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.file_select]);
    }

    //#endregion

    //#region SPM

    spmCompnayOpen(): void {
        this.spmOpenClick.emit(this.company);
    }

    gotoDashboard(): void {
        this.router.navigate([AppRoutes.Root, AppRoutes.distributor]);
    }

    //#endregion

}
