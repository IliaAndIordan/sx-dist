import { Injectable } from '@angular/core';
import { GravatarService } from '@infinitycube/gravatar';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { SxApiCompanyClient } from 'src/app/@core/services/api/company/api-client';
import { SxDistributorService } from '../distributor.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Nodels
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


export const SR_LEFT_PANEL_IN_KEY = 'sx_distributor_citems_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'sx_distributor_citems_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class SxDistributorCItemsService {

    /**
     *  Fields
     */

    constructor(
        private cus: CurrentUserService,
        private gravatarService: GravatarService,
        private client: SxApiCompanyClient,
        private dService: SxDistributorService) {
    }

    //#region company

    get company(): CompanyModel {
        return this.dService.company;
    }

    set company(value: CompanyModel) {
        this.dService.company = value;
    }

    spmCompanyOpen = new BehaviorSubject<boolean>(false);

    //#endregion
    
    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        // console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data

    loadCompany(companyId: number): Observable<CompanyModel> {

        return new Observable<CompanyModel>(subscriber => {

            this.client.companyById(companyId)
                .subscribe((resp: CompanyModel) => {
                    console.log('loadCompany-> resp', resp);
                    this.company = resp;
                    subscriber.next(resp);
                },
                    err => {
                        throw err;
                    });
        });

    }

    //#endregion


}
