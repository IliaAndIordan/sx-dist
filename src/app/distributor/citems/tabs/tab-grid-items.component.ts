import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { SxDistributorCItemsService } from '../distributor-citems.service';
import { SxDistributorService } from '../../distributor.service';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


export const PAGE_VIEW_TYPE = 'sx_distribbutor_citem_pageview';

@Component({
    selector: 'sx-distributor-citems-tab-grid',
    templateUrl: './tab-grid-items.component.html',
})
export class SxDistributorCItemsHomeTabGridComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    // @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    // @Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();

    @Input() tabIdx: number;
    /**
     * Fields
     */
    company: CompanyModel;
    companyChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private diService: SxDistributorCItemsService,
        private dService: SxDistributorService) {

    }


    ngOnInit(): void {

        this.totalCount = 0;

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });


        this.tabIdxChanged = this.diService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields(): void {
        this.selTabIdx = this.diService.tabIdx;
        this.company = this.diService.company;
    }



    //#region Action Methods

    spmCompanyOpenClick(value: any): void {

        console.log('spmCompanyOpenClick -> value=', value);
        if (value) {
            // this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}