import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { Bom, BomItem } from 'src/app/@core/services/api/bom/dto';
import { SxDistributorCItemsService } from '../distributor-citems.service';
import { SxDistributorService } from '../../distributor.service';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


@Component({
    selector: 'sx-distributor-citems-home-tabs',
    templateUrl: './home-tabs.component.html',
})
export class SxDistributorCItemsHomeTabsComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Output() spmCompanyOpen: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();

    /**
     * Fields
     */
    panelIChanged: Subscription;
    selTabIdx: number;
    tabIdxChanged: Subscription;

    company: CompanyModel;
    companyChanged: Subscription;

    get panelIn(): boolean {
        return this.diService.panelIn;
    }

    set panelIn(value: boolean) {
        this.diService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private diService: SxDistributorCItemsService,
        private dService: SxDistributorService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.diService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.diService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });


        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }

    }

    initFields(): void {
        this.selTabIdx = this.diService.tabIdx;
        this.company = this.diService.company;
        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editBomItem(data: BomItem): void {
        console.log('editBomItem-> data:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number): void {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.diService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods

    panelInOpen(): void {
        this.diService.panelIn = true;
    }

    spmCompanyOpenClick(): void {
        if (this.company) {
            this.spmCompanyOpen.emit(this.company);
        }
    }

    //#endregion

}
