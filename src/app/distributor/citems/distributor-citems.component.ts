

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { SxSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { Bom } from 'src/app/@core/services/api/bom/dto';
import { COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';
import { SxDistributorService } from '../distributor.service';
import { SxDistributorCItemsService } from './distributor-citems.service';
import { CompanyModel } from 'src/app/@core/models/common/sx-company.model';


@Component({
  templateUrl: './distributor-citems.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class SxDistributorItemsHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmCompany') spmCompany: SxSidePanelModalComponent;
  /**
   * Fields
   */
  panelInVar = Animate.in;
  panelInChanged: Subscription;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  companyChanged: Subscription;
  company: CompanyModel;
  spmCompanyModel: CompanyModel;

  get panelIn(): boolean {
    const rv = this.diService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    // console.log('panelIn-> hps:', this.hps);
    return this.diService.panelIn;
  }

  set panelIn(value: boolean) {
    this.diService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private spinerService: SpinnerService,
              private cus: CurrentUserService,
              private dService: SxDistributorService,
              private diService: SxDistributorCItemsService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    const tmp = this.panelIn;
    this.panelInChanged = this.diService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });


    this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
      this.diService.company = company;
      this.initFields();
    });
/*
    this.bomChanged = this.biService.spmBomOpen.subscribe((open: boolean) => {
      open ? this.spmBomOpen(this.bom) : this.spmBomClose();
    });

*/
    this.initFields();

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.companyChanged) { this.companyChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields(): void {
    
    this.company = this.diService.company;

    // console.log('initFields-> country:', this.country);
  }

  //#region  Actions

  sidebarToggle(): void {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoCompany(value: CompanyModel): void {
    if (value) {
      // this.biService.bom = value;
      // this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmCompanyClose(): void {
    if (this.spmCompany) {
      this.spmCompany.closePanel();
    }
  }

  spmCompanyOpen(data: CompanyModel): void {
    
    this.spmCompanyModel = data ;
    if (this.spmCompanyModel && this.spmCompany) {
      this.spmCompany.expandPanel();
    }

  }


  //#endregion
}
