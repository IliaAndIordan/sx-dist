import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EChartsOption } from 'echarts';
import * as echarts from 'echarts';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { SxDistributorService } from '../distributor.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Subscription } from 'rxjs';
import { echartMiniAreaOptions } from 'src/app/@core/models/app/charts/chart.model';
import { DateValuePoint, IDateValuePoint } from 'src/app/@core/models/common/pont.model';

@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class SxDistributorDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    roots = AppRoutes;
    logo = COMMON_IMG_LOGO_RED;
    tileImgUrl = URL_COMMON_IMAGE_TILE;
    acTypeImg = URL_COMMON_IMAGE_AIRCRAFT + 'aircraft_0_tableimg.png';
    imgItems = URL_COMMON_IMAGE_TILE + 'barcode-white.png';

    env: string;
    itemsCount: number;
    echartOpt: EChartsOption = echartMiniAreaOptions;

    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;

    dashboardRefresh: Subscription;
    chartItemsByDate: IDateValuePoint[];
    mergeOptItemsByDate: any;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private dService: SxDistributorService) {
    }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.itemsCount = 0;
        this.echartOpt.series = [];
        this.dashboardRefresh = this.dService.dashboardRefresh.subscribe((panelIn: boolean) => {
            this.loadDashboardStat();
        });
        this.checkDashboardStat();


    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields(): void {
        this.itemsCount = this.dService.dashboardStat ? this.dService.dashboardStat.itemsCount : 0;
        this.chartItemsByDate = this.dService.chartItemsByDate;


        if (this.chartItemsByDate) {
            const itemData = DateValuePoint.eChartMiniDataSeriesTime(this.dService.chartItemsByDate);

            // this.echartOpt = echartMiniAreaOptions;
            //this.echartOpt.series = data;
            //this.echartOpt.series.push(data);
            this.mergeOptItemsByDate = {
                series: [
                    {
                        name: 'Distributor Items',
                        data: itemData,
                        type: 'line',
                        smooth: true,
                        showSymbol: false,
                        areaStyle: {
                            opacity: 0.6,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(217, 255, 179, 0.4)'
                            }, {
                                offset: 1,
                                color: 'rgba(1, 191, 236)'
                            }])
                        },
                        tooltip: {
                            pointFormatter: () => {
                              return false;
                            }
                          },
                    }
                ]
            };
        }

        // console.log('loadLoginsCount-> data', resp.data);
    }

    //#region  data
    public checkDashboardStat(): void {
        if (!this.dService.dashboardStat) {
            this.loadDashboardStat();
        }
        else {
            this.initFields();
        }
    }
    public loadDashboardStat(): void {

        this.spinerService.show();
        this.dService.loadDashboardStat()
            .subscribe((data: any) => {
                console.log('loadDashboardStat-> data', data);
                this.spinerService.hide();
                this.initFields();
                /*
                this.loginsLis = list;
                this.usersCount = this.aService.usersCount;
                //console.log('gatGvdtTsoSearchHits-> usersCount', this.usersCount);
                this.chartDataUsers = LoginsCountModel.getLineChartData(this.loginsLis);
                this.chartLabelsUsers = LoginsCountModel.getLineChartLabels(this.loginsLis);
                //console.log('chartLabelsUsers=', this.chartLabelsUsers);
                */
            },
                err => {
                    this.spinerService.hide();
                    console.log('autenticate-> err: ', err);
                });

    }


    //#endregion

    //#region  Actions

    onChartClick(event): void {
        console.log('onChartClick -> event:', event);
    }

    gotoBomList(): void {
        console.log('gotoBomList ->');
        this.router.navigate([AppRoutes.Root, AppRoutes.bom, AppRoutes.list]);
    }

    gotoManufacturers(): void {
        // this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturers]);
    }

    //#endregion
}
